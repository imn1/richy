#!/bin/bash

git tag --sort="v:refname" | tail

echo -n "Enter version number: "
read VERSION

echo $VERSION > app/version
poetry version $VERSION
git commit -am "Updated: version number."
git tag -f $VERSION
git push --tags
