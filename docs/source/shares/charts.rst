Charts
======
Application gathers or renders a few charts to provide better
data visualization than just a simple table.

Scraped charts
--------------
Some charts are scraped from the internet via
``richy.core.scraper.Manager`` class. This class provides convenient
method ``capture_element`` which serves as "take a screenshot of such
and such element".

Earnings chart
~~~~~~~~~~~~~~
Earnings charts shows earning performance of a company. Provides
beat/met/missed values of each earnings estimation. The chart is
scraped from YAHOO! Finance portal from share detail page from
``settings.STATS_URL`` URL (currently "Financials" tab).

.. image:: imgs/earnings_chart.png
   :align: center

Generated charts
----------------
Some other charts are generated from data the app downloads from
the internet. The scraping process is placed in
``richy.core.scraper.Manager`` and the chart generating lives in
``richy.core.charts.Manager``.

Recommendations chart
~~~~~~~~~~~~~~~~~~~~~
Investors rating (recommendations) can be downloaded from
`finviz.com <https://finviz.com>`_ page. Raings are presented as table
on a ticker page.
(`for example <https://finviz.com/quote.ashx?t=AAPL&ty=c&p=d&b=1>`_).
Investors can initiate, retain, downgrade or upgrade their rating.
That's rating status which is mirrored with price value. The price and
investor name are the values that the chart is made of. Also current price
is shown as vertical line.

.. figure:: imgs/recommendations_table.png
   
    Source table on finviz.com

.. figure:: imgs/recommendation_chart.png

    Final chart
