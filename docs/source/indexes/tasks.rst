Tasks
=====

fetch_historical_data(index=None)
---------------------------------------------
Fetches historical prices data from the internet (YAHOO!). Each price is
checked if it's valid (format) and then saved into database. When all records
are saved we perform following actions:

* regenerate cache
* regenerate performance chart
* regenerate open investments chart

The task takes 1 param. PK of ``Index``. If index PK is specified only that
index is fetched. If not all indexes in the database will be fetched one by one.

fetch_current_price(share=None)
-------------------------------
Fetches current market price, status and percentage change even if the market
is closed (after/pre market price).

The data (Price namedtuple object) is cached for 1 hour under
``item-{}-current-price`` key.
