Charts
======

TransactionOverviewChart
------------------------
Shows overview of all open transaction groups. Group is an item of
:ref:`transactions_transactions_get_pile_stats` output. Each "leaf"
transaction is traced down it's root transactions. Root transactions
are investments and leaf are market values in this case. Already sold
transaction are not shown.

.. image:: imgs/transaction_overview_chart.png
   :align: center
