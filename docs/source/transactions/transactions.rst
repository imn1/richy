Transactions
============
A class placed in ``transactions.py`` file. All transaction digging
(pandas dataframing) is placed here. All methods that fetch data and
calculates some metrics are mainly placed in this class.

get_transaction_basic_stats()
-----------------------------
.. automethod:: richy.transactions.transactions.Transactions.get_transaction_basic_stats

.. _transactions_transactions_get_pile_stats:

get_pile_stats()
----------------
.. automethod:: richy.transactions.transactions.Transactions.get_pile_stats
