The Eye
=======
Price history component for all types of item. Shows
intraday data for shares/indexes/etfs in 5 minutes intervals
and past 24 hours prices in 30 minutes intervals for coins.

.. code-block:: javascript

    {
        "symbol": "...",
        "min": "...",
        "max": "...",
        "prices": [
            {
                "x": "...",
                "open": "...",
                "high": "...",
                "low": "...",
                "close": "...",
            }
        ]
    }

Component accepts following params:

- ``item`` - UserItem ID of the item it should display data for
