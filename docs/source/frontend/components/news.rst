News
====
News component displays articles or video news in 1 column on
small devices and in 3 columns on medium and up devices.
The component fetches news via AJAX and gets back following
data structure

.. code-block:: javascript

   [
       {
           "symbol": "...",
           "title": "...",
           "description": "...",
           "image": "...",
           "url": "...",
           "source": "...",
           "date": "...",
       }
   ]

Component accepts following params:

- ``symbol`` - Symbol of the item.
- ``videos`` - Switch for articles or video news
- ``from`` - Date (YYYY-MM-DD) as a start of the news time period (required).
- ``to`` - Date (YYYY-MM-DD) as an end of the news time period (required).
- ``limit`` - Limits the number of news.
- ``coinsOnly`` - Flag for coin news only.
- ``sharesOnly`` - Flag for shares news only.
- ``indexesOnly`` - Flag for indexes news only.
- ``etfsOnly`` - Flag for ETFs news only.
