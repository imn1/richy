Styles
======
App has tiny layer of own styles because it heavily utilizes UI kit
web framework. Custom styles are separated into files that are then
included into ``app.sass`` main file.
