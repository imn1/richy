CI
==
Continuous integration is held by Gitlab server which provides
limited (CPU time/month) platform for auto-triggering the
integration.

Integration can be run manually or automatically after each push
with new commits.

Settings
--------
Settings is saved inside ``.gitlab-ci.yml`` file. The file is placed
in the project root. The CI configuration is placed inside and the
documentation to Gitlab CI is
`here <https://docs.gitlab.com/ee/ci/yaml/README.html>`_.

As helping service server ``docker:dind`` image, which is Docker
in a Docker. The image provides docker daemon which is then set
via ``DOCKER_HOST`` environment variable to the CI engine.

Jobs
----

``build``
~~~~~~~~~
Job tests if the application image can be build for ``dev``
environment.
