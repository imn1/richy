Basics
======

Database dump
-------------
To dump the database use following command (which also bzip the dump).

.. code-block:: bash

    # (no news_news table)
    $ docker exec -it richy-db pg_dump -T news_news -U postgres richy | bzip2 -9f > /tmp/dump.sql.bz2
    # (full dump)
    $ docker exec -it richy-db pg_dump -U postgres richy | bzip2 -9f > /tmp/dump.sql.bz2

Transfer to local and unzip dump to root directory

.. code-block:: bash

    $ rsync vps:/tmp/dump.sql.bz2 /tmp ; bzip2 -df /tmp/dump.sql.bz2 -c > dump.sql

Remove postgres data

.. code-block:: bash

    $ docker rm richy-db

Once the dump file is in project root and database volume is removed
it get automatically loaded into database when you run the project.

To load the dump into database manually use

.. code-block:: bash

    $ docker cp /tmp/dump.sql richy-db:/tmp/dump.sql | docker exec richy-db psql -U postgres -f /tmp/dump.sql
