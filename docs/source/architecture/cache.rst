Cache
=====

Application caches various values across it's whole beauty.
It's mostly because of performance speed ups.

Cached values:

- Transactions performance + asset allocation chart data
    - keys:
        - shares-transaction-performance-and-assets
        - shares-transaction-profit-performance
        - coins-transaction-performance-and-assets
        - coins-transaction-profit-performance
        - indexes-transaction-performance-and-assets
        - indexes-transaction-profit-performance
        - etfs-transaction-performance-and-assets
        - etfs-transaction-profit-performance
    - timeout: settings ``TRANSACTIONS_CACHE_TIMEOUT`` (1 day)
    - invalidated by:
        - shares -- keys:
            - shares/tasks.py - fetch_historical_data()
            - transactions/views.py - ``BaseTransactionManipulationView`` - a new
              transaction added/updated
        - indexes -- keys:
            - indexes/tasks.py - fetch_historical_data()
            - transactions/views.py - ``BaseTransactionManipulationView`` - a new
              transaction added/updated
        - etfs -- keys:
            - etfs/tasks.py - fetch_historical_data()
            - transactions/views.py - ``BaseTransactionManipulationView`` - a new
              transaction added/updated
        - coins -- keys:
            - coins/tasks.py - fetch_historical_data()
            - transactions/views.py - ``BaseTransactionManipulationView`` - a new
              transaction added/updated

- Transactions basic stats
    - keys: transactions-basic-stats-user-X-closed-Y (X is user PK,
      Y is closed flag [True, False, None])
    - timeout: -
    - invalidated by:
        - invalidated by:
            - shares -- keys:
                - shares/tasks.py - fetch_historical_data()
                - transactions/views.py - ``BaseTransactionManipulationView`` - a new
                  transaction added/updated
            - indexes -- keys:
                - indexes/tasks.py - fetch_historical_data()
                - transactions/views.py - ``BaseTransactionManipulationView`` - a new
                  transaction added/updated
            - etfs -- keys:
                - etfs/tasks.py - fetch_historical_data()
                - transactions/views.py - ``BaseTransactionManipulationView`` - a new
                  transaction added/updated
            - coins -- keys:
                - coins/tasks.py - fetch_historical_data()
                - transactions/views.py - ``BaseTransactionManipulationView`` - a new
                  transaction added/updated

- Item price percentage day change
    - key: item-X-days-perc-change-Y (X is item PK,
      Y is number of days)
    - timeout: settings ``PRICE_PERCENTAGE_CHANGE_CACHE_TIMEOUT`` (1 day)
    - invalidated by:
        - shares/tasks.py - ``fetch_historical_data()``
        - coins/tasks.py - ``fetch_historical_data()``

- Item price percentage day change - compounded
    - key: item-X-days-perc-change-Y-compounded (X is item PK,
      Y is number of days)
    - timeout: settings ``PRICE_PERCENTAGE_CHANGE_CACHE_TIMEOUT`` (1 day)
    - invalidated by:
        - shares/tasks.py - ``fetch_historical_data()``
        - coins/tasks.py - ``fetch_historical_data()``

- Item ATH
    - key: item-X-ath
    - timeout: settings ``ATH_TIMEOUT`` (1 day)
    - invalidated by: 
        - shares/tasks.py - ``fetch_historical_data()``
        - coins/tasks.py - ``fetch_historical_data()``
        - indexes/tasks.py - ``fetch_historical_data()``
        - etfs/tasks.py - ``fetch_historical_data()``

- Item drawdown
    - key: item-X-drawdown
    - timeout: settings ``ATH_TIMEOUT`` (1 day)
    - invalidated by:
        - shares/tasks.py - ``fetch_historical_data()``
        - coins/tasks.py - ``fetch_historical_data()``
        - indexes/tasks.py - ``fetch_historical_data()``
        - etfs/tasks.py - ``fetch_historical_data()``

- Item last price
    - key: item-X-last-price
    - timeout: settings ``LAST_PRICE_TIMEOUT`` (1 day)
    - invalidated by: -

- Current item market price + change (fetched from YAHOO!)
    - key: item-X-current-price (X is the item PK)
    - timeout: settings ``ITEM_CURRENT_PRICE_TIMEOUT`` (1 hour)
    - invalidated by: -

- Item intraday market price data (the eye) (fetched from YAHOO!)
    - key: item-X-market-live (X is the item PK)
    - timeout: settings ``ITEM_MARKET_LIVE_CACHE_TIMEOUT`` (5 minutes)
    - invalidated by: -

- SMA
    - key: item-X-sma-Y (X is item PK, Y is SMA period (20, 50, ...))
    - timeout: settings ``SMA_TIMEOUT`` (1 day)
    - invalidated by:
        - shares/tasks.py - ``fetch_historical_data()``
        - coins/tasks.py - ``fetch_historical_data()``
        - indexes/tasks.py - ``fetch_historical_data()``
        - etfs/tasks.py - ``fetch_historical_data()``

- Application version number
    - key: version
    - timeout: forever
    - invalidated by: -

Flush cache
-----------
To update/re-cache cached values there are 2 approaches:

Selective (per item):

.. code-block:: python

    Item.objects.get(symbol="FB").update_cache()

Global for all items:

.. code-block:: console

    $ docker exec -it richy-redis redis-cli flushall
