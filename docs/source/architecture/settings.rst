Settings
========

Environment variables
---------------------
- ``WDB_SOCKET_SERVER`` - WDB server socket (part of WDB package)
- ``WDB_NO_BROWSER_AUTO_OPEN`` - Do (not) open browser when debug kicks in (part of WDB package)
- ``GUNICORN_DEBUG`` - enables debug log level for Gunicorn
- ``GUNICORN_RELOAD`` - enables Gunicorn auto reload
- ``DJANGO_DEBUG`` - enables Django debug mode
- ``DB_HOST`` - sets Postgres host - defaults to ``db``
- ``DB_PORT`` - sets Postgres port - defaults to ``5432``
- ``DB_NAME`` - sets Postgres database name - defaults to ``richy``
- ``DB_USER`` - sets Postgres database user - defaults to ``postgres``
- ``DB_PASSWORD`` - sets Postgres database password
- ``NO_CELERY_FIXTURES`` - disables loading celery fixtures
- ``BASE_URL`` - app base URL
- ``DOMAIN`` - domain(s) (separated by comma) the app runs at
- ``REDIS_URL`` - URL do redis instance, defaults to ``redis:6379``
- ``REDIS_DB`` - Redis database number, defaults to ``1``

Django settings
---------------
App introduces a few custom settings properties.

COMMON
------
- ``MAX_PRECISION`` - determines maximum precision of displayed values (prices) in the app (except coins)
- ``COINS_MAX_PRECISION`` - determines float precision of displayed values (inc. percentages) for coins
- ``SENTRY_ENABLE`` - value ``"true"`` enables Sentry mechanism (also requires ``SENTRY_URL`` to be
  fully functional.
- ``SENTRY_URL`` - URL to Sentry endpoint (DNS). Sentry is used if both this and ``SENTRY_ENABLE``
  directives are configured correctly.

News
----
- ``NEWS_BLACKLIST`` - a list of domains which are blacklisted from news downloading
- ``NEWS_FEED`` - an URL (pattern) to RSS feed which is later parsed for item news
- ``NEWS_PRUNE_THRESHOLD_DAYS`` - number of days when news record expires and gets deleted by celery task

Charts
------
- ``CHART_COLORS`` - a list of colors which cycle in charts
- ``COLOR_GREEN`` - green (positive) color for charts
- ``RED_GREEN`` - red (negative) color for charts

Coins
-----
- ``COIN_EPOCH`` - a date in ``YYYY-MM-DD`` format taken as the epoch of historical
  coin prices.
- ``\*_CACHE_\*`` - cache related timeout - see: :doc:`cache`

  - ``PRICES_CACHE_TIMEOUT``
  - ``INDEX_CACHE_TIMEOUT``
  - ``TRANSACTIONS_CACHE_TIMEOUT``
  - ``NEWS_CACHE_TIMEOUT``
  - ``PRICE_PERCENTAGE_CHANGE_CACHE_TIMEOUT``
  - ``ITEM_CURRENT_PRICE_TIMEOUT``
  - ``SMA_TIMEOUT``
- ``OVERVIEW_COINS_HISTORY`` - number of historical records (in days) for overview chart

Shares
------
- ``OVERVIEW_SHARES_HISTORY`` - number of historical records (in days) for overview chart

Indexes
-------
- ``OVERVIEW_INDEXES_HISTORY`` - number of historical records (in days) for overview chart
