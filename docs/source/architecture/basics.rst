Basics
======

Here are described basics of the app architecture.

Beat logs in ``error`` level mode and has no PID file
(``--pidfile=``). No PID file is because when celery hangs up
the PID file is kept on the disk and prevents to run new beat
process.

Worker runs in one thread (``-c`` is set to ``1``) so all tasks
are consumed in series not in parallel. Also each worker is
replaced with a new one after 3 tasks (``--max-tasks-child=3``).

Meta
----
``core.Meta`` is a special model for application metadata. They
are stored and read from various places of application.

Current metadata is:

* ``LAST_PRICE_UPDATE`` - last date + time when shares/coins data
  were updated - fetched from the internet. Is printed as tooltip
  on application name in the app top bar.

Database
--------

Model managers
~~~~~~~~~~~~~~

Application offers following model managers that extend classic
Django ORM manager.

::

   +-------------------+      +------------------+
   |   PandasManager   | <--- |   UsersManager   |
   +-------------------+      +------------------+
                          ^
                          |   +-----------------+
                          --- |   UserManager   |
                              +-----------------+
