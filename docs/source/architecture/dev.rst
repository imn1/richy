Dev
===

Commands (for dev purposes)
---------------------------

To force-reinstall all containers packages:

.. code-block:: bash

   $ for i in richy beat worker worker_slow worker_fast; do docker compose exec ${i} pip install -UI -r requirements_dev; done

To restart all celery instances:

.. code-block:: bash

   $ for i in richy beat worker worker_slow worker_fast; do docker compose kill -s HUP ${i}; done
