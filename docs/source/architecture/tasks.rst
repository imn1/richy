Tasks
=====
Each Django application has its own "Tasks" section this page contains
common info for all tasks regardless application.

Queues
------
Application uses 3 queues:

- celery (default for all tasks is queue is not specified)
- slow (for long-running tasks)
- fast (for tasks which takes seconds to complete)

Here is an overview table with routing:

+----------------------------------------------+--------+------------------------+
| task                                         | queue  | queue (periodic tasks) |
+==============================================+========+========================+
| richy.core.tasks.generate_performance_charts | celery | celery                 |
+----------------------------------------------+--------+------------------------+
| richy.coins.tasks.fetch_historical_data      | celery | slow                   |
+----------------------------------------------+--------+------------------------+
| richy.coins.tasks.fetch_basic_info           | fast   | celery                 |
+----------------------------------------------+--------+------------------------+
| richy.news.tasks.download_share_news         | slow   | slow                   |
+----------------------------------------------+--------+------------------------+
| richy.news.tasks.download_coin_news          | slow   | slow                   |
+----------------------------------------------+--------+------------------------+
| richy.news.tasks.delete_old_records          | celery | celery                 |
+----------------------------------------------+--------+------------------------+
| richy.shares.tasks.fetch_historical_data     | celery | slow                   |
+----------------------------------------------+--------+------------------------+
| richy.shares.tasks.capture_financial_charts  | celery | slow                   |
+----------------------------------------------+--------+------------------------+
| richy.shares.tasks.fetch_basic_info          | fast   | celery                 |
+----------------------------------------------+--------+------------------------+
| richy.shares.tasks.fetch_current_price       | fast   | celery                 |
+----------------------------------------------+--------+------------------------+
| richy.shares.tasks.fetch_ratings             | fast   | celery                 |
+----------------------------------------------+--------+------------------------+
| richy.shares.tasks.fetch_dividends           | fast   | celery                 |
+----------------------------------------------+--------+------------------------+
| richy.transactions.tasks.generate_graphs     | fast   | \-                     |
+----------------------------------------------+--------+------------------------+
| richy.transactions.tasks.calculate_dividends | celery | \-                     |
+----------------------------------------------+--------+------------------------+

