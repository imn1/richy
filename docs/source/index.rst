Richy's documentation
=====================

Architecture
------------
.. toctree::
   :maxdepth: 1

   architecture/index

Core
----
Application core stuff.

.. toctree::
   :maxdepth: 1

   core/index

Shares
------
Module with all functionality related to stock shares.

.. toctree::
   :maxdepth: 1

   shares/index

Indexes
-------
Module with all functionality ralated to indexes.

.. toctree::
   :maxdepth: 1

   indexes/index

Coins
-----
Module with all functionality related to cryptocurrencies (coins).

.. toctree::
   :maxdepth: 1

   coins/index

Transactions
------------
Module with transactions and related charts.

.. toctree::
   :maxdepth: 1

   transactions/index

News
----
Module fetching news from the internet.

.. toctree::
   :maxdepth: 1

   news/index

Trends
------
Module that handles all trends scraping.

.. toctree::
   :maxdepth: 1

   trends/index

Frontend
--------
All frontend stuff - components, styles, ...

.. toctree::
   :maxdepth: 1

   frontend/index

Server
------
Server stuff

.. toctree::
   :maxdepth: 1

   server/index
