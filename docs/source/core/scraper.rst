Scraper
=======
Module ``richy.core.scraper`` contains classes responsible for actual
web content scraping.

Manager
-------
Class ``Manager`` handles all the methods for scraping actual items:

.. literalinclude:: ../../../app/richy/core/scraper.py
   :pyobject: Manager
