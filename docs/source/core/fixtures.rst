Fixtures
========
App has some basic fixtures which are necessary for setting the app up.
Fixtures are placed in ``{APP}/fixtures`` directory where ``{APP}`` is
the name of django applications inside ``app`` directory.

Fixtures contains:

- site tree
- celery tasks + intervals 
- basic meta data
- crypto exchanges
