Math
----
A collection of common math operations.

``calc_percentage_change(end, start)``
======================================
Calculates difference between ``start`` and ``end`` value in percents.
