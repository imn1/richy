Charts
======

Matplotlib charts
-----------------

Some charts are rendered on backend (using matplotlib or seaborn
libraries) in ``richy.core.charts`` module. Each chart has it's
own class for clarity sake.

Charts are exported in SVG (by default) - for details there is
``export`` method for each  Matplotlib chart.

.. automethod:: richy.core.charts.BaseChart.export

Higcharts charts
----------------

Other charts are interactive Javarcript charts whose data and
options sits in ``richy.core.charts`` module. Following classes
represent those charts.

.. autoclass:: richy.core.charts.TransactionOverviewChart
.. autoclass:: richy.core.charts.TransactionsBasicColumnChart
.. autoclass:: richy.core.charts.TransactionBasicPieChart
.. autoclass:: richy.core.charts.TransactionDetailColumnChart
.. autoclass:: richy.core.charts.DashboardMarketValueRatioPieChart
.. autoclass:: richy.core.charts.GoogleTrendsChart
.. autoclass:: richy.core.charts.OpenStakingsRatioPieChart
.. autoclass:: richy.core.charts.PerformanceChart
.. autoclass:: richy.core.charts.DrawdownChart
