Item
====

Percentage change
-----------------
Percentage change from day 0 to day 1 but also from day 0 to
day 5 can be calculated with ``get_last_days_change(percents=True)``
method. This method seeks for last known price and a price
that suits the "days ago" criterium. When a price for "days ago"
criterium cannot be found (missing data because that date is weekend)
method searches for nearest price before "days ago" date.

Method also :doc:`caches <../architecture/cache>` the result.

Compounded change
~~~~~~~~~~~~~~~~~
Method can also calculate compounded change based on all open deposit
transactions. The method for calculation is
`weighted average <https://en.wikipedia.org/wiki/Weighted_arithmetic_mean>`_.

.. automethod:: richy.core.models.Item.get_last_days_change


ItemData
--------
A model where additional Item related data are stored. Data are
stored as JSON object under certaik keys:

- ``basic_info`` - basic info for all items (displayed on item detail page)
- ``holdings`` - list of ETF holdings - ETFs only

.. warning::

   ItemData model should never been accessed directly. There are methods
   in ``Item`` model and in ancestors. See methods like:

   - ``Item.set_basic_info()``
   - ``Item.get_basic_info()``
