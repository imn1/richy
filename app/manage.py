#!/usr/bin/env python3
import os
import sys
from importlib.util import find_spec

if __name__ == "__main__":
    if "true" == os.environ.get("DJANGO_DEBUG"):
        settings = "richy.settings.dev"
    else:
        settings = "richy.settings.base"

    os.environ.setdefault("DJANGO_SETTINGS_MODULE", settings)

    if find_spec("django.core.management"):
        from django.core.management import execute_from_command_line

        execute_from_command_line(sys.argv)
    else:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        )
