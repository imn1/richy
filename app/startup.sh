#!/bin/bash

RED="\033[0;31m"
GREEN="\033[0;32m"
NC="\033[0m" # No Color

################
#   Dev only   #
################
# apt update && apt install -y git
# pip install -r requirements_dev

###########
#   Run   #
###########
./wait_for_it.sh ${DB_HOST:-db}:${DB_PORT:-5432} --timeout=120 --strict  -- echo "Postgres is up"

# Run migrations.
if ./manage.py migrate; then
    echo -e "${GREEN}Database migrated.${NC}"
else
    echo -e "${RED}Couldn't migrate the database.${NC}"
    exit 1
fi

# Fixtures.
if ./manage.py loaddata users sitetree meta exchanges; then
    echo -e "${GREEN}Fixtures loaded.${NC}"
else
    echo -e "${RED}Couldn't load fixtures.${NC}"
    exit 1
fi

# Celery fixtures.
if [[ ${NO_CELERY_FIXTURES} == true ]]; then
    echo -e "Skipping loading celery fixtures."
else
    if ./manage.py loaddata celery_beat; then
        echo -e "${GREEN}Celery fixtures loaded.${NC}"
    else
        echo -e "${GREEN}Couldn't load celery fixtures.${NC}"
        exit 1
    fi
fi

# Collect static files.
./manage.py collectstatic -i node_modules --no-input --clear &> /dev/null

# Run the app.
gunicorn -c gunicorn.py richy.wsgi:application
