from django.db import models

from ..core.models import Item


class News(models.Model):
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    description = models.TextField(blank=True, null=True)
    image = models.URLField(blank=True, null=True)
    url = models.URLField()
    date = models.DateTimeField(db_index=True)
    is_video = models.BooleanField(default=False, db_index=True)
    is_new = models.BooleanField(default=True)

    class Meta:
        verbose_name_plural = "News"
        unique_together = ["item", "url"]

    def __str__(self):
        return self.title
