import logging
from datetime import timedelta

from django.conf import settings
from django.db import connection
from django.utils import timezone

from ..celery import app
from ..coins.models import Coin
from ..etfs.models import Etf
from ..indexes.models import Index
from ..news.downloader import CoinManager, ShareIndexEtfManager
from ..shares.models import Share
from .models import News

LOGGER = logging.getLogger("richy.celery")


@app.task(queue="slow")
def download_share_news(share=None):
    """
    Fetches news for the given share. If no share (PK) was
    given fetches all of them from database walks thru them
    and downloads news for each of them.

    :param int share: Item PK.
    """

    # Single share.
    if share:
        share = Share.objects.get(pk=share)

        try:
            m = ShareIndexEtfManager(share)
            total = m.fetch()
        except Exception:
            LOGGER.exception(
                "Couldn't download news for a share", extra={"share": share}
            )
            return

        # Add up stats for logging.
        counts = {share.symbol: total}

    # All of them.
    else:
        counts = {}
        total = 0

        for s in Share.objects.all():
            try:
                m = ShareIndexEtfManager(s)
                count = m.fetch()
            except Exception:
                LOGGER.exception(
                    "Couldn't download news for a share", extra={"share": s}
                )
                continue

            # Add up stats for logging.
            counts[s.symbol] = count
            total += count

    # Log news downloading stats.
    msgs = [f"Successfully downloaded {total} news."]

    for key, c in counts.items():
        msgs.append(f"\t- {key}: {c} news.")

    LOGGER.debug("\n".join(msgs))


@app.task(queue="slow")
def download_index_news(index=None):
    """
    Fetches news for the given index. If no index (PK) was
    given fetches all of them from database walks thru them
    and downloads news for each of them.

    :param int index: Item PK.
    """

    # Single index.
    if index:
        index = Index.objects.get(pk=index)

        try:
            m = ShareIndexEtfManager(index)
            total = m.fetch()
        except Exception:
            LOGGER.exception("Couldn't download news for index", extra={"index": index})
            return

        # Add up stats for logging.
        counts = {index.symbol: total}

    # All of them.
    else:
        counts = {}
        total = 0

        for i in Index.objects.all():
            try:
                m = ShareIndexEtfManager(i)
                count = m.fetch()
            except Exception:
                LOGGER.exception(
                    "Couldn't download news for a index", extra={"index": i}
                )
                continue

            # Add up stats for logging.
            counts[i.symbol] = count
            total += count

    # Log news downloading stats.
    msgs = [f"Successfully downloaded {total} news."]

    for key, c in counts.items():
        msgs.append(f"\t- {key}: {c} news.")

    LOGGER.debug("\n".join(msgs))


@app.task(queue="slow")
def download_coin_news(coin=None):
    """
    Fetches news for the given coin. If no coin (PK) was
    given fetches all of them from database walks thru them
    and downloads news for each of them.

    :param int coin: Item PK.
    """

    # Single coin.
    if coin:
        coin = Coin.objects.get(pk=coin)

        try:
            m = CoinManager(coin)
            total = m.fetch()
        except Exception:
            LOGGER.exception("Couldn't download news for a coin", extra={"coin": coin})
            return

        # Add up stats for logging.
        counts = {coin.symbol: total}

    # All of them.
    else:
        counts = {}
        total = 0

        for c in Coin.objects.all():
            try:
                m = CoinManager(c)
                count = m.fetch()
            except Exception:
                LOGGER.exception(
                    "Couldn't download news for a coin", extra={"coin": coin}
                )
                continue

            # Add up stats for logging.
            counts[c.symbol] = count
            total += count

    # Log news downloading stats.
    msgs = [f"Successfully downloaded {total} news."]

    for key, c in counts.items():
        msgs.append(f"\t- {key}: {c} news.")

    LOGGER.debug("\n".join(msgs))


@app.task(queue="slow")
def download_etfs_news(etf=None):
    """
    Fetches news for the given ETF. If no ETF (PK) was
    given fetches all of them from database walks thru them
    and downloads news for each of them.

    :param int etf: Item PK.
    """

    # Single ETF.
    if etf:
        etf = Etf.objects.get(pk=etf)

        try:
            m = ShareIndexEtfManager(etf)
            total = m.fetch()
        except Exception:
            LOGGER.exception("Couldn't download news for an etf", extra={"etf": etf})
            return

        # Add up stats for logging.
        counts = {etf.symbol: total}

    # All of them.
    else:
        counts = {}
        total = 0

        for e in Coin.objects.all():
            try:
                m = ShareIndexEtfManager(e)
                count = m.fetch()
            except Exception:
                LOGGER.exception(
                    "Couldn't download news for an etf", extra={"etf": etf}
                )
                continue

            # Add up stats for logging.
            counts[e.symbol] = count
            total += count

    # Log news downloading stats.
    msgs = [f"Successfully downloaded {total} news."]

    for key, c in counts.items():
        msgs.append(f"\t- {key}: {c} news.")

    LOGGER.debug("\n".join(msgs))


@app.task
def delete_old_records():
    """
    Deletes older news than ``settings.NEWS_PRUNE_THRESHOLD_DAYS`` days.
    """

    # Delete old records.
    threshold_date = timezone.now() - timedelta(days=settings.NEWS_PRUNE_THRESHOLD_DAYS)
    count, _ = News.objects.filter(date__lte=threshold_date).delete()

    LOGGER.debug(f"{count} news pruned.")

    # Vacuum the database.
    with connection.cursor() as c:
        c.execute("VACUUM")
        LOGGER.debug("Database vacuumed.")
