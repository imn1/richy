import logging
from datetime import datetime, timedelta
from urllib.parse import urlparse

from braces.views import LoginRequiredMixin
from django.template.defaultfilters import date as date_filter
from django.template.defaultfilters import truncatewords
from django.utils import timezone
from django.views.generic import TemplateView

from ..coins.models import Coin
from ..core.views import AjaxView
from ..etfs.models import Etf
from ..indexes.models import Index
from ..shares.models import Share
from .models import News

logger = logging.getLogger(__name__)


class DashboardTemplateView(LoginRequiredMixin, TemplateView):
    template_name = "news/dashboard.pug"

    def get_context_data(self, **kwargs):
        context = super(DashboardTemplateView, self).get_context_data(**kwargs)

        context["yesterday"] = str((timezone.now() - timedelta(days=1)).date())
        context["today"] = str(timezone.now().date())
        context["shares"] = Share.objects.by_user(self.request.user).order_by("symbol")
        context["coins"] = Coin.objects.by_user(self.request.user).order_by("symbol")
        context["indexes"] = Index.objects.by_user(self.request.user).order_by("symbol")
        context["etfs"] = Etf.objects.by_user(self.request.user).order_by("symbol")

        return context


class FetchAjaxView(AjaxView):
    def get(self, request, *args, **kwargs):
        params = self.clean_request_params(request.GET)

        # Fetch news by symbol and date range.
        q = News.objects.filter(
            date__date__range=(
                datetime.strptime(params["from"], "%Y-%m-%d"),
                datetime.strptime(params["to"], "%Y-%m-%d"),
            ),
            item__in=request.user.get_items().values_list("pk", flat=True),
            is_video=bool(params.get("videos")),
        ).order_by("-date")

        # Shares/indexes/etfs/coins/symbol only filter.
        if params.get("symbol"):
            q = q.filter(item__symbol=params["symbol"])
        elif params.get("shares_only"):
            q = q.filter(item__share__isnull=False)
        elif params.get("indexes_only"):
            q = q.filter(item__index__isnull=False)
        elif params.get("etfs_only"):
            q = q.filter(item__etf__isnull=False)
        elif params.get("coins_only"):
            q = q.filter(item__coin__isnull=False)

        # Limit filter.
        if params.get("limit"):
            q = q[: params["limit"]]

        # Set new news as not new.
        q.update(is_new=False)
        news = []

        for n in q[:20]:
            news.append(
                {
                    "symbol": n.item.symbol,
                    "title": n.title,
                    "description": truncatewords(n.description, 75),
                    "image": n.image,
                    "url": n.url,
                    "source": self.parse_source(n.url),
                    "date": date_filter(timezone.localtime(n.date), "DATETIME_FORMAT"),
                }
            )

        return self.send(True, news)

    def parse_source(self, url):
        url = urlparse(url)

        # Normalize server URL.
        if url.netloc.startswith("www."):
            server = url.netloc[4:]
        else:
            server = url.netloc

        return server
