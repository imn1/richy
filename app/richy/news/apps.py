from django.apps import AppConfig


class NewsConfig(AppConfig):
    name = "richy.news"
