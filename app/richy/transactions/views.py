import json
import logging
from datetime import date

import pandas as pd
from braces.views import FormMessagesMixin, LoginRequiredMixin
from django.conf import settings
from django.contrib import messages
from django.core.cache import cache
from django.db.models import Sum
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.urls import reverse, reverse_lazy
from django.utils import timezone
from django.utils.translation import gettext as _
from django.views.generic import (
    CreateView,
    DetailView,
    FormView,
    RedirectView,
    TemplateView,
    UpdateView,
)
from sitetree.sitetreeapp import get_sitetree

from ..core.charts import TransactionDetailColumnChart, TransactionOverviewChart
from ..core.math import calc_percentage_change
from ..core.models import Asset, Item, NavigationTreeItem
from ..core.templatetags.utils import autofloatformat
from ..core.views import (
    AjaxView,
    BaseFetchItemAjaxView,
    SubmenuViewMixin,
    TransactionChartDataMixin,
)
from ..staking.models import Staking
from .forms import PostClosedOrClosingTransactionForm, TransactionForm
from .models import Attachment, DividendTransaction, Transaction
from .tasks import calculate_dividends, generate_graphs
from .transactions import Performance, Transactions

LOGGER = logging.getLogger(__name__)


####################
#   Base classes   #
####################
class BaseTransactionMixin(LoginRequiredMixin, SubmenuViewMixin):
    """
    Mixin with submenu for transactions like "Closed transactions" etc.
    """

    def get_submenu(self):
        pass


class BaseTransactionManipulationMixin(LoginRequiredMixin, FormMessagesMixin):
    """
    Base transaction view for it's manipulating - create/update.
    """

    model = Transaction
    form_class = TransactionForm
    form_valid_message = _("Transaction has been saved.")
    form_invalid_message = _("Transaction hasn't been saved. Please check the form.")

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["user"] = self.request.user

        return kwargs

    def form_valid(self, form):
        """
        Deletes cache related to transactions and generates new
        open investments overview chart.
        """

        form.instance.user = self.request.user

        to_return = super().form_valid(form)
        self.update_cache()

        # In case the transaction is marked as closed or closing
        # we redirect to another step with a form (in needed).
        if self.object.is_closed and (
            self.object.parents.filter(is_closed=False).exists()
            or self.object.staking_set.filter(is_sold=False).exists()
        ):
            return HttpResponseRedirect(
                reverse(
                    "transactions:post_closed_or_closing_form", args=[self.object.pk]
                )
            )

        return to_return

    def update_cache(self):
        # Regenerate cache.
        cache.delete("shares-transaction-performance-and-assets")
        cache.delete("shares-transaction-profit-performance")
        cache.delete("indexes-transaction-performance-and-assets")
        cache.delete("indexes-transaction-profit-performance")
        cache.delete("coins-transaction-performance-and-assets")
        cache.delete("coins-transaction-profit-performance")
        cache.delete("etfs-transaction-performance-and-assets")
        cache.delete("etfs-transaction-profit-performance")

        Performance("share", profit=False, use_cache=False).get_data()
        Performance("share", profit=True, use_cache=False).get_data()
        Performance("index", profit=False, use_cache=False).get_data()
        Performance("index", profit=True, use_cache=False).get_data()
        Performance("coin", profit=False, use_cache=False).get_data()
        Performance("coin", profit=True, use_cache=False).get_data()
        Performance("etf", profit=False, use_cache=False).get_data()
        Performance("etf", profit=True, use_cache=False).get_data()

        # Generate graphs for all transactions.
        generate_graphs.delay()

        # In case of deleting a transaction there is no object.
        if hasattr(self, "object"):
            # Make sure it's a share.
            try:
                self.object.item.share
            except Exception:
                pass
            else:
                calculate_dividends.delay(self.object.item.pk)

        Transactions.update_cache()

    def get_success_url(self):
        """
        Redirects user back to appropriate transaction list.
        """

        if self.object.is_closed:
            return reverse("transactions:closed")

        return reverse("transactions:open")


class BaseTransactionDetailView(
    LoginRequiredMixin, TransactionChartDataMixin, SubmenuViewMixin, DetailView
):
    model = Transaction
    template_name = "transactions/detail.pug"

    def get_context_data(self, **kwargs):
        """
        Returns template context.
        """

        context = super().get_context_data(**kwargs)
        data = self.get_transaction_basic_chart_data()
        context["basic_stats_data"] = data

        context.update(self.get_chart_options())
        # Adjust price chart options.
        context["price_chart_overrides"] = {
            "rangeSelector": False,
            "navigator": {"enabled": False},
            "legend": {"enabled": False},
        }

        context["graph"] = self.get_graph()
        context |= self.get_stakings_context()

        return context

    def get_stakings_context(self):
        context = {}
        open_stakings = self.object.staking_set.open()
        closed_stakings = self.object.staking_set.closed()

        if open_stakings or closed_stakings:
            last_price = self.object.item.get_last_price()
            context["open_stakings"] = open_stakings
            context["closed_stakings"] = closed_stakings
            context["stakings_reward_sum"] = self.object.staking_set.aggregate(
                Sum("reward_amount")
            )["reward_amount__sum"]
            context["stakings_reward_sum_value"] = (
                context["stakings_reward_sum"] * last_price.price if last_price else 0
            )

        return context

    def get_transaction_basic_chart_data(self):
        df = Transactions(self.request.user).get_transaction_basic_stats()
        # Returns list - revenue and investment (see method docs for details).
        investment_and_revenue = TransactionDetailColumnChart(df).get_series(
            self.object
        )

        if not df.empty:
            data = {
                "investment_and_revenue": investment_and_revenue,
                "text": self.calc_text_stats(investment_and_revenue),
            }

            return json.loads(json.dumps(data))

    def calc_text_stats(self, data):
        stats = {
            data[0]["name"]: sum(data[0]["data"]),
        }

        # Open transaction has revenue = 0 so we don't need to
        # calculate percentages.
        if 0 < sum(data[1]["data"]):
            stats[data[1]["name"]] = sum(data[1]["data"])
            stats[f"{data[1]['name']} %"] = calc_percentage_change(
                sum(data[1]["data"]), sum(data[0]["data"])
            )

        return stats

    def get_graph(self):
        """
        Fetches transaction graph.

        :return: Asset model instance.
        :rtype: Asset
        """

        try:
            return self.object.asset_set.get(type=Asset.TRANSACTION_GRAPH)
        except Exception:
            pass

    def get_submenu(self):
        """
        Fetches direct chilldren of Transaction menu item.
        """

        menu_items = []
        items = get_sitetree().menu(
            "main",
            "dynamic_menu_parent",
            {
                "request": self.request,
                "object": {"pk": self.object.pk},
                "dynamic_menu_parent": str(
                    NavigationTreeItem.objects.get(url="transactions:index").pk
                ),
            },
        )

        for item in items:
            menu_items.append(
                {
                    "url": reverse(item.url),
                    "title": item.title,
                    "is_current": self.request.path.startswith(reverse(item.url)),
                }
            )

        return menu_items


####################
#   View classes   #
####################
class IndexRedirectView(LoginRequiredMixin, RedirectView):
    """
    Redirects use to open transactions page.
    """

    pattern_name = "transactions:open"


class OpenCreateView(
    TransactionChartDataMixin, BaseTransactionManipulationMixin, CreateView
):
    """
    Handles open transaction list and it's stats.
    """

    template_name = "transactions/open.pug"

    def get_initial(self):
        initial = super().get_initial()
        self.has_new_item = False

        if item := self.request.GET.get("new"):
            initial["item"] = Item.objects.get(pk=item)
            self.has_new_item = True

        return initial

    def get_context_data(self, **kwargs):
        """
        Returns template context.
        """

        context = super().get_context_data(**kwargs)
        context["has_new_item"] = self.has_new_item

        # Transaction list.
        context["transactions"] = (
            Transaction.objects.by_user(self.request.user)
            .select_related("item", "item__share", "item__coin", "item__etf")
            .extra(
                select={
                    "user_item": "SELECT id FROM core_useritem WHERE user_id = %s AND item_id = transactions_transaction.item_id"
                },
                select_params=(self.request.user.pk,),
            )
            .filter(is_closed=False)
            .order_by("-date", "-pk")
        )

        # Chart data.
        context["basic_stats_data"] = self.get_transaction_basic_chart_data()
        context.update(self.get_chart_options())

        # Overview chart.
        basic_stats = Transactions(self.request.user).get_transaction_basic_stats(
            closed=False
        )
        context["transactions_overview_data"] = {}

        # Get data for each item type
        if len(basic_stats):
            for typ in ("share", "coin", "etf"):
                df = basic_stats[basic_stats["type"] == typ]

                if len(df):
                    data = TransactionOverviewChart(self.request.user, df).get_series()
                    context["transactions_overview_data"][typ] = {
                        "data": data,
                        "options": TransactionOverviewChart.get_options(typ, data),
                    }

        return context


class TransactionUpdateView(BaseTransactionManipulationMixin, UpdateView):
    """
    Handles transactions update form.
    """

    template_name = "transactions/update.pug"

    def get_initial(self):
        initial = super().get_initial()

        if self.object.attachment_set.count():
            initial["attachments"] = list(self.object.attachment_set.all())

        return initial

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["object"] = self.object

        return context


class PostClosedOrClosingTransactionUpdateFormView(
    LoginRequiredMixin, FormMessagesMixin, FormView
):
    form_class = PostClosedOrClosingTransactionForm
    template_name = "transactions/post_closed_or_closing_form.pug"
    form_valid_message = _("Parent transaction(s) and/or staking(s) has been saved.")
    form_invalid_message = _(
        "Parent transaction(s) and/or staking(s) hasn't been saved. Please check the form."
    )
    success_url = reverse_lazy("transactions:closed")

    def dispatch(self, request, *args, **kwargs):
        self.object = get_object_or_404(Transaction, pk=kwargs["pk"])
        parents = Transactions(self.request.user).get_transaction_parents(
            self.object.pk
        )
        self.parents_and_self = parents + [self.object.pk]
        self.stakings = (
            Staking.objects.by_user(request.user)
            .filter(transaction__in=self.parents_and_self)
            .not_sold()
        )
        self.parents = Transaction.objects.by_user(self.request.user).filter(
            pk__in=parents
        )

        return super().dispatch(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["parents"] = self.parents
        kwargs["stakings"] = self.stakings

        return kwargs

    def get_initial(self):
        initial = super().get_initial()
        initial["stakings"] = self.stakings
        initial["parents"] = self.parents

        return initial

    def form_valid(self, form):
        for parent in form.cleaned_data["parents"]:
            parent.is_closed = True
            parent.closed_on = timezone.now().date()
            parent.save()

            LOGGER.debug(f"Transaction {parent} has been marked as closed")

        for staking in form.cleaned_data["stakings"]:
            staking.is_sold = True
            staking.save()

            LOGGER.debug(f"Staking {staking} has been marked as sold")

        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["parents"] = self.parents
        context["stakings"] = self.stakings
        context["object"] = self.object

        return context


class ClosedTemplateView(LoginRequiredMixin, TransactionChartDataMixin, TemplateView):
    """
    Handles closed transaction list and it's stats.
    """

    template_name = "transactions/closed.pug"

    def get_context_data(self, **kwargs):
        """
        Returns templae context.
        """

        context = super().get_context_data(**kwargs)

        context["transactions"] = (
            Transaction.objects.by_user(self.request.user)
            .select_related("item", "item__share", "item__coin", "item__etf")
            .extra(
                select={
                    "user_item": "SELECT id FROM core_useritem WHERE user_id = %s AND item_id = transactions_transaction.item_id"
                },
                select_params=(self.request.user.pk,),
            )
            .filter(is_closed=True)
            .order_by("-date", "-pk")
        )
        context["basic_stats_data"] = []
        context["transactions_timeline_data"] = []
        context["basic_stats_data"] = self.get_transaction_basic_chart_data(closed=True)
        #  context[
        #      "transactions_timeline_data"
        #  ] = self.get_transactions_timeline_chart_data(closed=True)
        context.update(self.get_chart_options())

        return context


class DeleteTransactionRedirectView(BaseTransactionManipulationMixin, RedirectView):
    """
    Handles transaction deleting.
    """

    def get_redirect_url(self, *args, **kwargs):
        """
        Deletes the transaction, deletes cache related to transactions,
        generates new open investments overview chart.
        """

        transaction = get_object_or_404(
            Transaction, pk=self.kwargs["pk"], user=self.request.user
        )
        transaction.delete()

        self.update_cache()

        messages.success(self.request, _("Transaction has been deleted."))

        return reverse("transactions:open")


class OpenTransactionDetailView(BaseTransactionDetailView):
    closed = False


class ClosedTransactionDetailView(BaseTransactionDetailView):
    closed = True


class DividendTransactionDetailView(LoginRequiredMixin, DetailView):
    model = DividendTransaction
    template_name = "transactions/dividend_detail.pug"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = (
            f"{self.object.dividend.share.symbol} dividend - {self.object.dividend.payment_date}"
        )

        return context


class DeleteAttachmentRedirectView(LoginRequiredMixin, RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        attachment = get_object_or_404(Attachment, pk=kwargs["pk"])
        attachment.delete()

        messages.success(self.request, _("Attachment has been deleted."))

        return self.request.META["HTTP_REFERER"]


##############################
#   Asset ajax chart views   #
##############################
class FetchItemPricesAjaxView(BaseFetchItemAjaxView):
    def get(self, request, *args, **kwargs):
        trans = get_object_or_404(Transaction, pk=kwargs["pk"])

        # Determine start and end of the timeline based on
        # type (negative/positive) and closed status of the
        # transaction.
        if 0 < trans.amount:
            start = trans.date

            if trans.is_closed:
                end = trans.closed_on
            else:
                end = date.today()

        else:
            stats = Transactions(request.user).get_transaction_basic_stats(
                closed=trans.is_closed
            )

            df = stats.loc[stats.loc[trans.pk].root_parent]
            start = None

            if isinstance(df, pd.Series):
                start = df["date"]
            else:
                start = df["date"].min()

            end = trans.date

        data = {
            "item": self.get_item_prices(trans.item, datetime__date__range=(start, end))
        }

        if trans.target:
            if "percents" in request.GET:
                target = trans.target_as_percents
                label = f"Target {autofloatformat(target)}%"
            else:
                target = trans.target_as_absolute_value
                label = f"Target {autofloatformat(target)} {trans.currency}"

            data["plotLines"] = [
                {
                    "value": target,
                    "color": settings.CHART_COLORS[1],
                    "width": 2,
                    "zIndex": 4,
                    "label": {"text": label},
                }
            ]

        # if trans.target:
        #     data["plotLines"] = [
        #         {
        #             "name": f"{_('Transaction')} {trans.pk}",
        #             "color": settings.CHART_COLORS[1],
        #         }
        #     ]

        return self.send(True, data)


class AssetsTemplateView(LoginRequiredMixin, TemplateView):
    """
    Handles page with asset charts.
    """

    template_name = "transactions/assets.pug"

    def get_context_data(self, **kwargs):
        """
        Returns template context.
        """

        context = super().get_context_data(**kwargs)

        context["assets_non_stacking_chart_options"] = self.get_asset_chart_options()
        context["assets_stacking_chart_options"] = self.get_asset_chart_options(
            stacking=True
        )

        context["has_shares"] = (
            Transaction.objects.by_user(self.request.user)
            .filter(item__share__isnull=False)
            .exists()
        )
        context["has_coins"] = (
            Transaction.objects.by_user(self.request.user)
            .filter(item__coin__isnull=False)
            .exists()
        )
        context["has_etfs"] = (
            Transaction.objects.by_user(self.request.user)
            .filter(item__etf__isnull=False)
            .exists()
        )
        context["currencies"] = {
            "share": self.request.user.get_traded_currencies("share"),
            "etf": self.request.user.get_traded_currencies("etf"),
            "coin": self.request.user.get_traded_currencies("coin"),
        }

        return context

    @staticmethod
    def get_asset_chart_options(stacking=False):
        """
        Returns option for asset normal/stacking chart.

        :param bool stacking: Stacking flag.
        :return: Chart options in JSON
        :rtype: str
        """

        options = {
            "rangeSelector": {"selected": 4, "inputPosition": {"align": "center"}},
            "navigator": {"enabled": False},
            "legend": {"enabled": True},
        }

        if stacking:
            options["plotOptions"] = {
                "area": {
                    "stacking": "percent",
                    "lineColor": "#ffffff",
                    "lineWidth": 1,
                    "marker": {"lineWidth": 1, "lineColor": "#ffffff"},
                }
            }

        return json.dumps(options)


class BaseAssetAllocationChartDataAjaxView(AjaxView):
    """
    Common view for all assets related views.
    """

    profit = False
    coins = False

    def get(self, request, *args, **kwargs):
        """
        Handles AJAX GET request.
        """

        perf = Performance(
            self.type, self.request.user, kwargs["currency"], self.profit
        )
        data = perf.get_data()

        return self.send(True, data)


class FetchSharesAssetAllocationChartDataAjaxView(BaseAssetAllocationChartDataAjaxView):
    """
    Handles asset allocation chart for shares.
    """

    type = "share"


class FetchSharesProfitsChartDataAjaxView(BaseAssetAllocationChartDataAjaxView):
    """
    Handles profit chart for shares.
    """

    type = "share"
    profit = True


class FetchEtfsProfitsChartDataAjaxView(BaseAssetAllocationChartDataAjaxView):
    """
    Handles profit chart for etfs.
    """

    type = "etf"
    profit = True


class FetchEtfsAssetAllocationChartDataAjaxView(BaseAssetAllocationChartDataAjaxView):
    """
    Handles asset allocation for etfs.
    """

    type = "etf"


class FetchCoinsAssetAllocationChartDataAjaxView(BaseAssetAllocationChartDataAjaxView):
    """
    Handles asset allocation chart for coins.
    """

    type = "coin"


class FetchCoinsProfitsChartDataAjaxView(BaseAssetAllocationChartDataAjaxView):
    """
    Handles profit chart for coins.
    """

    profit = True
    type = "coin"
