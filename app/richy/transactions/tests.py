import io
import logging
import os
import re
from datetime import date
from pathlib import Path

from bs4 import BeautifulSoup
from django.urls import reverse
from freezegun import freeze_time

from ..coins.models import Coin
from ..core.charts import TransactionDetailColumnChart
from ..core.models import Price, UserItem
from ..core.tests import (
    BaseDecimalFormatTestCase,
    BaseTestCase,
    create_exchanges,
    create_items,
    create_prices,
    create_transactions,
    create_user_items,
)
from ..shares.models import Share
from ..shares.tasks import fetch_dividends
from ..staking.models import Staking
from .dividends import DividendTransactions
from .models import Attachment, DividendTransaction, Exchange, Transaction
from .transactions import Graph, Performance, Transactions

LOGGER = logging.getLogger(__name__)


class TransactionTestCase(BaseTestCase):
    reset_sequences = True

    def test_open_transaction_basic_stats(self):
        """
        Schema:

        Binance
        -------

                                                                    -> [11]ETH(-25/0.5) -> [12]TRX(+50/0.25)
        $50 -> [6]BTC(+50/1) -> [9]BTC(-25/1) -> [10]ETH(+50/0.5) /
                          \
                            -> [7]BTC(-25/1) -> [8]XRP(+100/0.25) -----------------------
                                                                                          \
                                                                                            -> [13]XRP(-200/0.25) {SOLD}
                                                                        [3]XRP(+100/0.25) /
                                                                      ^
                                                                     /
        Kraken                                           -----------
        ------                                          /
                                                       /
        $50 -> [1]XRP(+200/0.25) -> [2]XRP(-100/0.25) /
                              \
                                -> [4]XRP(-100/0.25) -> [5]ETC(+100/0.25)
        """

        # Create all the database records.
        create_exchanges()
        create_items()
        create_transactions(
            [
                [
                    {
                        "item": Coin.objects.get(symbol="XRP"),
                        "price": 0.25,
                        "amount": 200,
                        "fee": 1,
                        "date": "2018-12-01",
                        "exchange": Exchange.objects.get(title="Kraken"),
                        "is_deposit": True,
                        "user": self.user,
                    },
                    {},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="XRP"),
                        "price": 0.25,
                        "amount": -100,
                        "fee": 0,
                        "date": "2019-12-02",
                        "exchange": Exchange.objects.get(title="Kraken"),
                        "is_deposit": False,
                        "user": self.user,
                    },
                    {"parents": [1]},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="XRP"),
                        "price": 0.25,
                        "amount": 100,
                        "fee": 0,
                        "date": "2019-12-02",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "is_deposit": False,
                        "user": self.user,
                    },
                    {"parents": [2]},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="XRP"),
                        "price": 0.25,
                        "amount": -100,
                        "fee": 0,
                        "date": "2019-12-02",
                        "exchange": Exchange.objects.get(title="Kraken"),
                        "user": self.user,
                    },
                    {"parents": [1]},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="ETC"),
                        "price": 0.25,
                        "amount": 100,
                        "fee": 0,
                        "date": "2019-12-02",
                        "exchange": Exchange.objects.get(title="Kraken"),
                        "user": self.user,
                    },
                    {"parents": [4]},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="BTC"),
                        "price": 1,
                        "amount": 50,
                        "fee": 0,
                        "date": "2019-12-05",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "is_deposit": True,
                        "user": self.user,
                    },
                    {},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="BTC"),
                        "price": 1,
                        "amount": -25,
                        "fee": 0,
                        "date": "2019-12-05",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "is_closed": True,
                        "user": self.user,
                    },
                    {"parents": [6]},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="XRP"),
                        "price": 0.25,
                        "amount": 100,
                        "fee": 0,
                        "date": "2019-12-05",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "is_closed": True,
                        "user": self.user,
                    },
                    {"parents": [7]},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="BTC"),
                        "price": 1,
                        "amount": -25,
                        "fee": 0,
                        "date": "2019-12-06",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "is_closed": True,
                        "user": self.user,
                    },
                    {"parents": [6]},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="ETH"),
                        "price": 0.5,
                        "amount": 50,
                        "fee": 0,
                        "date": "2019-12-06",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "user": self.user,
                    },
                    {"parents": [9]},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="ETH"),
                        "price": 0.5,
                        "amount": -25,
                        "fee": 0,
                        "date": "2019-12-07",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "user": self.user,
                    },
                    {"parents": [10]},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="TRX"),
                        "price": 0.25,
                        "amount": 50,
                        "fee": 0,
                        "date": "2019-12-07",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "user": self.user,
                    },
                    {"parents": [11]},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="XRP"),
                        "price": 0.25,
                        "amount": -200,
                        "fee": 0,
                        "date": "2019-12-12",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "is_closed": True,
                        "is_closing": True,
                        "user": self.user,
                    },
                    {"parents": [3, 8]},
                ],
            ]
        )

        # Create the df.
        df = Transactions(self.user).get_transaction_basic_stats(closed=False)
        df_short = df[["exchange", "amount", "root_parent", "is_leaf"]]

        # Test number of open transactions in the df.
        self.assertEqual(len(df.index), 14)

        # Test transactions spread across exchanges, it's amount and `is_leaf` flag.
        self.assertEqual(
            df_short.loc[3].to_dict(),
            {
                "exchange": "Binance",
                "amount": 100.0,
                "root_parent": 1,
                "is_leaf": False,
            },
        )
        self.assertEqual(
            df_short.loc[8].to_dict(),
            {
                "exchange": "Binance",
                "amount": 100.0,
                "root_parent": 6,
                "is_leaf": False,
            },
        )
        self.assertEqual(
            sorted(
                df.loc[13][["root_parent", "is_leaf"]].to_dict(orient="records"),
                key=lambda i: i["root_parent"],
            ),
            [
                {"root_parent": 1, "is_leaf": True},
                {"root_parent": 6, "is_leaf": True},
            ],
        )
        self.assertEqual(
            df_short.loc[10].to_dict(),
            {"exchange": "Binance", "amount": 50.0, "root_parent": 6, "is_leaf": False},
        )
        self.assertEqual(
            df_short.loc[11].to_dict(),
            {
                "exchange": "Binance",
                "amount": -25.0,
                "root_parent": 6,
                "is_leaf": False,
            },
        )
        self.assertEqual(
            df_short.loc[12].to_dict(),
            {"exchange": "Binance", "amount": 50.0, "root_parent": 6, "is_leaf": True},
        )

    def test_create_transaction_with_attachments(self):
        create_exchanges()
        create_items()
        create_user_items()

        attachment_1 = io.BytesIO(self.f.binary(length=64))
        attachment_1.name = "example_1.jpg"
        attachment_2 = io.BytesIO(self.f.binary(length=64))
        attachment_2.name = "example_2.jpg"

        response = self.c.post(
            reverse("transactions:open"),
            data={
                "date": str(date.today()),
                "currency": "USD",
                "exchange": Exchange.objects.order_by("?").first().pk,
                "item": UserItem.objects.by_user(self.user).order_by("?").first().pk,
                "amount": self.f.pyfloat(positive=True),
                "price": self.f.pyfloat(positive=True),
                "fee": self.f.pyfloat(positive=True),
                "attachments": [attachment_1, attachment_2],
            },
        )

        self.assertEqual(response.status_code, 302)
        self.assertEqual(1, Transaction.objects.count())
        self.assertEqual(2, Attachment.objects.count())

        for attachment in Attachment.objects.all():
            self.assertTrue(Path(attachment.file.path).exists())

        # Clean up attachment files.
        Transaction.objects.first().delete()

    def test_close_parent_transaction_and_stakings(self):
        """
        Schema:

        Binance
        -------
        $1 -> [1]ETH(+1/1) -> [2]ETH[-1/1] -> [3]ETC(+1/1) -> [4]ETC(-1/1)
                 ^                                 ^
                 |                                 |
            [St 1](1, 1)                      [St 2](1, 1)

        """

        create_exchanges()
        create_items()
        create_user_items()
        create_prices(
            [
                ["2023-01-01", "ETH", 1],
                ["2023-01-02", "ETH", 1],
                ["2023-01-03", "ETH", 1],
                ["2023-01-04", "ETH", 1],
                ["2023-01-01", "ETC", 1],
                ["2023-01-02", "ETC", 1],
                ["2023-01-03", "ETC", 1],
                ["2023-01-04", "ETC", 1],
            ]
        )
        binance = Exchange.objects.get(title="Binance")
        eth = Coin.objects.get(symbol="ETH")
        etc = Coin.objects.get(symbol="ETC")
        create_transactions(
            [
                [
                    {
                        "item": eth,
                        "price": 1,
                        "amount": 1,
                        "fee": 0,
                        "date": "2023-01-01",
                        "exchange": binance,
                        "is_deposit": True,
                        "user": self.user,
                    },
                    {},
                ],
                [
                    {
                        "item": eth,
                        "price": 1,
                        "amount": -1,
                        "fee": 0,
                        "date": "2023-01-02",
                        "exchange": binance,
                        "user": self.user,
                    },
                    {"parents": [1]},
                ],
                [
                    {
                        "item": etc,
                        "price": 1,
                        "amount": 1,
                        "fee": 0,
                        "date": "2023-01-03",
                        "exchange": binance,
                        "user": self.user,
                    },
                    {"parents": [2]},
                ],
                ## This transaction will created by POST request.
                # [
                #     {
                #         "item": etc,
                #         "price": 1,
                #         "amount": -1,
                #         "fee": 0,
                #         "date": "2023-01-04",
                #         "exchange": binance,
                #         "is_closing": True,
                #         "is_closed": True,
                #         "closed_on": "2023-01-04",
                #     },
                #     {"parents": [3]},
                # ],
            ]
        )

        Staking.objects.create(
            item=eth,
            exchange=binance,
            start="2023-01-01",
            end="2023-01-01",
            amount=1,
            reward_amount=1,
            transaction=Transaction.objects.get(pk=1),
            user=self.user,
        )
        Staking.objects.create(
            item=etc,
            exchange=binance,
            start="2023-01-03",
            end="2023-01-04",
            amount=1,
            reward_amount=1,
            transaction=Transaction.objects.get(pk=2),
            user=self.user,
        )

        response = self.c.post(
            reverse("transactions:open"),
            {
                "parents": [3],
                "date": "2023-01-04",
                "exchange": binance.pk,
                "currency": "USD",
                "item": etc.pk,
                "amount": -1,
                "price": 1,
                "fee": 0,
                "is_closing": True,
                "is_closed": True,
                "closed_on": "2023-01-04",
            },
            follow=True,
        )

        html = response.content.decode("utf-8")
        self.assertRedirects(
            response, reverse("transactions:post_closed_or_closing_form", args=[4])
        )
        self.assertIn(str(Transaction.objects.get(pk=1)), html)
        self.assertIn(str(Transaction.objects.get(pk=2)), html)
        self.assertIn(str(Transaction.objects.get(pk=3)), html)
        self.assertIn(str(Staking.objects.get(pk=1)), html)
        self.assertIn(str(Staking.objects.get(pk=2)), html)

        response = self.c.post(
            reverse("transactions:post_closed_or_closing_form", args=[4]),
            {"parents": [1, 2, 3], "stakings": [1, 2]},
        )

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, reverse("transactions:closed"))

        tr4 = Transaction.objects.get(pk=4)
        self.assertEqual(4, Transaction.objects.filter(is_closed=True).count())
        self.assertTrue(tr4.is_closing)
        self.assertEqual("2023-01-04", str(tr4.closed_on))

        self.assertEqual(2, Staking.objects.sold().count())


class TransactionTreeTestCase(BaseTestCase):
    reset_sequences = True

    def test_single_input_single_output(self):
        """
        Tests transaction chaining and grouping.

        Input: single
        Output: single
        Profit: 200%

              -
             | |
         _   | |
        | |  | |
        | |  | |

        Schema:

        Binance:
        --------

        $50 -> [1]BTC(+50/1) -> [2]BTC(-50/1) -> [3]XRP(+200/0.25) -> [4]XRP(-200/0.5) = $100
        """

        create_exchanges()
        create_items()
        create_prices(
            [
                ["2018-12-01", "BTC", 1],
                ["2018-12-02", "BTC", 1],
                ["2018-12-03", "XRP", 0.25],
                ["2018-12-10", "XRP", 0.5],
            ]
        )
        create_transactions(
            [
                [
                    {
                        "item": Coin.objects.get(symbol="BTC"),
                        "price": 1,
                        "amount": 50,
                        "fee": 0,
                        "date": "2018-12-01",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "is_deposit": True,
                        "is_closed": True,
                        "user": self.user,
                    },
                    {},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="BTC"),
                        "price": 1,
                        "amount": -50,
                        "fee": 0,
                        "date": "2018-12-02",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "is_closed": True,
                        "user": self.user,
                    },
                    {"parents": [1]},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="XRP"),
                        "price": 0.25,
                        "amount": 200,
                        "fee": 0,
                        "date": "2018-12-02",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "is_closed": True,
                        "user": self.user,
                    },
                    {"parents": [2]},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="XRP"),
                        "price": 0.5,
                        "amount": -200,
                        "fee": 0,
                        "date": "2018-12-10",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "is_closed": True,
                        "is_closing": True,
                        "user": self.user,
                    },
                    {"parents": [3]},
                ],
            ]
        )

        graph = Graph(Transaction.objects.get(pk=4))
        graph.generate()

        # Create the df.
        df = Transactions(self.user).get_transaction_basic_stats(closed=True)

        # Test number of open transactions in the df.
        self.assertEqual(len(df.index), 4)
        # Check if everything is sold.
        self.assertEqual(df["amount"].sum(), 0.0)
        # Test transaction amount and `is_leaf` flag.
        self.assertEqual(
            df.loc[4][["root_parent", "is_leaf"]].to_dict(),
            {"root_parent": 1, "is_leaf": True},
        )

        # Convert df to a chain.
        groups = Transactions.get_pile_stats(df)

        self.assertEqual(len(groups), 1)
        # Only one root parent.
        self.assertEqual(groups[0][0], [1])
        # Test whole group (open symbols, investments, incomes, fees).
        self.assertEqual(
            groups[0][1],
            {
                "open_symbols": [
                    {"symbol": "BTC", "amount": 0.0, "market_price": 0.0},
                    {"symbol": "XRP", "amount": 0.0, "market_price": 0.0},
                ],
                "investments": [{1: 50.0}],
                "incomes": [{4: 100.0}],
                "fees": 0.0,
            },
        )

        # Test graph
        self._compare_graphs(graph.dot.source, "graph_single_input_single_output")
        self.assertGreater(
            io.BytesIO(graph.dot.pipe(format="png")).getbuffer().nbytes, 0
        )

    def test_multiple_inputs_single_output(self):
        """
        Tests transaction chaining and grouping.

        Input: multiple
        Output: single
        Profit: 150%


              -
         _   | |
        |-|  | |
        | |  | |

        Schema:

        Binance:
        --------

        $50 -> [1]BTC(+50/1) -------------------------------------------
                                                                         \
                                                                           -> [5]BTC(-75/1.5) = $112.5
                                                                         /
        $25 -> [2]XRP(+100/0.25) -> [3]XRP(-100/0.25) -> [4]BTC(+25/1) -
        """

        create_exchanges()
        create_items()
        create_prices(
            [
                ["2018-12-01", "BTC", 1],
                ["2018-12-02", "BTC", 1],
                ["2018-12-03", "BTC", 1.5],
                ["2018-12-01", "XRP", 0.25],
                ["2018-12-01", "XRP", 0.25],
            ]
        )
        create_transactions(
            [
                [
                    {
                        "item": Coin.objects.get(symbol="BTC"),
                        "price": 1,
                        "amount": 50,
                        "fee": 0,
                        "date": "2018-12-01",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "is_deposit": True,
                        "is_closed": True,
                        "user": self.user,
                    },
                    {},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="XRP"),
                        "price": 0.25,
                        "amount": 100,
                        "fee": 0,
                        "date": "2018-12-01",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "is_deposit": True,
                        "is_closed": True,
                        "user": self.user,
                    },
                    {},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="XRP"),
                        "price": 0.25,
                        "amount": -100,
                        "fee": 0,
                        "date": "2018-12-02",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "is_closed": True,
                        "user": self.user,
                    },
                    {"parents": [2]},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="BTC"),
                        "price": 1,
                        "amount": 25,
                        "fee": 0,
                        "date": "2018-12-02",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "is_closed": True,
                        "user": self.user,
                    },
                    {"parents": [3]},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="BTC"),
                        "price": 1.5,
                        "amount": -75,
                        "fee": 0,
                        "date": "2018-12-03",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "is_closing": True,
                        "is_closed": True,
                        "user": self.user,
                    },
                    {"parents": [4, 1]},
                ],
            ]
        )

        graph = Graph(Transaction.objects.get(pk=4))
        graph.generate()

        # Create the df.
        df = Transactions(self.user).get_transaction_basic_stats(closed=True)

        # Test number of open transactions in the df.
        self.assertEqual(len(df.index), 6)
        # Check if everything is sold.
        self.assertEqual(df.groupby(df.index).first()["amount"].sum(), 0.0)
        # Test transaction amount and `is_leaf` flag.
        self.assertEqual(
            df.loc[5][["root_parent", "is_leaf"]].to_dict(orient="records"),
            [
                {"is_leaf": True, "root_parent": 1},
                {"is_leaf": True, "root_parent": 2},
            ],
        )

        # Convert df to a chain.
        groups = Transactions.get_pile_stats(df)

        self.assertEqual(len(groups), 1)
        # Only one root parent.
        self.assertEqual(groups[0][0], [1, 2])
        # Test whole group (open symbols, investments, incomes, fees).
        self.assertEqual(
            groups[0][1],
            {
                "open_symbols": [
                    {"symbol": "BTC", "amount": 0.0, "market_price": 0.0},
                    {"symbol": "XRP", "amount": 0.0, "market_price": 0.0},
                ],
                "investments": [{1: 50.0}, {2: 25.0}],
                "incomes": [{5: 112.5}],
                "fees": 0.0,
            },
        )

        # Test graph
        self._compare_graphs(graph.dot.source, "graph_multiple_inputs_single_output")
        self.assertGreater(
            io.BytesIO(graph.dot.pipe(format="png")).getbuffer().nbytes, 0
        )

    def test_single_input_multiple_outputs(self):
        """
        Tests transaction chaining and grouping.

        Input: single
        Output: multiple
        Profit: 50%


         -
        | |
        | |   -
        | |  |-|
        | |  | |

        Schema:

        Binance:
        --------

                               -> [2]BTC(-25/1) -> [3]XRP(+100/0.25) -> [7]XRP(-100/0.125) = $12.5
                             /
        $50 -> [1]BTC(+50/1)
                             \
                               -> [4]BTC(-25/1) -> [5]ETH(+50/0.5) -> [6]ETH(-50/0-25) = $12.5
        """

        create_exchanges()
        create_items()
        create_prices(
            [
                ["2018-12-01", "BTC", 1],
                ["2018-12-02", "BTC", 1],
                ["2018-12-03", "BTC", 1],
                ["2018-12-02", "XRP", 0.25],
                ["2018-12-04", "XRP", 0.125],
                ["2018-12-03", "ETH", 0.5],
                ["2018-12-04", "ETH", 0.25],
            ]
        )
        create_transactions(
            [
                [
                    {
                        "item": Coin.objects.get(symbol="BTC"),
                        "price": 1,
                        "amount": 50,
                        "fee": 0,
                        "date": "2018-12-01",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "is_deposit": True,
                        "is_closed": True,
                        "user": self.user,
                    },
                    {},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="BTC"),
                        "price": 1,
                        "amount": -25,
                        "fee": 0,
                        "date": "2018-12-02",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "is_closed": True,
                        "user": self.user,
                    },
                    {"parents": [1]},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="XRP"),
                        "price": 0.25,
                        "amount": 100,
                        "fee": 0,
                        "date": "2018-12-02",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "is_closed": True,
                        "user": self.user,
                    },
                    {"parents": [2]},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="BTC"),
                        "price": 1,
                        "amount": -25,
                        "fee": 0,
                        "date": "2018-12-03",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "is_closed": True,
                        "user": self.user,
                    },
                    {"parents": [1]},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="ETH"),
                        "price": 0.5,
                        "amount": 50,
                        "fee": 0,
                        "date": "2018-12-03",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "is_closed": True,
                        "user": self.user,
                    },
                    {"parents": [4]},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="ETH"),
                        "price": 0.25,
                        "amount": -50,
                        "fee": 0,
                        "date": "2018-12-04",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "is_closed": True,
                        "is_closing": True,
                        "user": self.user,
                    },
                    {"parents": [5]},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="XRP"),
                        "price": 0.125,
                        "amount": -100,
                        "fee": 0,
                        "date": "2018-12-04",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "is_closed": True,
                        "is_closing": True,
                        "user": self.user,
                    },
                    {"parents": [3]},
                ],
            ]
        )

        graph = Graph(Transaction.objects.get(pk=4))
        graph.generate()

        # Create the df.
        df = Transactions(self.user).get_transaction_basic_stats(closed=True)

        # Test number of open transactions in the df.
        self.assertEqual(len(df.index), 7)
        # Check if everything is sold.
        self.assertEqual(df.groupby(df.index).first()["amount"].sum(), 0.0)
        # Test transaction amount and `is_leaf` flag.
        self.assertEqual(
            df.loc[6][["root_parent", "is_leaf"]].to_dict(),
            {"is_leaf": True, "root_parent": 1},
        )
        self.assertEqual(
            df.loc[7][["root_parent", "is_leaf"]].to_dict(),
            {"is_leaf": True, "root_parent": 1},
        )

        # Convert df to a chain.
        groups = Transactions.get_pile_stats(df)

        self.assertEqual(len(groups), 1)
        # Only one root parent.
        self.assertEqual(groups[0][0], [1])
        # Test whole group (open symbols, investments, incomes, fees).
        self.maxDiff = None
        self.assertEqual(
            groups[0][1],
            {
                "open_symbols": [
                    {"symbol": "BTC", "amount": 0.0, "market_price": 0.0},
                    {"symbol": "ETH", "amount": 0.0, "market_price": 0.0},
                    {"symbol": "XRP", "amount": 0.0, "market_price": 0.0},
                ],
                "investments": [{1: 50.0}],
                "incomes": [{6: 12.5}, {7: 12.5}],
                "fees": 0.0,
            },
        )

        # Test graph
        self._compare_graphs(graph.dot.source, "graph_single_input_multiple_outputs")
        self.assertGreater(
            io.BytesIO(graph.dot.pipe(format="png")).getbuffer().nbytes, 0
        )

    def test_multiple_inputs_multiple_outputs(self):
        """
        Tests transaction chaining and grouping.

        Input: multiple
        Output: multiple
        Profit: 150%

              -
         -   |-|
        |-|  |-|
        | |  | |

        Schema:

        Binance:
        --------

                                   -> [2]ETH(-50/0.5) -> [3]XRP(+100/0.25) -> [4]XRP(-100/0.5) = $50
                                 /
        $50 -> [1]ETH(+100/0.5) -> [5]ETH(-50/1) = $50

        $50 -> [6]BTC(+50/1) -> [7]BTC(-50/1) = $50
        """

        create_exchanges()
        create_items()
        create_prices(
            [
                ["2018-12-02", "BTC", 1],
                ["2018-12-03", "BTC", 1],
                ["2018-12-01", "ETH", 0.5],
                ["2018-12-02", "ETH", 0.5],
                ["2018-12-03", "ETH", 1],
                ["2018-12-02", "XRP", 0.25],
                ["2018-12-03", "XRP", 0.5],
            ]
        )
        create_transactions(
            [
                [
                    {
                        "item": Coin.objects.get(symbol="ETH"),
                        "price": 0.5,
                        "amount": 100,
                        "fee": 0,
                        "date": "2018-12-01",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "is_deposit": True,
                        "is_closed": True,
                        "user": self.user,
                    },
                    {},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="ETH"),
                        "price": 0.5,
                        "amount": -50,
                        "fee": 0,
                        "date": "2018-12-02",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "is_closed": True,
                        "user": self.user,
                    },
                    {"parents": [1]},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="XRP"),
                        "price": 0.25,
                        "amount": 100,
                        "fee": 0,
                        "date": "2018-12-02",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "is_closed": True,
                        "user": self.user,
                    },
                    {"parents": [2]},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="XRP"),
                        "price": 0.5,
                        "amount": -100,
                        "fee": 0,
                        "date": "2018-12-03",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "is_closed": True,
                        "is_closing": True,
                        "user": self.user,
                    },
                    {"parents": [3]},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="ETH"),
                        "price": 1,
                        "amount": -50,
                        "fee": 0,
                        "date": "2018-12-03",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "is_closed": True,
                        "is_closing": True,
                        "user": self.user,
                    },
                    {"parents": [1]},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="BTC"),
                        "price": 1,
                        "amount": 50,
                        "fee": 0,
                        "date": "2018-12-02",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "is_deposit": True,
                        "is_closed": True,
                        "user": self.user,
                    },
                    {},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="BTC"),
                        "price": 1,
                        "amount": -50,
                        "fee": 0,
                        "date": "2018-12-03",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "is_closed": True,
                        "is_closing": True,
                        "user": self.user,
                    },
                    {"parents": [6]},
                ],
            ]
        )

        graph = Graph(Transaction.objects.get(pk=4))
        graph.generate()

        # Create the df.
        df = Transactions(self.user).get_transaction_basic_stats(closed=True)

        # Test number of open transactions in the df.
        self.assertEqual(len(df.index), 7)
        # Check if everything is sold.
        self.assertEqual(df.groupby(df.index).first()["amount"].sum(), 0.0)
        # Test transaction amount and `is_leaf` flag.
        self.assertEqual(
            df.loc[4][["root_parent", "is_leaf"]].to_dict(),
            {"is_leaf": True, "root_parent": 1},
        )
        self.assertEqual(
            df.loc[5][["root_parent", "is_leaf"]].to_dict(),
            {"is_leaf": True, "root_parent": 1},
        )
        self.assertEqual(
            df.loc[7][["root_parent", "is_leaf"]].to_dict(),
            {"is_leaf": True, "root_parent": 6},
        )

        # Convert df to a chain.
        groups = Transactions.get_pile_stats(df)

        self.assertEqual(len(groups), 2)

        # Check root parents.
        self.assertEqual(groups[0][0], [1])
        self.assertEqual(groups[1][0], [6])

        # Test whole group (open symbols, investments, incomes, fees).
        self.maxDiff = None
        self.assertEqual(
            groups[0][1],
            {
                "open_symbols": [
                    {"symbol": "ETH", "amount": 0.0, "market_price": 0.0},
                    {"symbol": "XRP", "amount": 0.0, "market_price": 0.0},
                ],
                "investments": [{1: 50.0}],
                "incomes": [{4: 50.0}, {5: 50.0}],
                "fees": 0.0,
            },
        )
        self.assertEqual(
            groups[1][1],
            {
                "open_symbols": [{"symbol": "BTC", "amount": 0.0, "market_price": 0.0}],
                "investments": [{6: 50.0}],
                "incomes": [{7: 50.0}],
                "fees": 0.0,
            },
        )

        # Test graph
        self._compare_graphs(graph.dot.source, "graph_multiple_inputs_multiple_outputs")
        self.assertGreater(
            io.BytesIO(graph.dot.pipe(format="png")).getbuffer().nbytes, 0
        )

    def _compare_graphs(self, source, file):
        """
        Compares Dot graph sources.

        :param str source: Generated graph source.
        :param str file: File name for a file the source will be compared to.
        """

        # print(file)
        # print(source)

        self.assertEqual(
            source.replace("\t", "    "),
            open(os.path.join(os.path.dirname(__file__), "test_data", file)).read(),
        )


class TransactionDetailColumnChartTestCase(BaseTestCase):
    reset_sequences = True

    def test_single_deposit_single_revenue_and_single_deposit_sell_and_single_deposit_buy(
        self,
    ):
        """
        Tests investment and revenue calculation related to a specific
        transaction.

        Deposit: single
        Investment: -
        Revenue: -
        Current: deposit

        Deposit: single
        Investment: -
        Revenue: -
        Current: sell

        Deposit: single
        Investment: -
        Revenue: -
        Current: buy

        Schema:

        Binance:
        --------

        $500 -> [1]BTC(+500/1) -> [2]BTC(-500/1.2) -> [3]ETH(+1200/0.5)
                      ^                  ^                    ^
                      |                  |                    |

        """

        create_exchanges()
        create_items()
        create_transactions(
            [
                [
                    {
                        "item": Coin.objects.get(symbol="BTC"),
                        "price": 1,
                        "amount": 500,
                        "fee": 0,
                        "date": "2018-12-01",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "is_deposit": True,
                        "user": self.user,
                    },
                    {},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="BTC"),
                        "price": 1.2,
                        "amount": -500,
                        "fee": 0,
                        "date": "2018-12-02",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "user": self.user,
                    },
                    {"parents": [1]},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="ETH"),
                        "price": 0.5,
                        "amount": 1200,
                        "fee": 0,
                        "date": "2018-12-02",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "user": self.user,
                    },
                    {"parents": [2]},
                ],
            ]
        )

        # Create price for item.
        Price.objects.create(
            item=Coin.objects.get(symbol="ETH"), datetime="2018-12-01", price=0.5
        )

        # Test deposit POV on the transactions.
        current = Transaction.objects.get(pk=1)
        df = Transactions(self.user).get_transaction_basic_stats()
        series = TransactionDetailColumnChart(df).get_series(current)
        self.assertEqual(
            series,
            [
                {"data": [500.0], "name": "Investment"},
                {"data": [600.0], "name": "Revenue", "visible": True},
            ],
        )

        # Test sell POV on the transactions.
        current = Transaction.objects.get(pk=2)
        series = TransactionDetailColumnChart(df).get_series(current)
        self.assertEqual(
            series,
            [
                {"data": [500.0], "name": "Deposit"},
                {"data": [500.0], "name": "Investment"},
                {"data": [600.0], "name": "Revenue"},
            ],
        )

        # Test buy POV on the transactions.
        current = Transaction.objects.get(pk=3)
        series = TransactionDetailColumnChart(df).get_series(current)
        self.assertEqual(
            series,
            [
                {"data": [500.0], "name": "Deposit"},
                {"data": [600.0], "name": "Investment"},
                {"data": [0], "name": "Revenue (none)", "visible": False},
                {"data": [600.0], "name": "Market value"},
            ],
        )

    def test_multiple_deposits_multiple_investments_sell(self):
        """
        Tests investment and revenue calculation related to a specific
        transaction.

        Deposit: multiple
        Investment: single
        Revenue: -
        Current: sell

        Schema:

        Binance:
        --------

        $500 -> [1]BTC(+500/1) -> [3]BTC(-500/1.2) -> [4]ETH(+1200/0.5) -
                                                                          \
                                                                            -> [5]ETH(-2200/0.75)
                                                                          /             ^
        $500 -> [2]ETH(+1000/0.5) ---------------------------------------               |

        """

        create_exchanges()
        create_items()
        create_transactions(
            [
                [
                    {
                        "item": Coin.objects.get(symbol="BTC"),
                        "price": 1,
                        "amount": 500,
                        "fee": 0,
                        "date": "2018-12-01",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "is_deposit": True,
                        "user": self.user,
                    },
                    {},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="ETH"),
                        "price": 0.5,
                        "amount": 1000,
                        "fee": 0,
                        "date": "2018-12-01",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "is_deposit": True,
                        "user": self.user,
                    },
                    {},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="BTC"),
                        "price": 1.2,
                        "amount": -500,
                        "fee": 0,
                        "date": "2018-12-02",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "user": self.user,
                    },
                    {"parents": [1]},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="ETH"),
                        "price": 0.5,
                        "amount": 1200,
                        "fee": 0,
                        "date": "2018-12-02",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "user": self.user,
                    },
                    {"parents": [3]},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="ETH"),
                        "price": 0.75,
                        "amount": -2200,
                        "fee": 0,
                        "date": "2018-12-03",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "user": self.user,
                    },
                    {"parents": [2, 4]},
                ],
            ]
        )

        # Test deposit POV on the transactions.
        current = Transaction.objects.get(pk=5)
        df = Transactions(self.user).get_transaction_basic_stats()
        series = TransactionDetailColumnChart(df).get_series(current)
        self.assertEqual(
            series,
            [
                {"data": [1000.0], "name": "Deposit (shared)"},
                {"data": [1100.0], "name": "Investment (shared)"},
                {"data": [1650.0], "name": "Revenue"},
            ],
        )

    def test_multiple_deposits_buy(self):
        """
        Tests investment and revenue calculation related to a specific
        transaction.

        Deposit: multiple
        Investment: single
        Revenue: -
        Current: buy

        Schema:

        Binance:
        --------

        $500 -> [1]BTC(+500/1) -
                                 \
                                   -> [3]BTC(-1000/0.75) -> [4]ETH(+1500/0.5)
                                 /                                 ^
        $500 -> [2]BTC(+500/1) -                                   |

        """

        create_exchanges()
        create_items()
        create_transactions(
            [
                [
                    {
                        "item": Coin.objects.get(symbol="BTC"),
                        "price": 1,
                        "amount": 500,
                        "fee": 0,
                        "date": "2018-12-01",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "is_deposit": True,
                        "user": self.user,
                    },
                    {},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="BTC"),
                        "price": 1,
                        "amount": 500,
                        "fee": 0,
                        "date": "2018-12-01",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "is_deposit": True,
                        "user": self.user,
                    },
                    {},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="BTC"),
                        "price": 0.75,
                        "amount": -1000,
                        "fee": 0,
                        "date": "2018-12-03",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "user": self.user,
                    },
                    {"parents": [1, 2]},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="ETH"),
                        "price": 0.5,
                        "amount": 1500,
                        "fee": 0,
                        "date": "2018-12-04",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "user": self.user,
                    },
                    {"parents": [3]},
                ],
            ]
        )

        # Create price for item.
        Price.objects.create(
            item=Coin.objects.get(symbol="ETH"), datetime="2018-12-01", price=0.5
        )

        # Test investment POV on the transactions.
        current = Transaction.objects.get(pk=4)
        df = Transactions(self.user).get_transaction_basic_stats()
        series = TransactionDetailColumnChart(df).get_series(current)
        self.assertEqual(
            series,
            [
                {"data": [1000.0], "name": "Deposit (shared)"},
                {"data": [750.0], "name": "Investment"},
                {"data": [0], "name": "Revenue (none)", "visible": False},
                {"data": [750.0], "name": "Market value"},
            ],
        )

    def test_multiple_deposits_single_revenue_buy_and_multiple_deposits_single_investment_sell(
        self,
    ):
        """
        Tests investment and revenue calculation related to a specific
        transaction.

        Deposit: multiple
        Investment: single
        Revenue: single
        Current: buy

        Deposit: multiple
        Investment: single
        Revenue: -
        Current: sell

        Schema:

        Binance:
        --------

        $500 -> [1]BTC(+500/1) -
                                 \
                                   -> [3]BTC(-1000/0.75) -> [4]ETH(+1500/0.5) -> [5]ETH(-1500/0.75)
                                 /                                  ^                    ^
        $500 -> [2]BTC(+500/1) -                                    |                    |

        """

        create_exchanges()
        create_items()
        create_transactions(
            [
                [
                    {
                        "item": Coin.objects.get(symbol="BTC"),
                        "price": 1,
                        "amount": 500,
                        "fee": 0,
                        "date": "2018-12-01",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "is_deposit": True,
                        "user": self.user,
                    },
                    {},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="BTC"),
                        "price": 1,
                        "amount": 500,
                        "fee": 0,
                        "date": "2018-12-01",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "is_deposit": True,
                        "user": self.user,
                    },
                    {},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="BTC"),
                        "price": 0.75,
                        "amount": -1000,
                        "fee": 0,
                        "date": "2018-12-03",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "user": self.user,
                    },
                    {"parents": [1, 2]},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="ETH"),
                        "price": 0.5,
                        "amount": 1500,
                        "fee": 0,
                        "date": "2018-12-04",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "user": self.user,
                    },
                    {"parents": [3]},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="ETH"),
                        "price": 0.75,
                        "amount": -1500,
                        "fee": 0,
                        "date": "2018-12-05",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "user": self.user,
                    },
                    {"parents": [4]},
                ],
            ]
        )

        # Test investment POV on the transactions.
        current = Transaction.objects.get(pk=4)
        df = Transactions(self.user).get_transaction_basic_stats()
        series = TransactionDetailColumnChart(df).get_series(current)
        self.assertEqual(
            series,
            [
                {"data": [1000.0], "name": "Deposit (shared)"},
                {"data": [750.0], "name": "Investment"},
                {"data": [1125.0], "name": "Revenue", "visible": True},
            ],
        )

        # Test sell POV on the transactions.
        current = Transaction.objects.get(pk=5)
        df = Transactions(self.user).get_transaction_basic_stats()
        series = TransactionDetailColumnChart(df).get_series(current)
        self.assertEqual(
            series,
            [
                {"data": [1000.0], "name": "Deposit (shared)"},
                {"data": [750.0], "name": "Investment"},
                {"data": [1125.0], "name": "Revenue"},
            ],
        )

    def test_single_deposit_multiple_revenue(self):
        """
        Tests investment and revenue calculation related to a specific
        transaction.

        Deposit: single
        Investment: single
        Revenue: multiple
        Current: deposit

        Schema:

        Binance:
        --------
                                   -> [2]BTC(-250/1.25)
                                 /
        $500 -> [1]BTC(+500/1) -
                      ^          \
                      |            -> [3]BTC(-250/1.5)

        """

        create_exchanges()
        create_items()
        create_transactions(
            [
                [
                    {
                        "item": Coin.objects.get(symbol="BTC"),
                        "price": 1,
                        "amount": 500,
                        "fee": 0,
                        "date": "2018-12-01",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "is_deposit": True,
                        "user": self.user,
                    },
                    {},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="BTC"),
                        "price": 1.25,
                        "amount": -250,
                        "fee": 0,
                        "date": "2018-12-02",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "user": self.user,
                    },
                    {"parents": [1]},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="BTC"),
                        "price": 1.5,
                        "amount": -250,
                        "fee": 0,
                        "date": "2018-12-03",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "user": self.user,
                    },
                    {"parents": [1]},
                ],
            ]
        )

        # Test deposit POV on the transactions.
        current = Transaction.objects.get(pk=1)
        df = Transactions(self.user).get_transaction_basic_stats()
        series = TransactionDetailColumnChart(df).get_series(current)
        self.assertEqual(
            series,
            [
                {"data": [500.0], "name": "Investment"},
                {"data": [687.5], "name": "Revenue (multiple)", "visible": True},
            ],
        )

    def test_multiple_deposits_buy_common_parent(self):
        """
        Tests investment and revenue calculation related to a specific
        transaction.

        Deposit: multiple
        Investment: single
        Revenue: -
        Current: buy

        Schema:

        Binance:
        --------

        $500 -> [1]BTC(+500/1) -                             -> [4]ETH(+1000/0.5)
                                 \                         /
                                   -> [3]BTC(-1000/0.75) -
                                 /                         \
        $500 -> [2]BTC(+500/1) -                             -> [5]TRX(+1000/0.25)
                                                                        ^
                                                                        |
        """

        create_exchanges()
        create_items()
        create_transactions(
            [
                [
                    {
                        "item": Coin.objects.get(symbol="BTC"),
                        "price": 1,
                        "amount": 500,
                        "fee": 0,
                        "date": "2018-12-01",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "is_deposit": True,
                        "user": self.user,
                    },
                    {},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="BTC"),
                        "price": 1,
                        "amount": 500,
                        "fee": 0,
                        "date": "2018-12-01",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "is_deposit": True,
                        "user": self.user,
                    },
                    {},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="BTC"),
                        "price": 0.75,
                        "amount": -1000,
                        "fee": 0,
                        "date": "2018-12-03",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "user": self.user,
                    },
                    {"parents": [1, 2]},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="ETH"),
                        "price": 0.5,
                        "amount": 1000,
                        "fee": 0,
                        "date": "2018-12-04",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "user": self.user,
                    },
                    {"parents": [3]},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="TRX"),
                        "price": 0.25,
                        "amount": 1000,
                        "fee": 0,
                        "date": "2018-12-05",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "user": self.user,
                    },
                    {"parents": [3]},
                ],
            ]
        )

        # Create price for item.
        Price.objects.create(
            item=Coin.objects.get(symbol="TRX"), datetime="2018-12-01", price=0.25
        )

        # Test sell POV on the transactions.
        current = Transaction.objects.get(pk=5)
        df = Transactions(self.user).get_transaction_basic_stats()
        series = TransactionDetailColumnChart(df).get_series(current)
        self.assertEqual(
            series,
            [
                {"data": [1000.0], "name": "Deposit (shared)"},
                {"data": [250.0], "name": "Investment"},
                {"data": [0], "name": "Revenue (none)", "visible": False},
                {"data": [250.0], "name": "Market value"},
            ],
        )


class DividendTransactionsTestCase(BaseTestCase):
    def test_calculating(self):
        create_items()
        share = Share.objects.get(symbol="QCOM")
        fetch_dividends(share.pk)

        create_transactions(
            [
                [
                    {
                        "item": share,
                        "price": 50,
                        "amount": 10,
                        "fee": 0,
                        "date": "2017-01-01",
                        "is_deposit": True,
                        "user": self.user,
                    },
                    {},
                ],
            ],
        )

        trans = DividendTransactions(share)
        trans.calculate()

        self.assertGreaterEqual(DividendTransaction.objects.count(), 12)

        # Now we create a closing transaction back in time.
        # Some of already calculated dividends should get removed.
        Transaction.objects.all().update(is_closed=True)
        create_transactions(
            [
                [
                    {
                        "item": share,
                        "price": 50,
                        "amount": -10,
                        "fee": 0,
                        "date": "2017-12-31",
                        "is_deposit": False,
                        "is_closing": True,
                        "user": self.user,
                    },
                    {},
                ],
            ],
        )

        trans.calculate()

        self.assertEqual(DividendTransaction.objects.count(), 4)


@freeze_time("2018-01-07")
class ProfitCharts(BaseTestCase):
    reset_sequences = True

    # Overloads parent method because freezegun probably hands over an empty
    # argument *args as (). Seems like a bug.
    def setUp(cls, *args):
        super().setUp()

    def test_single_deposit_with_exchange_transfer_and_coin_swap(self):
        """

        Schema:

        +------------------ Kraken ----------------++------------------------ Binance -----------------------+
        |                                          ||                                                        |
        | $500 -> [1]BTC(+500/1) -> [2]BTC(-500/1) -> [3]BTC(+500/1) -> [4]BTC(-500/1) -> [5]TRX(+2000/0.25) |
        |                                          ||                                                        |
        +------------------------------------------++--------------------------------------------------------+

        """

        #
        create_exchanges()
        create_items()
        create_transactions(
            [
                [
                    {
                        "item": Coin.objects.get(symbol="BTC"),
                        "price": 1,
                        "amount": 500,
                        "fee": 0,
                        "date": "2018-01-01",
                        "exchange": Exchange.objects.get(title="Kraken"),
                        "is_deposit": True,
                        "user": self.user,
                    },
                    {},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="BTC"),
                        "price": 1,
                        "amount": -500,
                        "fee": 0,
                        "date": "2018-01-03",
                        "exchange": Exchange.objects.get(title="Kraken"),
                        "user": self.user,
                    },
                    {"parents": [1]},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="BTC"),
                        "price": 1,
                        "amount": 500,
                        "fee": 0,
                        "date": "2018-01-03",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "user": self.user,
                    },
                    {"parents": [2]},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="BTC"),
                        "price": 1,
                        "amount": -500,
                        "fee": 0,
                        "date": "2018-01-03",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "user": self.user,
                    },
                    {"parents": [3]},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="TRX"),
                        "price": 0.25,
                        "amount": 2000,
                        "fee": 0,
                        "date": "2018-01-03",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "user": self.user,
                    },
                    {"parents": [4]},
                ],
            ]
        )
        create_prices(
            [
                ["2018-01-01", "BTC", 1],
                ["2018-01-02", "BTC", 1.25],
                ["2018-01-03", "BTC", 1.25],
                ["2018-01-04", "BTC", 1],
                ["2018-01-05", "BTC", 1],
                ["2018-01-01", "TRX", 0.25],
                ["2018-01-02", "TRX", 0.25],
                ["2018-01-03", "TRX", 0.25],
                ["2018-01-04", "TRX", 0.5],
                ["2018-01-05", "TRX", 0.5],
            ]
        )

        self.assertEqual(
            Performance("coin", self.user, "USD", profit=True).get_data(),
            [
                {
                    "name": "BTC",
                    "data": [
                        (1514764800000, 0.0),
                        (1514851200000, 125.0),
                        (1514937600000, None),
                        (1515024000000, None),
                        (1515110400000, None),
                    ],
                },
                {
                    "name": "TRX",
                    "data": [
                        (1514764800000, None),
                        (1514851200000, None),
                        (1514937600000, 0.0),
                        (1515024000000, 500.0),
                        (1515110400000, 500.0),
                    ],
                },
            ],
        )

    def test_single_deposit_with_partial_coin_swap(self):
        """

        Schema:

        +----------------- Kraken --------------++------------------------------- Binance -------------------------------+
        |                                       ||                                                                       |
        | $500 -> [1]ETH(+1000/0.5) -> [2]ETH(-250/0.75) -> [3]ETH(+250/0.75) -> [4]ETH(-250/0.75) -> [5]TRX(+750/0.25) |
        |                                       ||                                                                       |
        +---------------------------------------++-----------------------------------------------------------------------+

        """

        create_exchanges()
        create_items()
        create_transactions(
            [
                [
                    {
                        "item": Coin.objects.get(symbol="ETH"),
                        "price": 0.5,
                        "amount": 1000,
                        "fee": 0,
                        "date": "2018-01-01",
                        "exchange": Exchange.objects.get(title="Kraken"),
                        "is_deposit": True,
                        "user": self.user,
                    },
                    {},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="ETH"),
                        "price": 0.75,
                        "amount": -250,
                        "fee": 0,
                        "date": "2018-01-03",
                        "exchange": Exchange.objects.get(title="Kraken"),
                        "user": self.user,
                    },
                    {"parents": [1]},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="ETH"),
                        "price": 0.75,
                        "amount": 250,
                        "fee": 0,
                        "date": "2018-01-03",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "user": self.user,
                    },
                    {"parents": [2]},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="ETH"),
                        "price": 0.75,
                        "amount": -250,
                        "fee": 0,
                        "date": "2018-01-03",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "user": self.user,
                    },
                    {"parents": [3]},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="TRX"),
                        "price": 0.25,
                        "amount": 750,
                        "fee": 0,
                        "date": "2018-01-03",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "user": self.user,
                    },
                    {"parents": [4]},
                ],
            ]
        )
        create_prices(
            [
                ["2018-01-01", "ETH", 0.5],
                ["2018-01-02", "ETH", 0.5],
                ["2018-01-03", "ETH", 0.75],
                ["2018-01-04", "ETH", 0.75],
                ["2018-01-05", "ETH", 1.0],
                ["2018-01-01", "TRX", 0.15],
                ["2018-01-02", "TRX", 0.15],
                ["2018-01-03", "TRX", 0.25],
                ["2018-01-04", "TRX", 0.25],
                ["2018-01-05", "TRX", 0.5],
            ]
        )

        self.assertEqual(
            Performance("coin", self.user, "USD", profit=True).get_data(),
            [
                {
                    "name": "ETH",
                    "data": [
                        (1514764800000, 0.0),
                        (1514851200000, 0.0),
                        (1514937600000, 250.0),
                        (1515024000000, 250.0),
                        (1515110400000, 437.5),
                    ],
                },
                {
                    "name": "TRX",
                    "data": [
                        (1514764800000, None),
                        (1514851200000, None),
                        (1514937600000, 0.0),
                        (1515024000000, 0.0),
                        (1515110400000, 187.5),
                    ],
                },
            ],
        )

    def test_two_deposits_with_one_merging_sell(self):
        """

        Schema:

        $500 -> [1]BTC(+500/1) ---
                                   \
                                     -> [2]BTC(-700/2)
                                   /
        $300 -> [1]BTC(+200/1.5) -

        """

        create_exchanges()
        create_items()
        create_transactions(
            [
                [
                    {
                        "item": Coin.objects.get(symbol="BTC"),
                        "price": 1,
                        "amount": 500,
                        "fee": 0,
                        "date": "2018-01-01",
                        "exchange": Exchange.objects.get(title="Kraken"),
                        "is_deposit": True,
                        "user": self.user,
                    },
                    {},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="BTC"),
                        "price": 1.5,
                        "amount": 200,
                        "fee": 0,
                        "date": "2018-01-03",
                        "exchange": Exchange.objects.get(title="Kraken"),
                        "user": self.user,
                    },
                    {},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="BTC"),
                        "price": 2,
                        "amount": -700,
                        "fee": 0,
                        "date": "2018-01-06",
                        "exchange": Exchange.objects.get(title="Kraken"),
                        "user": self.user,
                    },
                    {"parents": [1, 2]},
                ],
            ]
        )
        create_prices(
            [
                ["2018-01-01", "BTC", 1],
                ["2018-01-02", "BTC", 1.25],
                ["2018-01-03", "BTC", 1.5],
                ["2018-01-04", "BTC", 1.5],
                ["2018-01-05", "BTC", 2],
                ["2018-01-06", "BTC", 2],
            ]
        )

        self.assertEqual(
            Performance("coin", self.user, "USD", profit=True).get_data(),
            [
                {
                    "name": "BTC",
                    "data": [
                        (1514764800000, 0.0),
                        (1514851200000, 125.0),
                        (1514937600000, 250.0),
                        (1515024000000, 250.0),
                        (1515110400000, 600.0),
                    ],
                }
            ],
        )


@freeze_time("2023-01-01")
class TransactionTargetTestCase(BaseTestCase):
    reset_sequences = True
    fixtures = ["sitetree", "meta"]

    # Overloads parent method because freezegun probably hands over an empty
    # argument *args as (). Seems like a bug.
    def setUp(cls, *args):
        super().setUp()

    def prepare_transaction(self):
        create_items()
        create_user_items()
        create_exchanges()
        self.btc = Coin.objects.get(symbol="BTC")
        create_transactions(
            [
                [
                    {
                        "item": self.btc,
                        "price": 1,
                        "amount": 1,
                        "fee": 0,
                        "date": "2023-01-01",
                        "exchange": Exchange.objects.get(title="Kraken"),
                        "target": 1.1,
                        "is_deposit": True,
                        "user": self.user,
                    },
                    {},
                ]
            ]
        )
        create_prices(
            [
                ["2023-01-01", "BTC", 1],
                ["2023-01-02", "BTC", 1],
                ["2023-01-03", "BTC", 1],
            ]
        )

    def test_transaction_list_item_has_icon(self):
        self.prepare_transaction()

        response = self.c.get(reverse("transactions:open"))

        self.assertEqual(response.status_code, 200)

        doc = BeautifulSoup(response.content, "html.parser")

        tables = doc.find_all("table")
        self.assertEqual(len(tables), 1)

        trs = tables[0].tbody.find_all("tr")
        self.assertEqual(len(trs), 2)

        self.assertEqual(
            str(trs[1].find_all("td")[1].i),
            '<i class="bi bi-crosshair" title="1.1"></i>',
        )

    def test_transaction_detail_chart_has_line(self):
        self.prepare_transaction()
        data = self.c.get(
            reverse("transactions:fetch_prices", args=[self.btc.pk])
        ).json()

        self.assertEqual(data["data"]["plotLines"][0]["value"], 1.1)
        self.assertEqual(
            data["data"]["plotLines"][0]["label"], {"text": "Target 1.1 USD"}
        )

    def test_transaction_detail_has_target(self):
        self.prepare_transaction()

        response = self.c.get(
            reverse(
                "transactions:open_detail",
                args=[1],
            )
        )

        self.assertEqual(response.status_code, 200)

        doc = BeautifulSoup(response.content, "html.parser")

        lists = doc.find_all("dl")
        self.assertEqual(len(lists), 2)

        self.assertEqual(lists[1].find_all("dt")[1].text, "Target")
        self.assertEqual(lists[1].find_all("dd")[1].text.strip(), "1.1 (10.0%)")

    def test_item_detail_transaction_list_has_target(self):
        self.prepare_transaction()
        response = self.c.get(
            reverse(
                "coins:coin_detail",
                args=[UserItem.objects.get(item=self.btc.pk, user=self.user).pk],
            )
        )
        doc = BeautifulSoup(response.content, "html.parser")

        tables = doc.find_all("table")
        self.assertEqual(len(tables), 1)

        trs = tables[0].tbody.find_all("tr")
        self.assertEqual(len(trs), 3)
        self.assertEqual(trs[1].find_all("td")[5].text, "1.1")


class DecimalFormatTestCase(BaseDecimalFormatTestCase):
    fixtures = ["sitetree"]

    def test_overview_chart_data(self):
        context = self.c.get(reverse("transactions:open")).context
        self.assertTrue(
            f"toFixed({self.get_decimals('AMD')})",
            context["transactions_overview_data"]["share"]["options"],
        )
        self.assertTrue(
            f"toFixed({self.get_decimals('SPLG')})",
            context["transactions_overview_data"]["etf"]["options"],
        )
        self.assertTrue(
            f"toFixed({self.get_decimals('BTC')})",
            context["transactions_overview_data"]["coin"]["options"],
        )

    def test_column_chart_data(self):
        context = self.c.get(reverse("transactions:open")).context
        data = context["basic_stats_data"]["investment_and_revenue"]

        for item in data["share"]["USD"]:
            for value in item["data"]:
                self.assert_format("AMD", str(value))

        for item in data["coin"]["USD"]:
            for value in item["data"]:
                self.assert_format("BTC", str(value))

        for item in data["etf"]["USD"]:
            for value in item["data"]:
                self.assert_format("SPLG", str(value))

    def test_pie_chart_data(self):
        context = self.c.get(reverse("transactions:open")).context
        data = context["basic_stats_data"]["profit"]

        if "share" in data:
            for item in data["share"]["USD"][0]["data"]:
                self.assert_format("AMD", str(item["y"]))

        if "coin" in data:
            for item in data["coin"]["USD"][0]["data"]:
                self.assert_format("BTC", str(item["y"]))

        if "etf" in data:
            for item in data["etf"]["USD"][0]["data"]:
                self.assert_format("SPLG", str(item["y"]))

    def test_text_stats_data(self):
        context = self.c.get(reverse("transactions:open")).context
        data = context["basic_stats_data"]["text"]

        for _, value in data["share"]["USD"].items():
            self.assert_format("AMD", str(value))

        for _, value in data["coin"]["USD"].items():
            self.assert_format("BTC", str(value))

        for _, value in data["etf"]["USD"].items():
            self.assert_format("SPLG", str(value))

    def test_detail_column_chart(self):
        for trans in Transaction.objects.all():
            data = self.c.get(
                reverse("transactions:fetch_prices", args=[trans.pk])
            ).json()

            self.assertTrue(data["status"])

            for item in data["data"]["item"]:
                self.assert_format(trans.item.symbol, str(item[1]))

    def test_graphs(self):
        for trans in Transaction.objects.exclude(item__coin__isnull=True):
            graph = Graph(trans)
            graph.generate()

            self.assert_format(
                trans.item.symbol,
                re.search("[0-9\.]+/[0-9\.]+", graph.dot.source)[0].split("/")[1],
            )


class AssetsDecimalFormatTestCase(BaseDecimalFormatTestCase):
    def test_shares_profit_chart(self):
        self.perform_test_assets_chart(
            reverse("transactions:shares_profits_chart_fetch", args=["USD"])
        )

    def test_shares_asset_allocation_chart(self):
        self.perform_test_assets_chart(
            reverse("transactions:shares_asset_alloc_chart_fetch", args=["USD"])
        )

    def test_etfs_profit_chart(self):
        self.perform_test_assets_chart(
            reverse("transactions:etfs_profits_chart_fetch", args=["USD"])
        )

    def test_etfs_asset_allocation_chart(self):
        self.perform_test_assets_chart(
            reverse("transactions:etfs_asset_alloc_chart_fetch", args=["USD"])
        )

    def test_coins_profit_chart(self):
        self.perform_test_assets_chart(
            reverse("transactions:coins_profits_chart_fetch", args=["USD"])
        )

    def test_coins_asset_allocation_chart(self):
        self.perform_test_assets_chart(
            reverse("transactions:coins_asset_alloc_chart_fetch", args=["USD"])
        )
