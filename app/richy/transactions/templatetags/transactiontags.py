from django import template
from django.urls import reverse

register = template.Library()


@register.simple_tag
def average_price(transactions):
    return sum([t.get_value() for t in transactions]) / sum(
        [t.amount for t in transactions]
    )


@register.simple_tag
def transaction_url(transaction):
    return reverse(
        "transactions:{}_detail".format("closed" if transaction.is_closed else "open"),
        args=[transaction.pk],
    )
