from django.urls import path

from . import views

app_name = "transactions"
urlpatterns = [
    path("", views.IndexRedirectView.as_view(), name="index"),
    path("open/", views.OpenCreateView.as_view(), name="open"),
    path(
        "open/detail/<int:pk>/",
        views.OpenTransactionDetailView.as_view(),
        name="open_detail",
    ),
    path("closed/", views.ClosedTemplateView.as_view(), name="closed"),
    path(
        "closed/detail/<int:pk>/",
        views.ClosedTransactionDetailView.as_view(),
        name="closed_detail",
    ),
    path("assets/", views.AssetsTemplateView.as_view(), name="assets"),
    path(
        "assets/shares/chart/<str:currency>/fetch/",
        views.FetchSharesAssetAllocationChartDataAjaxView.as_view(),
        name="shares_asset_alloc_chart_fetch",
    ),
    path(
        "assets/shares/chart/profits/<str:currency>/fetch/",
        views.FetchSharesProfitsChartDataAjaxView.as_view(),
        name="shares_profits_chart_fetch",
    ),
    path(
        "assets/etfs/chart/profits/<str:currency>/fetch/",
        views.FetchEtfsProfitsChartDataAjaxView.as_view(),
        name="etfs_profits_chart_fetch",
    ),
    path(
        "assets/etfs/chart/<str:currency>/fetch/",
        views.FetchEtfsAssetAllocationChartDataAjaxView.as_view(),
        name="etfs_asset_alloc_chart_fetch",
    ),
    path(
        "assets/coins/chart/<str:currency>/fetch/",
        views.FetchCoinsAssetAllocationChartDataAjaxView.as_view(),
        name="coins_asset_alloc_chart_fetch",
    ),
    path(
        "assets/coins/chart/profits/<str:currency>/fetch/",
        views.FetchCoinsProfitsChartDataAjaxView.as_view(),
        name="coins_profits_chart_fetch",
    ),
    path("update/<int:pk>/", views.TransactionUpdateView.as_view(), name="update"),
    path(
        "update/post-closed-or-closing/<int:pk>/",
        views.PostClosedOrClosingTransactionUpdateFormView.as_view(),
        name="post_closed_or_closing_form",
    ),
    path(
        "delete/<int:pk>/",
        views.DeleteTransactionRedirectView.as_view(),
        name="delete",
    ),
    path(
        "detail/<int:pk>/fetch-prices/",
        views.FetchItemPricesAjaxView.as_view(),
        name="fetch_prices",
    ),
    path(
        "dividend/detail/<int:pk>/",
        views.DividendTransactionDetailView.as_view(),
        name="dividend_detail",
    ),
    path(
        "attachment/delete/<int:pk>/",
        views.DeleteAttachmentRedirectView.as_view(),
        name="delete_attachment",
    ),
]
