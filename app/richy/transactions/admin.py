from django.contrib import admin

from .models import Exchange, Transaction
from ..core.models import Asset


@admin.register(Exchange)
class ExchangeAdmin(admin.ModelAdmin):
    list_display = ["title"]


class AssetInline(admin.TabularInline):
    model = Asset
    extra = 0


@admin.register(Transaction)
class TransactionAdmin(admin.ModelAdmin):
    list_display = [
        "date",
        "item",
        "exchange",
        "amount",
        "fee",
        "is_deposit",
        "is_closing",
        "is_closed",
    ]
    ordering = ["-date"]
    inlines = [AssetInline]
