import logging
from datetime import date, timedelta
from unittest.mock import patch

from django.urls import reverse
from django.utils import timezone
from karpet import Karpet

from ..core.models import Price, UserItem
from ..core.scraper import CurrentPrice
from ..core.tests import (
    BaseCacheTestCase,
    BaseDecimalFormatTestCase,
    BaseDeleteUserItemTestCase,
    BaseDrawdownAndAthsPageTestCase,
    BaseDrawdownTestCase,
    BaseItemOverviewAllocationTestCase,
    BaseTestCase,
    create_items,
    create_user_items,
)
from ..transactions.models import Transaction
from .models import Coin
from .tasks import fetch_current_price, fetch_historical_data

LOGGER = logging.getLogger(__name__)


class HistoricalDataTestCase(BaseTestCase):
    def test_fetch_historical_data_without_transactions(self):
        """
        Tests fetching historical data (prices) of a share
        without any transactions in the database.
        """

        # Let's operate with BTC ticket which should
        # have more than 1000 historical records.
        create_items()
        fetch_historical_data(Coin.objects.get(symbol="BTC").pk)

        count = Price.objects.count()
        LOGGER.debug(f"Downloaded {count} historical records.")

        # Let's consider more than 1000 results as success.
        self.assertTrue(1000 < count)

    def test_fetch_historical_data_with_transactions(self):
        """
        Tests fetching historical data (prices) of a share
        with some transactions in the database.
        """

        create_items()
        # Let's operate with BTC ticket which should
        # have more than 1000 historical records.
        c = Coin.objects.get(symbol="BTC")

        # Also create a transaction.
        t = Transaction(
            item=c,
            price=3500,  # pure guess
            amount=1,
            fee=10,
            date=date.today() - timedelta(days=300),
            is_deposit=True,
            user=self.user,
        )
        t.save()

        fetch_historical_data(c.pk)

        count = Price.objects.count()
        LOGGER.debug(f"Downloaded {count} historical records.")

        # Let's consider more than 1000 results as success.
        self.assertTrue(1000 < count)


class CoinFormTestCase(BaseTestCase):
    def test_delete_coin(self):
        """
        Test if a coin can be deleted.
        """

        create_items()
        create_user_items()
        all_coins = (
            UserItem.objects.by_user(self.user).filter(item__coin__isnull=False).count()
        )
        item = UserItem.objects.get(item__symbol="BTC")

        response = self.c.get(reverse("coins:delete_coin", args=[item.pk]))
        self.assertEqual(response.status_code, 302)
        self.assertEqual(
            UserItem.objects.by_user(self.user)
            .filter(item__coin__isnull=False)
            .count(),
            all_coins - 1,
        )

    def test_delete_coin_with_transaction(self):
        """
        Test if a coin with a transaction cannot be deleted.
        """

        create_items()
        create_user_items()
        all_coins = Coin.objects.count()
        item = UserItem.objects.get(item__symbol="BTC")
        trans = Transaction(
            item=item.item,
            price=1.0,
            amount=1.0,
            fee=0.0,
            date=date.today(),
            user=self.user,
        )
        trans.save()

        response = self.c.get(reverse("coins:delete_coin", args=[item.pk]))
        self.assertEqual(response.status_code, 302)
        self.assertEqual(Coin.objects.count(), all_coins)

    def test_archive_coin(self):
        """
        Test if a coin without open transactions can be archived.
        """

        create_items()
        create_user_items()
        item = UserItem.objects.get(item__symbol="BTC")

        response = self.c.post(
            reverse("coins:update_coin", args=[item.pk]),
            data={
                "symbol": "BTC",
                "coin_id": item.item.coin.coin_id,
                "is_archived": True,
            },
        )
        item.refresh_from_db()
        self.assertEqual(response.status_code, 302)
        self.assertTrue(item.is_archived)

    def test_archive_coin_with_open_transaction(self):
        """
        Test if a coin with open transactions can be archived.
        """

        create_items()
        create_user_items()
        item = UserItem.objects.get(item__symbol="BTC")
        trans = Transaction(
            item=item.item,
            price=1.0,
            amount=1.0,
            fee=0.0,
            date=date.today(),
            user=self.user,
        )
        trans.save()

        response = self.c.post(
            reverse("coins:update_coin", args=[item.pk]),
            data={"symbol": "BTC", "is_archived": True},
        )
        item.refresh_from_db()
        self.assertEqual(response.status_code, 200)
        self.assertFalse(item.is_archived)

    @patch.object(Karpet, "get_coin_slugs", return_value=["bitcoin"])
    def test_archive_coin_with_closed_transaction(self, _mocked_get_coin_slugs):
        """
        Test if a coin with closed transactions can be archived.
        """

        create_items()
        create_user_items()

        item = UserItem.objects.get(item__symbol="BTC")
        trans = Transaction(
            item=item.item,
            price=1.0,
            amount=1.0,
            fee=0.0,
            date=date.today(),
            is_closed=True,
            user=self.user,
        )
        trans.save()

        response = self.c.post(
            reverse("coins:update_coin", args=[item.pk]),
            data={
                "symbol": "BTC",
                "coin_id": item.item.coin.coin_id,
                "is_archived": True,
            },
        )
        item.refresh_from_db()
        self.assertEqual(response.status_code, 302)
        self.assertTrue(item.is_archived)

    def test_ambiguous_coin_id(self):
        """
        Test if a coin without coin ID which has multiple IDs won't be added.
        """

        response = self.c.post(
            reverse("coins:overview"),
            data={"symbol": "sta"},
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(0, Coin.objects.count())

    @patch.object(
        Karpet, "get_coin_slugs", return_value=["defi-stoa", "stable-asset", "statera"]
    )
    def test_wrong_coin_id(self, _mocked_get_coin_ids):
        """
        Test if a coin with wrong ID won't get added.
        """

        response = self.c.post(
            reverse("coins:overview"),
            data={"symbol": "sta", "coin_id": "abcd123991"},
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(0, Coin.objects.count())

    @patch.object(
        Karpet, "get_coin_slugs", return_value=["defi-stoa", "stable-asset", "statera"]
    )
    def test_correct_coin_id(self, _mocked_get_coin_slugs):
        """
        Test if a coin with specified correct ID gets added.
        """

        response = self.c.post(
            reverse("coins:overview"),
            data={"symbol": "sta", "coin_id": "statera"},
        )
        self.assertEqual(response.status_code, 302)
        self.assertEqual(1, Coin.objects.count())


class CurrentPriceTestCase(BaseCacheTestCase):
    @patch("karpet.Karpet.get_basic_info")
    def test_coin(self, mock):
        """
        Tests coin current price and market state caching.
        """

        # Ethereum data for 6.3.2022
        mock.return_value = {
            "name": "Ethereum",
            "current_price": 2605.99,
            "market_cap": 312326949561,
            "rank": 2,
            "reddit_average_posts_48h": 168.182,
            "reddit_average_comments_48h": 168.182,
            "reddit_subscribers": 1236735,
            "reddit_accounts_active_48h": 371.0,
            "forks": 13308,
            "stars": 35878,
            "total_issues": 6239,
            "closed_issues": 6021,
            "pull_request_contributors": 634,
            "commit_count_4_weeks": 26,
            "year_low": 1581.631055931366,
            "year_high": 4815.004634322234,
            "yoy_change": 58.784309880619446,
            "price_change_24": -39.09575632639,
            "price_change_24_percents": -1.47805,
            "open_issues": 218,
        }
        create_items()

        coin = Coin.objects.get(symbol="ETH")
        fetch_current_price(coin.pk)
        price = coin.get_last_price(current=True)

        self.assertEqual(price.price, mock.return_value["current_price"])
        self.assertEqual(price.state, "open")


class DeleteTestCase(BaseDeleteUserItemTestCase):
    model = Coin
    url = "coins:delete_coin"
    target_url = "coins:overview"
    symbol = "TRX"
    task = fetch_historical_data

    def test(self):
        self.spawn()


class OverviewAllocationViewTestCase(BaseItemOverviewAllocationTestCase):
    def test_allocation(self):
        self.prepare(
            [
                [str(timezone.now().date()), "BTC", 1],
                [str(timezone.now().date()), "ETH", 2],
            ]
        )

        btc = Coin.objects.get(symbol="BTC")
        eth = Coin.objects.get(symbol="ETH")

        self.spawn(
            [
                [
                    {
                        "item": btc,
                        "price": 1,
                        "amount": 5,
                        "fee": 0,
                        "date": "2023-01-01",
                        "is_deposit": True,
                        "user": self.user,
                    },
                    {},
                ],
                [
                    {
                        "item": eth,
                        "price": 2,
                        "amount": 2,
                        "fee": 0,
                        "date": "2023-01-01",
                        "is_deposit": True,
                        "user": self.user,
                    },
                    {},
                ],
            ],
            "coins:overview",
        )


class CoinDecimalFormatTestCase(BaseDecimalFormatTestCase):
    def test_price(self):
        self.perform_test_price_format(reverse("coins:overview"))

    def test_overview_chart_price(self):
        self.perform_test_overview_chart_price_format(reverse("coins:overview_fetch"))

    def test_item_detail_current_price(self):
        for user_item in self.user.useritem_set.filter(item__coin__isnull=False):
            user_item.item.set_current_price_and_change(
                CurrentPrice(
                    price=self.generate_price(),
                    change_value=self.generate_price_change(),
                    change_percents=self.generate_price_change(),
                )
            )
            self.perform_test_item_detail_current_price_format(
                user_item.item.symbol, reverse("coins:coin_detail", args=[user_item.pk])
            )

    def test_item_detail_yoy_change(self):
        for user_item in self.user.useritem_set.filter(item__coin__isnull=False):
            self.perform_test_item_detail_yoy_change(
                user_item.item,
                reverse("coins:coin_detail", args=[user_item.item.pk]),
            )

    def test_item_detail_ath(self):
        for user_item in self.user.useritem_set.filter(item__coin__isnull=False):
            self.perform_test_item_detail_ath(
                user_item.item, reverse("coins:coin_detail", args=[user_item.pk])
            )

    def test_item_detail_year_low_high(self):
        for user_item in self.user.useritem_set.filter(item__coin__isnull=False):
            self.perform_test_item_detail_year_low_high(
                user_item.item, reverse("coins:coin_detail", args=[user_item.pk]), 2
            )

    def test_item_detail_chart_data(self):
        for user_item in self.user.useritem_set.filter(item__coin__isnull=False):
            self.perform_test_item_detail_chart_price_format(
                user_item.item.symbol,
                reverse("coins:coin_fetch", args=[user_item.pk]),
            )

    def test_item_performance_chart_data(self):
        for user_item in self.user.useritem_set.filter(item__coin__isnull=False):
            self.perform_test_item_performance_chart_price_format(
                user_item.item.symbol,
                reverse("coins:performance", args=[user_item.pk]),
            )

    def test_item_performance_table_data(self):
        for user_item in self.user.useritem_set.filter(item__coin__isnull=False):
            self.perform_test_item_performance_table_price_format(
                user_item.item.symbol,
                reverse("coins:performance", args=[user_item.pk]),
            )


class DrawdownTestCase(BaseDrawdownTestCase):
    model = Coin
    url_pattern = "coins:coin_detail"
    basic_info_position = 3

    def test_chart_data(self):
        self.spawn_basic_info_data()

    def test_basic_info_data(self):
        self.spawn_basic_info_data()


class DrawdownAndAthsPageTestCase(BaseDrawdownAndAthsPageTestCase):
    model = Coin
    url_pattern = "coins:drawdown_periods_and_aths"

    def test_drawdown_page_table(self):
        self.spawn_drawdown_page_table()

    def test_drawdown_page_chart(self):
        self.spawn_drawdown_page_chart()
