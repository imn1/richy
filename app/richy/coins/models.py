from django.db import models
from django.utils.translation import gettext as _

from ..core.models import Item


class Coin(Item):
    coin_id = models.CharField(
        _("Coin ID"), max_length=50, blank=True, null=True, unique=True
    )

    def is_share(self):
        return False

    def is_index(self):
        return False

    def is_etf(self):
        return False

    def is_coin(self):
        return True
