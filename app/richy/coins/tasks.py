import logging
import time
from datetime import datetime

import pytz
from django.core.cache import cache
from django.utils import timezone

from ..celery import app
from ..core.models import Meta, Price
from ..core.scraper import Manager
from ..core.tasks import generate_performance_charts
from ..transactions.transactions import Performance, Transactions
from .models import Coin

LOGGER = logging.getLogger("richy.celery")


@app.task
def fetch_historical_data(coin=None):
    def fetch_coin(coin):
        """
        Fetches coin historical data.

        :param Coin coin: Coin model instance.
        """

        # 1. Download dataframe with prices.
        df = Manager.fetch_coin_prices(coin)

        # If not a dataframe was returned immediately terminate.
        if df.empty:
            LOGGER.debug("Got empty dataframe with coin prices - skipping.")
            return

        # 2. Save all prices into database.
        for index, row in df.iterrows():
            # Create/Update database price record.
            try:
                price, created = Price.objects.get_or_create(
                    item=coin,
                    datetime=datetime(
                        index.year, index.month, index.day, tzinfo=pytz.UTC
                    ),
                    defaults={"price": row["price"]},
                )

                if created:
                    LOGGER.debug(
                        f"Prices successfully saved to database for {coin.symbol}"
                        f" - date {price.datetime} - ID {price.pk}."
                    )
            except Exception:
                LOGGER.exception(
                    f"Prices weren't saved to database for {coin.symbol} - date {index}!"
                )

    def update_caches(coin):
        """
        Delete and/or updates related caches.

        :param Coin coin: Coin model instance.
        """

        # SMAs, perc. change, ...
        coin.update_cache()

        # Assets + performance charts.
        cache.delete("coins-transaction-performance-and-assets")
        cache.delete("coins-transaction-profit-performance")

        Performance("coin", profit=False, use_cache=False).get_data()
        Performance("coin", profit=True, use_cache=False).get_data()

        # Transactions stats.
        Transactions.update_cache()

    # Fetch coin.
    if coin:
        coin = Coin.objects.get(pk=coin)
        fetch_coin(coin)
        update_caches(coin)
        generate_performance_charts(coin.pk)

    # Fetch all coins in the database.
    else:
        for coin_item in Coin.objects.order_by("symbol"):
            try:
                fetch_coin(coin_item)
            except Exception:
                LOGGER.exception(
                    "Couldn't fetch coin historical data.", extra={"coin": coin_item}
                )
                LOGGER.debug(f"Coin: {coin_item.symbol} ^")
            else:
                update_caches(coin_item)
                generate_performance_charts(coin_item.pk)

            # Slow down for a while.
            time.sleep(10)

    # Save "last price update" meta stamp.
    Meta.objects.update_or_create(
        type=Meta.LAST_PRICE_UPDATE, defaults={"value": timezone.now()}
    )


@app.task(queue="fast")
def fetch_basic_info(coin=None):
    """
    Downloads basic info and saves it to ShareData models.

    :param int coin: Coin PK.
    """

    def fetch_coin(coin):
        # Download data.
        basic_info = Manager.get_coin_basic_info(coin)
        coin.set_basic_info(basic_info)

        LOGGER.debug(f"Basic data were created/updated for coin {coin.symbol}")

    if coin:
        fetch_coin(Coin.objects.get(pk=coin))
    else:
        for coin in Coin.objects.all():
            try:
                fetch_coin(coin)
            except Exception:
                LOGGER.exception(
                    "Couldn't download coin basic info",
                    extra={"coin": coin},
                )
                LOGGER.debug(f"Coin: {coin} ^")

            time.sleep(10)  # Based on coingecko.com API rate limit.


@app.task(queue="fast")
def fetch_current_price(coin=None):
    """
    Fetches current market price and caches
    it under "item-{}-current-price" key for one hour.
    If no coin PK instance is given fetches all coins in database.
    Sleeps 10 seconds between each fetch.

    :param int coin: Coin PK.
    """

    def fetch_coin(coin):
        price = Manager.get_current_price_and_change(coin)

        if price:
            coin.set_current_price_and_change(price)

    if coin:
        fetch_coin(Coin.objects.get(pk=coin))
    else:
        for c in Coin.objects.all():
            try:
                fetch_coin(c)
            except Exception:
                LOGGER.exception("Couldn't fetch coin current price", extra={"coin": c})
                LOGGER.debug(f"Coin: {c} ^")

            time.sleep(5)
