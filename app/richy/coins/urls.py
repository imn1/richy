from django.urls import path

from . import views

app_name = "coins"
urlpatterns = [
    path("", views.OverviewCreateView.as_view(), name="overview"),
    path(
        "overview/fetch/", views.FetchOverviewAjaxView.as_view(), name="overview_fetch"
    ),
    path("coin/<int:pk>/", views.CoinDetailView.as_view(), name="coin_detail"),
    path("coin/<int:pk>/update/", views.CoinUpdateView.as_view(), name="update_coin"),
    path(
        "coin/<int:pk>/delete/",
        views.DeleteCoinRedirectView.as_view(),
        name="delete_coin",
    ),
    path(
        "coin/<int:pk>/reset/", views.ResetCoinRedirectView.as_view(), name="reset_coin"
    ),
    # path("share/<int:pk>/chart/linear-regression/", views.FetchLinearRegressionAjaxView.as_view(), name="chart_linear_regression"),
    path("coin/<int:pk>/fetch/", views.FetchCoinAjaxView.as_view(), name="coin_fetch"),
    path(
        "share/<int:pk>/performance/",
        views.PerformanceDetailView.as_view(),
        name="performance",
    ),
    path(
        "coin/<int:pk>/performance/",
        views.PerformanceDetailView.as_view(),
        name="performance",
    ),
    path(
        "coin/<int:pk>/drawdown-periods-and-aths/",
        views.DrawdownPeriodsAndAthsTemplateView.as_view(),
        name="drawdown_periods_and_aths",
    ),
    path("coin/<int:pk>/the-eye/", views.CoinTheEyeView.as_view(), name="the_eye"),
    path(
        "coin/<int:pk>/historical-data/",
        views.HistoricalDataListView.as_view(),
        name="historical_data",
    ),
]
