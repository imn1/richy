import logging
from datetime import date, timedelta

from braces.views import FormMessagesMixin, LoginRequiredMixin
from django.conf import settings
from django.contrib import messages
from django.db.models import Avg, Max, Min
from django.shortcuts import get_object_or_404
from django.urls import reverse, reverse_lazy
from django.utils.translation import gettext as _
from django.views.generic import RedirectView, UpdateView

from ..core.charts import OpenStakingsRatioPieChart
from ..core.math import calc_percentage_change
from ..core.models import UserItem
from ..core.views import (
    BaseDeleteUserItemRedirectView,
    BaseHistoricalData,
    BaseDrawdownPeriodsAndAthsDetailView,
    BaseFetchItemAjaxView,
    BaseItemDetailView,
    BasePerformanceDetailView,
    BaseURLMixin,
    BaseUserItemCreateView,
    SubmenuViewMixin,
    UserItemManipulationMixin,
    BaseTheEyeView,
)
from ..news.tasks import download_coin_news
from .forms import UserCoinForm
from .tasks import fetch_basic_info, fetch_historical_data

logger = logging.getLogger(__name__)


class FetchDataMixin:
    def fetch_data(self, pk):
        fetch_historical_data.delay(pk)
        download_coin_news.delay(pk)
        fetch_basic_info.delay(pk)


class CoinChildrenSubmenuViewMixin(SubmenuViewMixin):
    # FIXME: is_current needs to be detected based on current URL (or so)
    def get_submenu(self):
        items = []

        # Performance
        items.append(
            {
                "url": reverse("coins:performance", args=[self.object.pk]),
                "title": _("Performance"),
                "is_current": False,
            }
        )
        # Drawdowns and ATHs
        items.append(
            {
                "url": reverse(
                    "coins:drawdown_periods_and_aths", args=[self.object.pk]
                ),
                "title": _("Drawdowns and ATHs"),
                "is_current": False,
            }
        )
        # Historical data
        items.append(
            {
                "url": reverse("coins:historical_data", args=[self.object.pk]),
                "title": _("Historical data"),
                "is_current": False,
            }
        )
        # The eye
        items.append(
            {
                "url": reverse("coins:the_eye", args=[self.object.pk]),
                "title": _("The Eye"),
                "is_current": False,
            }
        )

        return items


class OverviewCreateView(FetchDataMixin, BaseUserItemCreateView):
    form_class = UserCoinForm
    template_name = "coins/overview.pug"
    success_url = reverse_lazy("coins:overview")
    form_valid_message = _("Coin has been saved.")
    form_invalid_message = _("Coin hasn't been saved. Please check the form.")

    def item_created_callback(self, form, item):
        item.coin.coin_id = form.cleaned_data["coin_id"]
        item.coin.save()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        # Fetch shares.
        coin_list = (
            UserItem.objects.select_related("item")
            .by_user(self.request.user)
            .filter(item__coin__isnull=False)
            .order_by("is_archived", "item__symbol")
        )
        context["staking_ratio_chart_options"] = self.get_staking_chart_options()
        context["possession_stats"] = self.get_open_items_possession_stats("coin")
        open_coins_list, context["coin_list"] = self.split_open_items(coin_list)

        # Sort open shares based on possession_stats (which are
        # sorted by percentage value).
        context["open_coins_list"] = self.sort_items_by_other_dict_keys(
            open_coins_list, context["possession_stats"]
        )

        return context

    def get_staking_chart_options(self):
        data = OpenStakingsRatioPieChart(self.request.user).get_series()

        if data:
            return {
                "title": False,
                "chart": {"type": "pie"},
                "colors": settings.CHART_COLORS,
                "plotOptions": {
                    "pie": {
                        "size": "50%",
                        "dataLabels": {
                            "enabled": False,
                        },
                        "showInLegend": True,
                    }
                },
                "tooltip": {"pointFormat": "<b>{point.percentage:.1f} %</b>"},
                "series": [{"name": _("Staked vs. Non-staked"), "data": data}],
            }


class FetchOverviewAjaxView(BaseFetchItemAjaxView):
    def get(self, request, *args, **kwargs):
        series = []

        for c in UserItem.objects.by_user(self.request.user).filter(
            item__coin__isnull=False, show_in_overview=True, is_archived=False
        ):
            series.append(
                {
                    "name": c.item.symbol,
                    "data": self.get_item_prices(
                        c.item,
                        datetime__date__gte=date.today()
                        - timedelta(days=settings.OVERVIEW_COINS_HISTORY),
                    ),
                }
            )

        return self.send(True, {"item": series})


class CoinDetailView(BaseURLMixin, BaseItemDetailView):
    model = UserItem
    template_name = "coins/detail.pug"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        # Day sums.
        context["day_sums"] = self.get_day_sums()

        return context

    def get_day_sums(self):
        """
        Calculates moving averages (50, 200, 365), "vs today" for 50, 200
        days period and min/max for a year.

        :return: The sums in a dict with keys 50, 200, 365.
        :rtype: dict
        """

        # Moving averages + percentage changes.
        day_sums = {
            50: {
                "ma": self.object.item.price_set.filter(
                    datetime__date__gte=date.today() - timedelta(days=50)
                ).aggregate(Avg("price"))["price__avg"]
            },
            200: {
                "ma": self.object.item.price_set.filter(
                    datetime__date__gte=date.today() - timedelta(days=200)
                ).aggregate(Avg("price"))["price__avg"]
            },
            365: {
                "min": self.object.item.price_set.filter(
                    datetime__date__gte=date.today() - timedelta(days=365)
                ).aggregate(Min("price"))["price__min"],
                "max": self.object.item.price_set.filter(
                    datetime__date__gte=date.today() - timedelta(days=365)
                ).aggregate(Max("price"))["price__max"],
            },
        }

        try:
            day_sums[50]["vs_today"] = calc_percentage_change(
                day_sums[50]["ma"], self.object.price_set.last().price
            )
            day_sums[200]["vs_today"] = calc_percentage_change(
                day_sums[200]["ma"], self.object.price_set.last().price
            )
        except Exception:
            pass

        return day_sums


class FetchCoinAjaxView(BaseFetchItemAjaxView):
    def get(self, request, *args, **kwargs):
        coin = get_object_or_404(UserItem, pk=kwargs["pk"])
        data = {"item": []}

        # Fetch main series - price series - "coin".
        data["item"] = self.get_item_prices(coin.item)

        # Lookup for transactions.
        if request.GET.get("transactions"):
            data["transactions"] = self.get_transactions(coin.item)

        # SMAs.
        if request.GET.get("smas"):
            if 20 <= len(data["item"]):
                data["ma20"] = coin.item.get_sma(20)

            if 50 <= len(data["item"]):
                data["ma50"] = coin.item.get_sma(50)

            if 200 <= len(data["item"]):
                data["ma200"] = coin.item.get_sma(200)

        return self.send(True, data)


class CoinUpdateView(
    LoginRequiredMixin,
    FormMessagesMixin,
    UserItemManipulationMixin,
    FetchDataMixin,
    UpdateView,
):
    model = UserItem
    form_class = UserCoinForm
    template_name = "coins/update.pug"
    success_url = reverse_lazy("coins:overview")
    form_valid_message = _("Coin has been saved.")
    form_invalid_message = _("Coin hasn't been saved. Please check the form.")

    def form_valid(self, form):
        original = UserItem.objects.get(pk=form.instance.pk)

        # Unarchiving -> fetch all the data.
        if original.is_archived and not form.instance.is_archived:
            self.fetch_data(form.instance.pk)

        return super().form_valid(form)


class ResetCoinRedirectView(LoginRequiredMixin, FetchDataMixin, RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        item = get_object_or_404(
            UserItem,
            pk=self.kwargs["pk"],
            user=self.request.user,
            item__coin__isnull=False,
        )
        item.item.price_set.all().delete()
        self.fetch_data(item.item.pk)

        messages.success(self.request, _("Coin prices are being reset."))

        return reverse("coins:overview")


class DeleteCoinRedirectView(BaseDeleteUserItemRedirectView):
    def get_redirect_url(self, *args, **kwargs):
        if self.can_be_deleted():
            self.delete()

            messages.success(self.request, _("Coin has been deleted."))

        else:
            messages.error(
                self.request,
                _("Coin has open/closed transaction(s) thus cannot be deleted."),
            )

        return reverse("coins:overview")


class PerformanceDetailView(CoinChildrenSubmenuViewMixin, BasePerformanceDetailView):
    pass


class DrawdownPeriodsAndAthsTemplateView(
    CoinChildrenSubmenuViewMixin, BaseDrawdownPeriodsAndAthsDetailView
):
    pass


class CoinTheEyeView(CoinChildrenSubmenuViewMixin, BaseTheEyeView):
    pass


class HistoricalDataListView(CoinChildrenSubmenuViewMixin, BaseHistoricalData): ...
