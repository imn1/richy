from django.urls import path

from . import views

app_name = "staking"
urlpatterns = [
    path("", views.IndexRedirectView.as_view(), name="index"),
    path("open/", views.OpenCreateView.as_view(), name="open"),
    path("closed/", views.ClosedTemplateView.as_view(), name="closed"),
    path("delete/<int:pk>/", views.DeleteStakingRedirectView.as_view(), name="delete"),
    path("update/<int:pk>/", views.StakingUpdateView.as_view(), name="update"),
    path(
        "attachment/delete/<int:pk>/",
        views.DeleteAttachmentRedirectView.as_view(),
        name="delete_attachment",
    ),
]
