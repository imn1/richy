import io
from pathlib import Path

from django.urls import reverse

from ..coins.models import Coin
from ..core.models import Item
from ..core.tests import (
    BaseTestCase,
    create_exchanges,
    create_items,
    create_transactions,
    create_user_items,
)
from ..transactions.models import Exchange, Transaction
from .models import Attachment, Staking


class StakingFormTestCase(BaseTestCase):
    def test_create_staking_with_attachments(self):
        create_exchanges()
        binance = Exchange.objects.get(title="Binance")
        create_items()
        create_user_items()
        create_transactions(
            [
                [
                    {
                        "item": Coin.objects.get(symbol="BTC"),
                        "price": 1,
                        "amount": 10,
                        "fee": 0,
                        "exchange": binance,
                        "date": "2020-01-01",
                        "is_deposit": True,
                        "user": self.user,
                    },
                    {},
                ]
            ]
        )

        attachment_1 = io.BytesIO(self.f.binary(length=64))
        attachment_1.name = "example_1.jpg"
        attachment_2 = io.BytesIO(self.f.binary(length=64))
        attachment_2.name = "example_2.jpg"

        response = self.c.post(
            reverse("staking:open"),
            data={
                "item": Item.objects.get(symbol="BTC").pk,
                "exchange": binance.pk,
                "start": "2021-01-01",
                "end": "2021-04-01",  # 90 days
                "amount": 10,
                "reward_amount": 1,
                "transaction": Transaction.objects.first().pk,
                "note": "test",
                "attachments": [attachment_1, attachment_2],
                "is_sold": False,
            },
        )

        self.assertEqual(response.status_code, 302)
        self.assertEqual(1, Staking.objects.count())
        self.assertEqual(2, Attachment.objects.count())

        for attachment in Attachment.objects.all():
            self.assertTrue(Path(attachment.file.path).exists())

        # Clean up attachment files.
        Staking.objects.first().delete()
