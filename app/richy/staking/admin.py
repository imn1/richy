from django.contrib import admin

from .models import Attachment, Staking


class AttachmentInline(admin.TabularInline):
    model = Attachment


@admin.register(Staking)
class StakingAdmin(admin.ModelAdmin):
    list_display = [
        "item",
        "exchange",
        "transaction",
        "start",
        "end",
        "amount",
        "reward_amount",
        "is_sold",
    ]
    inlines = [AttachmentInline]
