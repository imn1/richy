# ruff: noqa
import os

from .base import *

DEBUG = True

INSTALLED_APPS += ["debug_toolbar"]

# Make all loggers use the console and debug level.
for logger in LOGGING["loggers"]:
    LOGGING["loggers"][logger]["handlers"] = ["console"]


LOGGING["loggers"]["richy"]["level"] = "DEBUG"
LOGGING["loggers"]["richy.celery"]["level"] = "DEBUG"

# LOGGING["loggers"]["django.db.backends"] = {
#     "handlers": ["console"],
#     "level": "DEBUG",
#     "propagate": False,
# }

MIDDLEWARE += ["debug_toolbar.middleware.DebugToolbarMiddleware"]

DEBUG_TOOLBAR_CONFIG = {"SHOW_TOOLBAR_CALLBACK": lambda _: True}

os.environ["SENTRY_ENVIRONMENT"] = "development"
