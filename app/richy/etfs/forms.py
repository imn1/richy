from ..core.forms import BaseUserItemForm
from .models import Etf


class UserEtfForm(BaseUserItemForm):
    item_model = Etf
