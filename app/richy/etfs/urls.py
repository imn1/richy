from django.urls import path

from . import views

app_name = "etfs"
urlpatterns = [
    path("", views.OverviewCreateView.as_view(), name="overview"),
    path(
        "overview/fetch/", views.FetchOverviewAjaxView.as_view(), name="overview_fetch"
    ),
    path("etf/<int:pk>/", views.EtfDetailView.as_view(), name="etf_detail"),
    path("etfs/<int:pk>/update/", views.EtfUpdateView.as_view(), name="update_etf"),
    path(
        "etf/<int:pk>/delete/",
        views.DeleteEtfRedirectView.as_view(),
        name="delete_etf",
    ),
    path(
        "etf/<int:pk>/reset/",
        views.ResetEtfRedirectView.as_view(),
        name="reset_etf",
    ),
    path("etf/<int:pk>/fetch/", views.FetchEtfAjaxView.as_view(), name="etf_fetch"),
    path(
        "index/<int:pk>/performance/",
        views.PerformanceDetailView.as_view(),
        name="performance",
    ),
    path(
        "etf/<int:pk>/drawdown-periods-and-aths/",
        views.DrawdownPeriodsAndAthsTemplateView.as_view(),
        name="drawdown_periods_and_aths",
    ),
    path("etf/<int:pk>/the-eye/", views.EtfTheEyeView.as_view(), name="the_eye"),
    path(
        "etf/<int:pk>/historical-data/",
        views.HistoricalDataListView.as_view(),
        name="historical_data",
    ),
]
