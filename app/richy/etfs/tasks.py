import logging
import time
from datetime import datetime

import numpy as np
import pytz
from django.core.cache import cache
from django.utils import timezone

from ..celery import app
from ..core.models import Meta, Price
from ..core.scraper import Manager
from ..core.tasks import generate_performance_charts
from ..transactions.transactions import Performance, Transactions
from .models import Etf

LOGGER = logging.getLogger("richy.celery")


@app.task
def fetch_historical_data(etf=None):
    """
    Fetches new historical data (prices) for the given etf.
    If no etf was given fetches data for all etfs.

    If new data has been found we update caches and
    regenerate performance chart.

    :param int etfs: Etf PK.
    """

    def fetch_etf(etf):
        """
        Fetches etf historical data.

        :param Etf etf: Etf model instance.
        """

        LOGGER.debug(f"Downloading prices for {etf}.")
        df = Manager.fetch_etf_prices(etf)

        # Save prices into database.
        for index, r in df.iterrows():
            # # Check if price format is valid.
            # price = PManager.sanitize(r["Close"])

            # if price is False:
            #     LOGGER.debug(f"Price for {share.symbol} at {index} is not valid - it's \"{r['Close']}\". Skipping ...")
            #     continue

            try:
                price = float(r["Close"])
                assert not np.isnan(price)
            except Exception:
                LOGGER.debug(
                    "Price for {} at {} is not valid - it's {}. Skipping ...".format(
                        etf.symbol, index, r["Close"]
                    )
                )
                continue

            # Create/Update database price record.
            try:
                p, created = Price.objects.get_or_create(
                    item=etf,
                    datetime=datetime(
                        index.year, index.month, index.day, tzinfo=pytz.UTC
                    ),
                    defaults={"price": price},
                )

                if created:
                    LOGGER.debug(
                        "Prices successfully saved to database for {} - date {} - ID {}.".format(
                            etf.symbol, p.datetime, p.pk
                        )
                    )
            except Exception:
                LOGGER.exception(
                    f"Prices weren't saved to database for {etf.symbol} - date {index}!"
                )

    def update_caches(etf):
        """
        Delete and/or updates related caches.

        :param Etf etf: Etf model instance.
        """

        etf.update_cache()
        cache.delete("etfs-transaction-performance-and-assets")
        cache.delete("etfs-transaction-profit-performance")

        Performance("etf", profit=False, use_cache=False).get_data()
        Performance("etf", profit=True, use_cache=False).get_data()

        # Transactions stats.
        Transactions.update_cache()

    LOGGER.debug("Starting datasets downloader...")

    # Fetch specific etf.
    if etf:
        etf = Etf.objects.get(pk=etf)
        fetch_etf(etf)
        update_caches(etf)
        generate_performance_charts(etf.pk)

    # Or fetch all etfs in the database.
    else:
        for e in Etf.objects.order_by("symbol"):
            try:
                fetch_etf(e)
            except Exception:
                LOGGER.exception(
                    "Share historical data fetching has failed.", extra={"etf": e}
                )
                LOGGER.debug(f"Etf: {e.symbol} ^")
            else:
                update_caches(e)
                generate_performance_charts(e.pk)

    # Save "last price update" meta stamp.
    Meta.objects.update_or_create(
        type=Meta.LAST_PRICE_UPDATE, defaults={"value": timezone.now()}
    )


# @app.task
# def fetch_financial_data(share=None):
#     """
#     Fetches financial data for all or the given share. Financial
#     data are:
#     - earnings
#     - revenues
#     - ratings
#
#     :param int share: Share PK.
#     """
#
#     if share:
#         try:
#             Manager.fetch_financials(Share.objects.get(pk=share))
#         except:
#             LOGGER.exception("Couldn't fetch financial data", extra={"share": share})
#             LOGGER.debug(f"Share: {share.symbol} ^")
#     else:
#         for share in Share.objects.exclude(is_archived=True).order_by("symbol"):
#             try:
#                 Manager.fetch_financials(share)
#             except:
#                 LOGGER.exception(
#                     "Couldn't fetch financial data", extra={"share": share}
#                 )
#                 LOGGER.debug(f"Share: {share.symbol} ^")
#
#             time.sleep(5)


@app.task(queue="fast")
def fetch_basic_info(etf=None):
    """
    Downloads basic info and saves it to ItemData model.

    :param int etf: Etf PK.
    """

    def fetch_etf(etf):
        # Download data.
        basic_info = Manager.get_etf_basic_info(etf)
        basic_info_updated = {}

        # Update dict keys to be usable in templates.
        for key, item in basic_info.items():
            basic_info_updated[key.replace(" ", "_").lower()] = item

        etf.set_basic_info(basic_info_updated)

        LOGGER.debug(f"Basic data were created/updated for etf {etf.symbol}")

    if etf:
        fetch_etf(Etf.objects.get(pk=etf))
    else:
        for etf in Etf.objects.all():
            try:
                fetch_etf(etf)
            except Exception:
                LOGGER.exception("Couldn't fetch etf basic info", extra={"etf": etf})
                LOGGER.debug(f"Etf: {etf.symbol} ^")

            time.sleep(5)


@app.task(queue="fast")
def fetch_current_price(etf=None):
    """
    Fetches current market price and caches
    it under "item-{}-current-price" key for one hour.
    If no etf PK instance is given fetches all etfs in database.
    Sleeps 10 seconds between each fetch.

    :param int etf: Etf PK.
    """

    def fetch_etf(etf):
        price = Manager.get_current_price_and_change(etf)

        if price:
            etf.set_current_price_and_change(price)

    if etf:
        fetch_etf(Etf.objects.get(pk=etf))
    else:
        for e in Etf.objects.all():
            try:
                fetch_etf(e)
            except Exception:
                LOGGER.exception("Couldn't fetch etf current price", extra={"etf": e})
                LOGGER.debug(f"Etf: {e.symbol} ^")
            time.sleep(5)


@app.task(queue="fast")
def fetch_holdings(etf=None):
    """
    Downloads ETF holdings and saves it to ItemData model.

    :param int etf: Etf PK.
    """

    def fetch_etf(etf):
        # Download data.
        holdings = Manager.fetch_etf_holdings(etf)
        holdings.sort(
            key=lambda i: i[2] if isinstance(i[2], (float, int)) else 0.0, reverse=True
        )
        etf.set_holdings(holdings)

        LOGGER.debug(f"Holdings  were created/updated for etf {etf.symbol}")

    if etf:
        fetch_etf(Etf.objects.get(pk=etf))
    else:
        for etf in Etf.objects.all():
            try:
                fetch_etf(etf)
            except Exception:
                LOGGER.exception("Couldn't fetch etf holdings", extra={"etf": etf})
                LOGGER.debug(f"Etf: {etf.symbol} ^")

            time.sleep(5)
