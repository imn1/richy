from django.db import models

from ..core.models import Item, ItemData
from ..indexes.models import Index


class Etf(Item):
    indexes = models.ManyToManyField(Index, blank=True)

    def is_share(self):
        return False

    def is_index(self):
        return False

    def is_etf(self):
        return True

    def is_coin(self):
        return False

    # ItemData access methods.
    def set_holdings(self, data):
        try:
            record = self.itemdata
            record.data["holdings"] = data
        except ItemData.DoesNotExist:
            record = ItemData(item=self, data={"holdings": data})

        record.save()

    def get_holdings(self):
        try:
            return self.itemdata.data["holdings"]
        except (ItemData.DoesNotExist, KeyError):
            return None
