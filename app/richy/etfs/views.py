import logging
from datetime import date, timedelta

from braces.views import FormMessagesMixin, LoginRequiredMixin
from django.conf import settings
from django.contrib import messages
from django.shortcuts import get_object_or_404
from django.urls import reverse, reverse_lazy
from django.utils.translation import gettext as _
from django.views.generic import RedirectView, UpdateView

from ..core.models import UserItem
from ..core.views import (
    BaseDeleteUserItemRedirectView,
    BaseDrawdownPeriodsAndAthsDetailView,
    BaseHistoricalData,
    BaseFetchItemAjaxView,
    BaseItemDetailView,
    BasePerformanceDetailView,
    BaseUserItemCreateView,
    SubmenuViewMixin,
    UserItemManipulationMixin,
    BaseTheEyeView,
)
from ..news.tasks import download_etfs_news
from .forms import UserEtfForm
from .tasks import fetch_basic_info, fetch_historical_data

LOGGER = logging.getLogger(__name__)


class FetchDataMixin:
    """
    Mixin that ensures all etf data which needs to be downloaded.
    """

    def fetch_data(self, pk):
        fetch_basic_info.delay(pk)
        fetch_historical_data.delay(pk)
        download_etfs_news.delay(pk)


# class EtfSubmenuViewMixin(SubmenuViewMixin):
#
#     # FIXME: is_current needs to be detected based on current URL (or so)
#     def get_submenu(self):
#
#         items = []
#
#         # Dividends
#         if self.object.dividend_set.exists():
#
#             items.append(
#                 {
#                     "url": reverse("shares:dividends", args=[self.object.pk]),
#                     "title": "Dividends",
#                     "is_current": False,
#                 }
#             )
#
#         return items


class EtfSubmenuViewMixin(SubmenuViewMixin):
    def get_submenu(self):
        return [
            {
                "url": reverse("etfs:performance", args=[self.object.pk]),
                "title": _("Performance"),
                "is_current": False,
            },
            {
                "url": reverse("etfs:drawdown_periods_and_aths", args=[self.object.pk]),
                "title": _("Drawdowns and ATHs"),
                "is_current": False,
            },
            {
                "url": reverse("etfs:historical_data", args=[self.object.pk]),
                "title": _("Historical data"),
                "is_current": False,
            },
            {
                "url": reverse("etfs:the_eye", args=[self.object.pk]),
                "title": _("The Eye"),
                "is_current": False,
            },
        ]


class OverviewCreateView(FetchDataMixin, BaseUserItemCreateView):
    form_class = UserEtfForm
    template_name = "etfs/overview.pug"
    success_url = reverse_lazy("etfs:overview")
    form_valid_message = _("ETF has been saved.")
    form_invalid_message = _("ETF hasn't been saved. Please check the form.")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        # Fetch etfs.
        etf_list = (
            UserItem.objects.select_related("item")
            .by_user(self.request.user)
            .filter(item__etf__isnull=False)
            .order_by("is_archived", "item__symbol")
        )
        context["possession_stats"] = self.get_open_items_possession_stats("etf")
        open_etfs_list, context["etf_list"] = self.split_open_items(etf_list)

        # Sort open etfs based on possession_stats (which are
        # sorted by percentage value).
        context["open_etfs_list"] = self.sort_items_by_other_dict_keys(
            open_etfs_list, context["possession_stats"]
        )

        return context


class FetchOverviewAjaxView(BaseFetchItemAjaxView):
    def get(self, request, *args, **kwargs):
        series = []

        for e in UserItem.objects.by_user(self.request.user).filter(
            item__etf__isnull=False, show_in_overview=True, is_archived=False
        ):
            series.append(
                {
                    "name": e.item.symbol,
                    "data": self.get_item_prices(
                        e.item,
                        datetime__date__gte=date.today()
                        - timedelta(days=settings.OVERVIEW_ETFS_HISTORY),
                    ),
                }
            )

        return self.send(True, {"item": series})


class DeleteEtfRedirectView(BaseDeleteUserItemRedirectView):
    def get_redirect_url(self, *args, **kwargs):
        if self.can_be_deleted():
            self.delete()
            messages.success(self.request, _("ETF has been deleted."))

        else:
            messages.error(
                self.request,
                _("ETF has open/closed transaction(s) thus cannot be deleted."),
            )

        return reverse("etfs:overview")


class ResetEtfRedirectView(LoginRequiredMixin, FetchDataMixin, RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        item = get_object_or_404(
            UserItem,
            pk=self.kwargs["pk"],
            user=self.request.user,
            item__etf__isnull=False,
        )
        item.item.price_set.all().delete()
        self.fetch_data(item.item.pk)

        messages.success(self.request, _("ETF prices are being reset."))

        return reverse("etfs:overview")


class EtfUpdateView(
    LoginRequiredMixin,
    FormMessagesMixin,
    UserItemManipulationMixin,
    FetchDataMixin,
    UpdateView,
):
    model = UserItem
    form_class = UserEtfForm
    template_name = "etfs/update.pug"
    success_url = reverse_lazy("etfs:overview")
    form_valid_message = _("ETF has been saved.")
    form_invalid_message = _("ETF hasn't been saved. Please check the form.")

    def form_valid(self, form):
        original = UserItem.objects.get(pk=form.instance.pk, user=self.request.user)

        # Unarchiving -> fetch all the data.
        if original.is_archived and not form.instance.is_archived:
            self.fetch_data(form.instance.item.pk)

        return super().form_valid(form)


# class EtfDetailView(EtfSubmenuViewMixin, BaseItemDetailView):
class EtfDetailView(BaseItemDetailView):
    model = UserItem
    template_name = "etfs/detail.pug"

    def dispatch(self, request, *args, **kwargs):
        # Check if there are already basic data (ItemData model).
        # Probably being downloaded right now.
        if not self.get_object().item.get_basic_info():
            messages.warning(request, _("Basic data are not ready yet."))

        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        yoy_change = {
            "yoy_change": self.object.item.get_last_days_change(365, percents=True)
        }

        if context["basic_info"]:
            context["basic_info"] |= yoy_change
        else:
            context["basic_info"] = yoy_change

        context["holdings"] = self.object.item.etf.get_holdings()

        return context


class FetchEtfAjaxView(BaseFetchItemAjaxView):
    def get(self, request, *args, **kwargs):
        etf = get_object_or_404(UserItem, pk=kwargs["pk"])
        data = {"item": []}

        # Fetch main series - price series - "etf".
        data["item"] = self.get_item_prices(etf.item)

        # Lookup for transactions.
        if request.GET.get("transactions"):
            data["transactions"] = self.get_transactions(etf.item)

        # SMAs.
        if request.GET.get("smas"):
            if 20 <= len(data["item"]):
                data["ma20"] = etf.item.get_sma(20)

            if 50 <= len(data["item"]):
                data["ma50"] = etf.item.get_sma(50)

            if 200 <= len(data["item"]):
                data["ma200"] = etf.item.get_sma(200)

        # Lookup for index.
        self.add_index_data(request, data)

        return self.send(True, data)


class PerformanceDetailView(EtfSubmenuViewMixin, BasePerformanceDetailView):
    pass


class DrawdownPeriodsAndAthsTemplateView(
    EtfSubmenuViewMixin, BaseDrawdownPeriodsAndAthsDetailView
):
    pass


class EtfTheEyeView(EtfSubmenuViewMixin, BaseTheEyeView):
    pass


class HistoricalDataListView(EtfSubmenuViewMixin, BaseHistoricalData): ...
