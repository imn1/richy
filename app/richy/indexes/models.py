from ..core.models import Item


class Index(Item):
    def is_share(self):
        return False

    def is_index(self):
        return True

    def is_etf(self):
        return False

    def is_coin(self):
        return False
