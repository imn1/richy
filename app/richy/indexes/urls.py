from django.urls import path

from . import views

app_name = "indexes"
urlpatterns = [
    path("", views.OverviewCreateView.as_view(), name="overview"),
    path(
        "index/<int:pk>/delete/",
        views.DeleteIndexRedirectView.as_view(),
        name="delete_index",
    ),
    path(
        "overview/fetch/", views.FetchOverviewAjaxView.as_view(), name="overview_fetch"
    ),
    path("index/<int:pk>/", views.IndexDetailView.as_view(), name="index_detail"),
    path(
        "index/<int:pk>/update/", views.IndexUpdateView.as_view(), name="update_index"
    ),
    path(
        "index/<int:pk>/fetch/", views.FetchIndexAjaxView.as_view(), name="index_fetch"
    ),
    path(
        "index/<int:pk>/reset/",
        views.ResetIndexRedirectView.as_view(),
        name="reset_index",
    ),
    path(
        "index/<int:pk>/performance/",
        views.PerformanceDetailView.as_view(),
        name="performance",
    ),
    path(
        "index/<int:pk>/drawdown-periods-and-aths/",
        views.DrawdownPeriodsAndAthsTemplateView.as_view(),
        name="drawdown_periods_and_aths",
    ),
    path("index/<int:pk>/the-eye/", views.IndexTheEyeView.as_view(), name="the_eye"),
    path(
        "index/<int:pk>/historical-data/",
        views.HistoricalDataListView.as_view(),
        name="historical_data",
    ),
]
