from django.contrib import admin

from ..core.admin import ItemMixin
from .models import Index


@admin.register(Index)
class IndexAdmin(ItemMixin, admin.ModelAdmin):
    pass
