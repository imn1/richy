import logging
from datetime import date, timedelta

from braces.views import FormMessagesMixin, LoginRequiredMixin
from django.conf import settings
from django.contrib import messages
from django.shortcuts import get_object_or_404
from django.urls import reverse, reverse_lazy
from django.utils.translation import gettext as _
from django.views.generic import RedirectView, UpdateView

from ..core.models import UserItem
from ..core.views import (
    BaseDeleteUserItemRedirectView,
    BaseDrawdownPeriodsAndAthsDetailView,
    BaseFetchItemAjaxView,
    BaseHistoricalData,
    BaseItemDetailView,
    BasePerformanceDetailView,
    BaseUserItemCreateView,
    SubmenuViewMixin,
    UserItemManipulationMixin,
    BaseTheEyeView,
)
from ..indexes.tasks import fetch_historical_data
from ..news.tasks import download_index_news
from .forms import UserIndexForm

logger = logging.getLogger(__name__)


class FetchDataMixin:
    """
    Mixin that ensures all share data which needs to be downloaded.
    """

    def fetch_data(self, pk):
        # fetch_basic_info.delay(pk)
        fetch_historical_data.delay(pk)
        download_index_news.delay(pk)
        # fetch_financial_data.delay(pk)


class OverviewCreateView(FetchDataMixin, BaseUserItemCreateView):
    form_class = UserIndexForm
    template_name = "indexes/overview.pug"
    success_url = reverse_lazy("indexes:overview")
    form_valid_message = _("Index has been saved.")
    form_invalid_message = _("Index hasn't been saved. Please check the form.")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        # Fetch indexes.
        context["index_list"] = (
            UserItem.objects.select_related("item")
            .by_user(self.request.user)
            .filter(item__index__isnull=False)
            .order_by("is_archived", "item__symbol")
        )

        return context


class FetchOverviewAjaxView(BaseFetchItemAjaxView):
    def get(self, request, *args, **kwargs):
        series = []

        for i in UserItem.objects.by_user(request.user).filter(
            item__index__isnull=False, show_in_overview=True, is_archived=False
        ):
            series.append(
                {
                    "name": i.item.symbol,
                    "data": self.get_item_prices(
                        i.item,
                        datetime__date__gte=date.today()
                        - timedelta(days=settings.OVERVIEW_INDEXES_HISTORY),
                    ),
                }
            )

        return self.send(True, {"item": series})


class DeleteIndexRedirectView(BaseDeleteUserItemRedirectView):
    def get_redirect_url(self, *args, **kwargs):
        self.delete()
        messages.success(self.request, _("Index has been deleted."))

        return reverse("indexes:overview")


class IndexDetailView(BaseItemDetailView):
    model = UserItem
    template_name = "indexes/detail.pug"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        min_price, max_price = self.get_object().item.get_year_min_max_prices()
        context["basic_info"] = {
            "year_low": min_price,
            "year_high": max_price,
            "yoy_change": self.get_object().item.get_last_days_change(
                365, percents=True
            ),
        }

        return context


class IndexUpdateView(
    LoginRequiredMixin,
    FormMessagesMixin,
    UserItemManipulationMixin,
    FetchDataMixin,
    UpdateView,
):
    model = UserItem
    form_class = UserIndexForm
    template_name = "indexes/update.pug"
    success_url = reverse_lazy("indexes:overview")
    form_valid_message = _("Index has been saved.")
    form_invalid_message = _("Index hasn't been saved. Please check the form.")

    def form_valid(self, form):
        original = UserItem.objects.get(pk=form.instance.pk, user=self.request.user)

        # Unarchiving -> fetch all the data.
        if original.is_archived and not form.instance.is_archived:
            self.fetch_data(form.instance.pk)

        return super().form_valid(form)


class ResetIndexRedirectView(LoginRequiredMixin, FetchDataMixin, RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        item = get_object_or_404(
            UserItem,
            pk=self.kwargs["pk"],
            user=self.request.user,
            item__index__isnull=False,
        )
        item.item.price_set.all().delete()
        self.fetch_data(item.item.pk)

        messages.success(self.request, _("Index prices are being reset."))

        return reverse("indexes:overview")


class FetchIndexAjaxView(BaseFetchItemAjaxView):
    def get(self, request, *args, **kwargs):
        index = get_object_or_404(UserItem, pk=kwargs["pk"])
        data = {"item": [], "transactions": []}

        # Fetch main series - price series - "index".
        data["item"] = self.get_item_prices(index.item)

        # SMAs.
        if request.GET.get("smas"):
            if 20 <= len(data["item"]):
                data["ma20"] = index.item.get_sma(20)

            if 50 <= len(data["item"]):
                data["ma50"] = index.item.get_sma(50)

            if 200 <= len(data["item"]):
                data["ma200"] = index.item.get_sma(200)

        return self.send(True, data)


class IndexChildrenSubmenuViewMixin(SubmenuViewMixin):
    def get_submenu(self):
        items = []

        # Performance
        items.append(
            {
                "url": reverse("indexes:performance", args=[self.object.pk]),
                "title": _("Performance"),
                "is_current": False,
            }
        )
        # Drawdowns and ATHs
        items.append(
            {
                "url": reverse(
                    "indexes:drawdown_periods_and_aths", args=[self.object.pk]
                ),
                "title": _("Drawdowns and ATHs"),
                "is_current": False,
            }
        )
        # Historical data
        items.append(
            {
                "url": reverse("indexes:historical_data", args=[self.object.pk]),
                "title": _("Historical data"),
                "is_current": False,
            }
        )
        # The Eye
        items.append(
            {
                "url": reverse("indexes:the_eye", args=[self.object.pk]),
                "title": _("The Eye"),
                "is_current": False,
            }
        )

        return items


class PerformanceDetailView(IndexChildrenSubmenuViewMixin, BasePerformanceDetailView):
    pass


class DrawdownPeriodsAndAthsTemplateView(
    IndexChildrenSubmenuViewMixin, BaseDrawdownPeriodsAndAthsDetailView
):
    pass


class IndexTheEyeView(IndexChildrenSubmenuViewMixin, BaseTheEyeView):
    pass


class HistoricalDataListView(IndexChildrenSubmenuViewMixin, BaseHistoricalData): ...
