import logging

from django.conf import settings as stngs

from .forms import SearchForm
from .models import Item, Meta

logger = logging.getLogger(__name__)


def meta(request):
    try:
        last_price_update = Meta.objects.get(type=Meta.LAST_PRICE_UPDATE)
    except Meta.DoesNotExist:
        last_price_update = None

    return {
        "meta": {
            "last_price_update": last_price_update.value if last_price_update else "",
        }
    }


def settings(request):
    return {
        "settings": {
            "CHART_COLORS": stngs.CHART_COLORS,
            "VERSION": stngs.VERSION,
            "MAX_PRECISION": stngs.MAX_PRECISION,
            "COINS_MAX_PRECISION": stngs.COINS_MAX_PRECISION,
        }
    }


def search_form(request):
    return {"search_form": SearchForm()}


# TODO: cache this and invalidate once a item is added/removed
def all_symbols(request):
    return {"all_symbols": Item.objects.values_list("symbol", flat=True)}
