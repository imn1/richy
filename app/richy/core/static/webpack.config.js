var path = require('path')
var webpack = require('webpack')
var CompressionPlugin = require("compression-webpack-plugin")
// var UglifyJsPlugin = require("uglifyjs-webpack-plugin")
// var VueLoaderPlugin = require('vue-loader')
const { VueLoaderPlugin } = require('vue-loader')
var MiniCssExtractPlugin = require("mini-css-extract-plugin")
const TerserPlugin = require('terser-webpack-plugin');


// Set as "development" or "production"
const mode = process.env.NODE_ENV ? process.env.NODE_ENV : "development"
console.log("Compiling in " + mode + " environment")

let conf = {
    mode: mode,
    entry: ['./js/app.js', './sass/app.sass'],
    output: {
        path: path.resolve(__dirname, './js'),
        filename: './app_build.js',
        publicPath: ""
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules)/,
                use :{
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            ["@babel/preset-env", {targets: "defaults"}]
                        ]
                    }
                }
            },
            {
                test: /\.vue/,
                loader: "vue-loader",
            },
            {
                test: /\.pug$/,
                loader: 'vue-pug-loader'
            },
            {
                test: /\.(sa|sc|c)ss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    "css-loader",
                    {
                        loader: "sass-loader",
                        options:  {
                            sassOptions: {
                                includePaths: ["node_modules"]
                            }
                        }
                    }
                ],
            },
            {
                test: /\.(jpe|jpg|woff|woff2|eot|ttf|svg)(\?.*$|$)/,
                loader: "file-loader",
                options: {
                    name: "../assets/[name].[ext]",
                }
            }
        ]
    },

    resolve: {
        // symlinks: false,
        alias: {
            // "vue$": "vue/dist/vue" + ("production" == mode ? ".min" : "")
            // 'vue$': 'vue/dist/vue.esm.js'
            // 'vue': '@vue/runtime-dom'
            "vue": "vue/dist/vue.esm-bundler.js"

        //     buffer: require.resolve("buffer/")
        }
    },

    plugins: [

        new MiniCssExtractPlugin({
            filename: "../css/app_build.css"
        }),

        // Creates gzipped version of JS.
        // new CompressionPlugin({
        //     // asset: "[path].gz[query]",
        //     // algorithm: "gzip",
        //     test: /\.(js|html)$/,
        //     threshold: 10240
        //     // minRatio: 0.8
        // }),

        new VueLoaderPlugin()
    ],
}

if ("production" == mode)
    conf.optimization = {
        minimize: true,
        minimizer: [new TerserPlugin()],
    }

if ("development" == mode)
    conf.devtool = "source-map"

module.exports = conf
