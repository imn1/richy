import Hammer from "hammerjs"
import moment from "moment"
import Ajax from "./ajax.js"


class Core {

    constructor() {

        document.addEventListener("DOMContentLoaded", this.tabsLinking)
        document.addEventListener("DOMContentLoaded", this.setUpMobileImages)
    }

    /**
     * Artificially activates (with a click) particular tab if it's
     * ID matches URL hash part.
     **/
    tabsLinking() {

        // Switches to a tab that coreponds with URL hash.
        setTimeout(() => {
            if (window.location.hash) {

                let el = document.querySelector("[href='"+window.location.hash.toLowerCase()+"']");

                if (el)
                    el.click();
            }
        }, 300);
    }

    /**
     * Sets long click (press) handler to all .mobile-images elements.
     * Tries to open them in new tab.
     *
     * @param {Object} el DOM node to be added to mobile images setup.
     */
    setUpMobileImages(el = null) {

        if (el instanceof HTMLElement)
            var images = [el];
        else
            var images = [...document.querySelectorAll(".mobile-image")];

        for (const i of images) {

            let mc = new Hammer.Manager(i);
            let press = new Hammer.Press();

            mc.add(press);
            mc.on("press", function(e) {

                // For all inline data we clone whole element
                // and pass it to the new tab.
                if (e.target.src.startsWith("data:")) {

                    let w = window.open("");
                    w.document.body.appendChild(e.target.cloneNode());
                }

                // Commonly opens new tab to target src.
                else
                    window.open(e.target.src, "_blank");
            });
        }

    }
}

export default Core;
