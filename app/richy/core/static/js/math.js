/**
 *
 * Calculates percentage difference between
 * initial value (start) and final value (end).
 *
 * @param Integer end Final value.
 * @param Integer start Initial value.
 * @returns float
 */
export function calc_percentage_change(end, start) {

    return 100 * (end / start - 1)
}
