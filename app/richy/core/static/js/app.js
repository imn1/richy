import { createApp } from "vue"
import { createI18n } from "vue-i18n"
import HighchartsVue from "highcharts-vue"
import Highcharts from "highcharts"
import HighchartsDataModule from "highcharts/modules/data"
import HighstockModule from 'highcharts/modules/stock'
import HighchartsExporting from 'highcharts/modules/exporting'
// import HighchartsDraggablePoints from "highcharts/modules/draggable-points"

import Core from "./core.js"

import News from "./components/news.vue"
import Chart from "./components/chart.vue"
import ItemChart from "./components/item_chart.vue"
import StaticChart from "./components/static_chart.vue"
import ItemPrice from "./components/item_price.vue"
import TheEye from "./components/the_eye.vue"


/************* HIGHCHARTS INITIALIZATION ************/
HighchartsDataModule(Highcharts)
HighstockModule(Highcharts)
HighchartsExporting(Highcharts)
// HighchartsDraggablePoints(Highcharts)
/****************************************************/


/**************** VUE INITIALIZATION ****************/
// Vue.config.debug = true


// Dummy setup of i18n.
const i18n = createI18n({
    "locale": "en",
    "messages": {
        "en": {
            "no-data": "No data",
            "no-news": "No news",
            "market-open": "Market open",
            "market-close": "Market close",
        }
    }
})
const app = createApp({
    components: {
        news: News,
        chart: Chart,
        itemChart: ItemChart,
        staticChart: StaticChart,
        itemPrice: ItemPrice,
        theEye: TheEye,
    }
})
app.use(i18n)
app.use(HighchartsVue)

app.mount("main")
/****************************************************/


/*********** CUSTOM CLASSES INITIALIZATION **********/
window.core = new Core()
/****************************************************/

