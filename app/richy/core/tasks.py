# from django.utils.timezone import make_naive

import logging

from ..celery import app

# from ..indexes.models import Index
from .charts import Manager as ChartManager
from .models import Asset, Item

LOGGER = logging.getLogger("richy.celery")


@app.task
def generate_performance_charts(item=None):
    """
    Generates performance chart to a given item. If no item (PK) was
    given fetches all of them from database walks thru them
    and generates charts for each of them.

    :param int share: Item PK.
    """

    def generate(item):
        manager = ChartManager()
        buffer = manager.performance(item)

        Asset.upload(buffer, Asset.PERFORMANCE_CHART, item=item, extension="svg")
        buffer.close()

        LOGGER.debug(f"Performance chart generated for {item.symbol}.")

    # Single item.
    if item:
        generate(Item.objects.get(pk=item))

    # All of them.
    else:
        for i in Item.objects.all():
            try:
                generate(i)
            except Exception:
                LOGGER.exception(
                    "Couldn't generate performance chart.", extra={"item": i}
                )
                LOGGER.debug(f"Item: {i.symbol} ^")
