import json

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as OriginalUserAdmin
from django.utils.html import escape
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _
from sitetree.admin import TreeItemAdmin, override_item_admin

from .models import Asset, ItemData, User, UserItem


class NavigationTreeItemAdmin(TreeItemAdmin):
    fieldsets = (
        (_("Basic settings"), {"fields": ("parent", "title", "url", "icon")}),
        (
            _("Access settings"),
            {
                "classes": ("collapse",),
                "fields": (
                    "access_loggedin",
                    "access_guest",
                    "access_restricted",
                    "access_permissions",
                    "access_perm_type",
                ),
            },
        ),
        (
            _("Display settings"),
            {
                "classes": ("collapse",),
                "fields": ("hidden", "inmenu", "inbreadcrumbs", "insitetree"),
            },
        ),
        (
            _("Additional settings"),
            {
                "classes": ("collapse",),
                "fields": ("hint", "description", "alias", "urlaspattern"),
            },
        ),
    )


override_item_admin(NavigationTreeItemAdmin)


@admin.register(User)
class UserAdmin(OriginalUserAdmin):
    fieldsets = (
        (None, {"fields": ("email", "password")}),
        (
            _("Personal info"),
            {
                "fields": (
                    "first_name",
                    "last_name",
                )
            },
        ),
        (
            _("Permissions"),
            {
                "fields": (
                    "is_active",
                    "is_staff",
                    "is_superuser",
                    "groups",
                    "user_permissions",
                ),
            },
        ),
        (_("Important dates"), {"fields": ("last_login", "date_joined")}),
    )
    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": ("email", "password1", "password2"),
            },
        ),
    )
    list_display = ("email", "first_name", "last_name", "is_staff")
    search_fields = ("first_name", "last_name", "email")
    ordering = ("email",)


class AssetInline(admin.TabularInline):
    model = Asset
    extra = 0


class ItemDataInline(admin.TabularInline):
    model = ItemData
    extra = 0
    fields = ["formatted_data"]
    readonly_fields = ["formatted_data"]

    @admin.display(description=_("Data"))
    def formatted_data(self, obj):
        data = json.dumps(obj.data, indent=4, sort_keys=True)
        return mark_safe(f"<pre>{escape(data)}</pre>")


class ItemMixin:
    list_display = [
        "symbol",
    ]
    search_fields = ["symbol"]
    inlines = [AssetInline, ItemDataInline]


@admin.register(UserItem)
class UserItemAdmin(admin.ModelAdmin):
    list_display = [
        "user",
        "item",
        "show_in_overview",
        "show_in_news",
        "show_on_dashboard",
        "is_archived",
    ]
    search_fields = ["item__symbol", "user__email"]
