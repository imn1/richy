from django import forms
from .widgets import MultipleFileInput


class MultipleFileField(forms.FileField):
    def __init__(self, delete_url_pattern=None, *args, **kwargs):
        kwargs.setdefault("widget", MultipleFileInput())
        self.delete_url_pattern = delete_url_pattern
        super().__init__(*args, **kwargs)

    def clean(self, data, initial=None):
        single_file_clean = super().clean

        if isinstance(data, (list, tuple)):
            result = [single_file_clean(d, initial) for d in data]
        else:
            result = [single_file_clean(data, initial)]

        return result

    def widget_attrs(self, widget):
        attrs = super().widget_attrs(widget)
        attrs["delete_url_pattern"] = self.delete_url_pattern

        return attrs
