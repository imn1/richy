from django import forms


# class MultipleFileInput(FileInput):
class MultipleFileInput(forms.ClearableFileInput):
    template_name = "widgets/clearable_multiple_file.html"
    allow_multiple_selected = True

    def __init__(self, *args, **kwargs):
        super().__init__(attrs={"multiple": True}, *args, **kwargs)

    def format_value(self, value):
        """
        We need to override this method to prevent any formatting.
        Our value is a list of Attachment model instances.
        """

        return value

    def get_context(self, *args, **kwargs):
        context = super().get_context(*args, **kwargs)
        context["delete_url_pattern"] = self.attrs["delete_url_pattern"]

        return context
