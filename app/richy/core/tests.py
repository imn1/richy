import itertools
import json
import logging
from datetime import date, datetime, timedelta
from decimal import Decimal
from unittest.mock import patch

import numpy as np
import pandas as pd
import pytz
from bs4 import BeautifulSoup
from django.conf import settings
from django.test import Client, SimpleTestCase, TransactionTestCase, override_settings
from django.urls import reverse
from django.utils.timezone import make_aware
from faker import Faker

from ..coins.models import Coin
from ..etfs.models import Etf
from ..indexes.models import Index
from ..shares.models import Share
from ..transactions.models import Exchange, Transaction
from ..transactions.transactions import Transactions
from ..transactions.views import TransactionChartDataMixin
from .charts import DashboardMarketValueRatioPieChart, TransactionOverviewChart
from .models import Item, ItemData, Price, User, UserItem, date_to_highcharts_timestamp
from .templatetags.utils import autofloatformat, coinautofloatformat, to_quarter_period

LOGGER = logging.getLogger(__name__)


def create_items():
    """
    Creates some basic symbols.
    """

    Coin(symbol="BTC", coin_id="bitcoin").save()
    Coin(symbol="ETH", coin_id="ethereum").save()
    Coin(symbol="ETC", coin_id="ethereum-classic").save()
    Coin(symbol="TRX", coin_id="tron").save()
    Coin(symbol="XRP", coin_id="ripple").save()
    Coin(symbol="LTC", coin_id="litecoin").save()
    Share(symbol="AMD").save()
    Share(symbol="QCOM").save()
    Share(symbol="MSFT").save()
    Share(symbol="TSLA").save()
    Index(symbol="SOX").save()
    Index(symbol="GSPC").save()
    Etf(symbol="SPLG").save()
    Etf(symbol="TQQQ").save()


def create_user_items(user=None):
    if not user:
        user = User.objects.get(email=UserTestMixin.USER_EMAIL)

    for c in Coin.objects.all():
        UserItem(user=user, item=c).save()

    for s in Share.objects.all():
        UserItem(user=user, item=s).save()

    for i in Index.objects.all():
        UserItem(user=user, item=i).save()

    for e in Etf.objects.all():
        UserItem(user=user, item=e).save()


def create_exchanges():
    """
    Creates some basic exchanges.
    """

    Exchange(title="Binance").save()
    Exchange(title="Coinbase").save()
    Exchange(title="Kraken").save()


def create_transactions(transactions):
    """
    Creates transactions according to the given schema.
    Schema be like:

    ```
    [
        [
            {
                "item": Coin.objects.get(symbol="BTC"),
                "price": 1,
                "amount": 50,
                "fee": 0,
                "date": "2018-12-01",
                "exchange": Exchange.objects.get(title="Binance"),
                "is_deposit": True,
                "user": self.user,
            },
            {},
        ]
    ]
    ```

    :param list transactions: Transactions schema.
    """

    for data, relations in transactions:
        trans = Transaction(**data)
        trans.save()

        if "parents" in relations:
            trans.parents.set(relations["parents"])


def create_prices(prices):
    """
    Creates price records according to given schema.
    The given date is treaten as UTC.

    Prices has following schema:

    ```
    [
        ["2018-01-01", "BTC", 1],
        [date.today(), "BTC", 1.25],
        ....
    ]
    ```

    :param list prices: Prices schema.
    """

    for dt, symbol, price in prices:
        item = Item.by_symbol(symbol)

        # Parse date from YYYY-MM-DD string.
        if isinstance(dt, str):
            dt = datetime.strptime(dt, "%Y-%m-%d")
        else:
            dt = datetime.combine(dt, datetime.min.time())

        dt = make_aware(dt, pytz.UTC)
        Price.objects.create(item=item, datetime=dt, price=price)


def create_basic_infos():
    """
    Creates shares and coins basic info with fake data.
    """

    f = Faker()

    for item in itertools.chain(
        Share.objects.all(), Coin.objects.all(), Etf.objects.all()
    ):
        data = {}

        if item.is_share() or item.is_coin():
            data["basic_info"] = {
                "yoy_change": f.pyfloat(1, 6, min_value=-10, max_value=10),
                "year_low": f.pyfloat(1, 6, min_value=2, max_value=4),
                "year_high": f.pyfloat(1, 6, min_value=2, max_value=4),
            }

        if item.is_etf():
            data["basic_info"] = {
                "year_low": f.pyfloat(1, 6, min_value=2, max_value=4),
                "year_high": f.pyfloat(1, 6, min_value=2, max_value=4),
            }

        ItemData(item=item, data=data).save()


class UserTestMixin:
    USER_EMAIL = "test@test.cz"
    USER_PASSWORD = "test"

    @classmethod
    def set_up_users(cls):
        cls.user = User.objects.create_user(
            UserTestMixin.USER_EMAIL, UserTestMixin.USER_PASSWORD
        )
        cls.user2 = User.objects.create_user(
            "tes2t@test.com", UserTestMixin.USER_PASSWORD
        )


@override_settings(
    CACHES={
        "default": {
            "BACKEND": "django.core.cache.backends.dummy.DummyCache",
        }
    }
)
class BaseTestCase(UserTestMixin, TransactionTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.f = Faker()

    @classmethod
    def setUp(cls):
        super().setUp(cls)
        cls.set_up_users()

        # Create an user and sign him in.
        cls.c = Client()
        cls.c.force_login(cls.user)

    def get_url_dom(self, url):
        """
        Fetcher URL DOM.

        :param str url: URL to fetch DOM for.
        :return: DOM as BeautifulSoup instance.
        :rtype: BeautifulSoup
        """
        response = self.c.get(url)
        html = response.content.decode("utf-8")

        return BeautifulSoup(html, "html.parser")


@override_settings(
    CACHES={
        "default": {
            "BACKEND": "django.core.cache.backends.locmem.LocMemCache",
        }
    }
)
class BaseCacheTestCase(BaseTestCase):
    """
    Base test case with cache support.
    """


class BaseDecimalFormatTestCase(BaseTestCase):
    decimals = {"share": 2, "coin": 3, "etf": 2, "index": 2}

    @classmethod
    def setUp(cls):
        super().setUp()

        days = 30
        date_base = date.today() - timedelta(days=days)

        create_exchanges()
        create_items()
        create_user_items()
        create_transactions(
            [
                [
                    {
                        "item": Coin.objects.get(symbol="BTC"),
                        "price": cls.generate_price(),
                        "amount": 10,
                        "fee": 0,
                        "date": date_base,
                        "exchange": Exchange.objects.get(title="Binance"),
                        "is_deposit": True,
                        "user": cls.user,
                    },
                    {},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="ETH"),
                        "price": cls.generate_price(),
                        "amount": 10,
                        "fee": 0,
                        "date": date_base,
                        "exchange": Exchange.objects.get(title="Binance"),
                        "is_deposit": True,
                        "user": cls.user,
                    },
                    {},
                ],
                [
                    {
                        "item": Share.objects.get(symbol="MSFT"),
                        "price": cls.generate_price(),
                        "amount": 10,
                        "fee": 0,
                        "date": date_base,
                        "is_deposit": True,
                        "user": cls.user,
                    },
                    {},
                ],
                [
                    {
                        "item": Share.objects.get(symbol="AMD"),
                        "price": cls.generate_price(),
                        "amount": 10,
                        "fee": 0,
                        "date": date_base,
                        "is_deposit": True,
                        "user": cls.user,
                    },
                    {},
                ],
                [
                    {
                        "item": Etf.objects.get(symbol="SPLG"),
                        "price": cls.generate_price(),
                        "amount": 10,
                        "fee": 0,
                        "date": date_base,
                        "is_deposit": True,
                        "user": cls.user,
                    },
                    {},
                ],
                [
                    {
                        "item": Etf.objects.get(symbol="TQQQ"),
                        "price": cls.generate_price(),
                        "amount": 10,
                        "fee": 0,
                        "date": date_base,
                        "is_deposit": True,
                        "user": cls.user,
                    },
                    {},
                ],
            ]
        )
        create_basic_infos()

        for item in itertools.chain(
            Share.objects.values_list("symbol", flat=True),
            Coin.objects.values_list("symbol", flat=True),
            Etf.objects.values_list("symbol", flat=True),
            Index.objects.values_list("symbol", flat=True),
        ):
            create_prices(
                zip(
                    [date_base + timedelta(days=i) for i in range(0, days)],
                    [item] * days,
                    [cls.generate_price() for _ in range(0, days)],
                )
            )

    @classmethod
    def generate_price(cls):
        return cls.f.pyfloat(1, 6, min_value=2, max_value=4)

    @classmethod
    def generate_price_change(cls):
        return cls.f.pyfloat(1, 6, min_value=-10, max_value=10)

    def assert_format(self, symbol, value):
        """
        Cleans value to decimal (in string format) and checks
        if it has less or equal decimal places than it should have.
        Case "less decimal places" happens when the price is i.e.
        1.3001 - the app prints out 1.3 which has only 1 decimal place.
        """

        LOGGER.debug(f"Testing {symbol} with value {value}")

        value = value.strip(" \n()%")
        dec = Decimal(value).as_tuple()
        decimals = self.get_decimals(symbol)

        # In case any zeroes before decimal
        zeroes_before_decimal = dec.exponent * -1 - len(dec.digits)
        # LOGGER.debug(f"Zeroes before decimal: {zeroes_before_decimal}")

        if 0 < zeroes_before_decimal:
            decimals += zeroes_before_decimal + 1

        # LOGGER.debug(f"Symbol: {symbol}, value: '{value}', decimals: {decimals}")

        # There might be a case where the number is not float at all (100%, 50%, etc...).
        if "." in value:
            self.assertTrue(len(value.split(".")[1]) <= decimals)
        else:
            self.assertEqual(str(int(value)), value)

    def get_decimals(self, symbol):
        return self.decimals[Item.by_symbol(symbol).__class__.__name__.lower()]

    def perform_test_price_format(self, url, is_indexes_page=False):
        doc = self.get_url_dom(url)

        if is_indexes_page:
            open_items = doc.find("table")
            self.do_other_items(open_items)
        else:
            open_items, other_items = doc.find_all("table")
            self.do_open_items_table(open_items)
            self.do_other_items(other_items)

    def do_open_items_table(self, table):
        # Open items table.
        for row in table.find_all("tr")[1:]:
            cells = row.find_all("td")

            # dig out symbol
            symbol = list(cells[0].stripped_strings)[0]

            # meter percents
            self.assert_format(symbol, cells[1].div.div.find_all("div")[0].string)

            # other table values
            for c in cells[2:6]:
                self.assert_format(symbol, list(c.stripped_strings)[0])

    def do_other_items(self, table):
        # Other items table.
        for row in table.find_all("tr")[1:]:
            cells = row.find_all("td")

            # dig out symbol
            symbol = list(cells[0].stripped_strings)[0]

            # other table values
            for c in cells[1:5]:
                self.assert_format(symbol, list(c.stripped_strings)[0])

    def perform_test_overview_chart_price_format(self, url):
        data = self.c.get(url).json()

        self.assertTrue(data["status"])

        for item in data["data"]["item"]:
            for item_data in item["data"]:
                self.assert_format(item["name"], str(item_data[1]))

    def perform_test_item_detail_current_price_format(self, symbol, url):
        doc = self.get_url_dom(url)

        # current price
        price_data = json.loads(doc.find("item-price").attrs[":default-value"])
        self.assert_format(symbol, price_data["price"])
        self.assert_format(symbol, price_data["change_value"])
        self.assert_format(symbol, price_data["change_percents"])
        # last day move
        for value in doc.find(id="basic-info").find_all("li")[0].get_text().split("("):
            self.assert_format(symbol, value)

    def perform_test_item_detail_yoy_change(self, item, url):
        year_ago = make_aware(
            datetime.combine(date.today() - timedelta(days=365), datetime.min.time()),
            pytz.UTC,
        )
        Price(item=item, datetime=year_ago, price=self.generate_price()).save()
        doc = self.get_url_dom(url)
        self.assert_format(
            item.symbol,
            doc.find(id="basic-info").find_all("li")[1].get_text(),
        )

    def perform_test_item_detail_ath(self, item, url):
        position = 2

        # In case of owned item, the position is +1 because of
        # another row in the list (ul).
        if Transaction.objects.filter(item=item).exists():
            position += 1

        doc = self.get_url_dom(url)
        li = doc.find(id="basic-info").parent.find_all("li")[position].get_text()

        self.assert_format(item.symbol, li.split("(")[0])

    def perform_test_item_detail_year_low_high(self, item, url, position):
        doc = self.get_url_dom(url)
        li = doc.css.select("h2 + div > div:nth-of-type(2) ul > li")[
            position
        ].get_text()

        self.assert_format(item.symbol, li.split("-")[0])
        self.assert_format(item.symbol, li.split("-")[1])

    def perform_test_item_detail_chart_price_format(self, symbol, url):
        # Values.
        data = self.c.get(url).json()

        self.assertTrue(data["status"])

        for item in data["data"]["item"]:
            self.assert_format(symbol, str(item[1]))

        # Percents.
        data = self.c.get(url + "?percents=true").json()

        self.assertTrue(data["status"])

        for item in data["data"]["item"]:
            self.assert_format(symbol, str(item[1]))

    def perform_test_item_performance_chart_price_format(self, symbol, url):
        context = self.c.get(url).context
        self.assertEqual(
            self.get_decimals(symbol),
            context["performance_chart_options"]["tooltip"]["valueDecimals"],
        )

    def perform_test_item_performance_table_price_format(self, symbol, url):
        doc = self.get_url_dom(url)

        for row in doc.find("table").find_all("tr")[1:]:
            cells = row.find_all("td")

            LOGGER.debug(cells[1].get_text())

            # 2nd column.
            low, high = cells[1].get_text().split("-")
            self.assert_format(symbol, low)
            self.assert_format(symbol, high)

            # 3rd column
            self.assert_format(symbol, cells[2].get_text())

            # 4th column
            self.assert_format(symbol, cells[3].get_text())

    def perform_test_assets_chart(self, url):
        data = self.c.get(url).json()
        self.assertTrue(data["status"])

        for item in data["data"]:
            for item_data in item["data"]:
                self.assert_format(item["name"], str(item_data[1]))


class DashboardDecimalFormatTestCase(BaseDecimalFormatTestCase):
    fixtures = ["sitetree", "meta"]

    def test_dashboard(self):
        response = self.c.get(reverse("dashboard"))

        self.assertIn("<h1>Dashboard", response.content.decode("utf-8"))

    def test_card_price_format(self):
        doc = self.get_url_dom(reverse("dashboard"))
        tabs = doc.find("ul", class_="uk-switcher").find_all("li", recursive=False)
        del tabs[1]  # discard indexes

        for tab in tabs:
            for card in tab.find_all("div", class_="uk-card"):
                symbol = card.find("h4").string
                numbers = []

                for value in list(card.find("p").stripped_strings):
                    # values from "current investments" cards
                    if "," in value:
                        numbers.extend(value.split(","))
                    # values from rest of cards
                    else:
                        numbers.append(value)

                for number in numbers:
                    self.assert_format(symbol, number)


class ChartTestCase(BaseTestCase):
    reset_sequences = True

    def fake_market_prices(self):
        """
        Fakes market prices like:

                      BTC    XRP    ETH    LTC
        2018-12-01      1      1    0.5    0.25
        2018-12-02      1      1    0.5    0.25
        2018-12-03      1   0.25    0.5    0.25
        2018-12-04      1   0.25    0.5    0.25
        2018-12-05      1   0.25    0.5    0.25
        2018-12-06      1   0.25      1    0.25
        2018-12-07      2    0.5      1    0.25
        """

        btc = Item.objects.get(symbol="BTC")
        xrp = Item.objects.get(symbol="XRP")
        eth = Item.objects.get(symbol="ETH")
        ltc = Item.objects.get(symbol="LTC")

        Price(item=btc, datetime="2018-12-01", price=1).save()
        Price(item=btc, datetime="2018-12-02", price=1).save()
        Price(item=btc, datetime="2018-12-03", price=1).save()
        Price(item=btc, datetime="2018-12-04", price=1).save()
        Price(item=btc, datetime="2018-12-05", price=1).save()
        Price(item=btc, datetime="2018-12-06", price=1).save()
        Price(item=btc, datetime="2018-12-07", price=2).save()

        Price(item=xrp, datetime="2018-12-01", price=1).save()
        Price(item=xrp, datetime="2018-12-02", price=1).save()
        Price(item=xrp, datetime="2018-12-03", price=0.25).save()
        Price(item=xrp, datetime="2018-12-04", price=0.25).save()
        Price(item=xrp, datetime="2018-12-05", price=0.25).save()
        Price(item=xrp, datetime="2018-12-06", price=0.25).save()
        Price(item=xrp, datetime="2018-12-07", price=0.5).save()

        Price(item=eth, datetime="2018-12-01", price=0.5).save()
        Price(item=eth, datetime="2018-12-02", price=0.5).save()
        Price(item=eth, datetime="2018-12-03", price=0.5).save()
        Price(item=eth, datetime="2018-12-04", price=0.5).save()
        Price(item=eth, datetime="2018-12-05", price=0.5).save()
        Price(item=eth, datetime="2018-12-06", price=1).save()
        Price(item=eth, datetime="2018-12-07", price=1).save()

        Price(item=ltc, datetime="2018-12-01", price=0.25).save()
        Price(item=ltc, datetime="2018-12-02", price=0.25).save()
        Price(item=ltc, datetime="2018-12-03", price=0.25).save()
        Price(item=ltc, datetime="2018-12-04", price=0.25).save()
        Price(item=ltc, datetime="2018-12-05", price=0.25).save()
        Price(item=ltc, datetime="2018-12-06", price=0.25).save()
        Price(item=ltc, datetime="2018-12-07", price=0.25).save()

    def test_transaction_overview(self):
        """
        Tests transaction overview chart series.

              -                                         -
         _   | |       -    -             -        -   |-|   -
        | |  | |      |-|  | |       -   |-|      |-|  |-|  |-|
        | |  | |      | |  | |      | |  | |      | |  | |  | |

        Schema:
        (Binance)

        1)

        $50 -> [1]BTC(+50/1)

        2)

        $50 -> [2]XRP(+200/0.25) -> [3]XRP(-200/0.25) -
                                                        \
                                                          -> [6]ETH(+200/0.5)
                                                        /
        $50 -> [4]ETH(+100/0.5) -> [5]ETH(-100/0.5) ---

        (Kraken)
        3)

        $100 -> [7]ETH(+200/0.5) -
                                   \
                                     -> [8]ETH(-100/1) -> [9]LTC(+200/0.25)

        (Coinbase)
        4)

        $50 -> [10]BTC(+50/1) -> [11]BTC(-50/1) -      -> [14](LTC)[200/0.25] -> [17](LTC)[-100/0.25]
                                                  \  /
                                                    .  -> [15](XRP)[100/0.25] -> [18](XRP)[-30/0.5]
                                                  /  \
        $50 -> [12]BTC(+50/1) -> [13]XRP(-50/1) -      -> [16](ETH)[100/0.25]
        """

        self.maxDiff = None
        create_exchanges()
        create_items()
        # 1
        create_transactions(
            [
                [
                    {
                        "item": Coin.objects.get(symbol="BTC"),
                        "price": 1,
                        "amount": 50,
                        "fee": 0,
                        "date": "2018-12-01",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "is_deposit": True,
                        "user": self.user,
                    },
                    {},
                ]
            ]
        )
        # 2)
        create_transactions(
            [
                [
                    {
                        "item": Coin.objects.get(symbol="XRP"),
                        "price": 0.25,
                        "amount": 200,
                        "fee": 0,
                        "date": "2018-12-03",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "user": self.user,
                    },
                    {},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="XRP"),
                        "price": 0.25,
                        "amount": -200,
                        "fee": 0,
                        "date": "2018-12-03",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "user": self.user,
                    },
                    {"parents": [2]},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="ETH"),
                        "price": 0.5,
                        "amount": 100,
                        "fee": 0,
                        "date": "2018-12-03",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "user": self.user,
                    },
                    {},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="ETH"),
                        "price": 0.5,
                        "amount": -100,
                        "fee": 0,
                        "date": "2018-12-03",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "user": self.user,
                    },
                    {"parents": [4]},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="XRP"),
                        "price": 0.5,
                        "amount": 200,
                        "fee": 0,
                        "date": "2018-12-06",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "user": self.user,
                    },
                    {"parents": [3, 5]},
                ],
            ]
        )
        # 3
        create_transactions(
            [
                [
                    {
                        "item": Coin.objects.get(symbol="ETH"),
                        "price": 0.5,
                        "amount": 200,
                        "fee": 0,
                        "date": "2018-12-05",
                        "exchange": Exchange.objects.get(title="Kraken"),
                        "user": self.user,
                    },
                    {},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="ETH"),
                        "price": 1,
                        "amount": -100,
                        "fee": 0,
                        "date": "2018-12-06",
                        "exchange": Exchange.objects.get(title="Kraken"),
                        "user": self.user,
                    },
                    {"parents": [7]},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="LTC"),
                        "price": 0.25,
                        "amount": 200,
                        "fee": 0,
                        "date": "2018-12-05",
                        "exchange": Exchange.objects.get(title="Kraken"),
                        "user": self.user,
                    },
                    {"parents": [8]},
                ],
            ]
        )
        # 4
        create_transactions(
            [
                [
                    {
                        "item": Coin.objects.get(symbol="BTC"),
                        "price": 1,
                        "amount": 50,
                        "fee": 0,
                        "date": "2018-12-05",
                        "exchange": Exchange.objects.get(title="Coinbase"),
                        "user": self.user,
                    },
                    {},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="BTC"),
                        "price": 1,
                        "amount": -50,
                        "fee": 0,
                        "date": "2018-12-05",
                        "exchange": Exchange.objects.get(title="Coinbase"),
                        "user": self.user,
                    },
                    {"parents": [10]},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="BTC"),
                        "price": 1,
                        "amount": 50,
                        "fee": 0,
                        "date": "2018-12-05",
                        "exchange": Exchange.objects.get(title="Coinbase"),
                        "user": self.user,
                    },
                    {},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="BTC"),
                        "price": 1,
                        "amount": -50,
                        "fee": 0,
                        "date": "2018-12-05",
                        "exchange": Exchange.objects.get(title="Coinbase"),
                        "user": self.user,
                    },
                    {"parents": [12]},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="LTC"),
                        "price": 0.25,
                        "amount": 200,
                        "fee": 0,
                        "date": "2018-12-05",
                        "exchange": Exchange.objects.get(title="Coinbase"),
                        "user": self.user,
                    },
                    {"parents": [11, 13]},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="XRP"),
                        "price": 0.25,
                        "amount": 100,
                        "fee": 0,
                        "date": "2018-12-05",
                        "exchange": Exchange.objects.get(title="Coinbase"),
                        "user": self.user,
                    },
                    {"parents": [11, 13]},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="ETH"),
                        "price": 0.25,
                        "amount": 100,
                        "fee": 0,
                        "date": "2018-12-05",
                        "exchange": Exchange.objects.get(title="Coinbase"),
                        "user": self.user,
                    },
                    {"parents": [11, 13]},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="LTC"),
                        "price": 0.25,
                        "amount": -100,
                        "fee": 0,
                        "date": "2018-12-06",
                        "exchange": Exchange.objects.get(title="Coinbase"),
                        "is_closing": True,
                        "user": self.user,
                    },
                    {"parents": [14]},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="XRP"),
                        "price": 0.5,
                        "amount": -30,
                        "fee": 0,
                        "date": "2018-12-07",
                        "exchange": Exchange.objects.get(title="Coinbase"),
                        "is_closing": True,
                        "user": self.user,
                    },
                    {"parents": [15]},
                ],
            ]
        )
        self.fake_market_prices()

        # First test chain groups (pile).
        df = Transactions(self.user).get_transaction_basic_stats(closed=False)
        groups = Transactions(self.user).get_pile_stats(df)
        self.assertEqual(
            groups,
            [
                (
                    [1],
                    {
                        "fees": np.float64(0.0),
                        "incomes": [],
                        "investments": [{1: 50.0}],
                        "open_symbols": [
                            {"symbol": "BTC", "amount": 50.0, "market_price": 100.0}
                        ],
                    },
                ),
                (
                    [2, 4],
                    {
                        "fees": np.float64(0.0),
                        "incomes": [],
                        "investments": [{2: 50.0}, {4: 50.0}],
                        "open_symbols": [
                            {"amount": 0.0, "market_price": 0.0, "symbol": "ETH"},
                            {"amount": 200.0, "market_price": 100.0, "symbol": "XRP"},
                        ],
                    },
                ),
                (
                    [10, 12],
                    {
                        "fees": np.float64(0.0),
                        "incomes": [{17: 25.0}, {18: 15.0}],
                        "investments": [{10: 50.0}, {12: 50.0}],
                        "open_symbols": [
                            {"amount": 0.0, "market_price": 0.0, "symbol": "BTC"},
                            {"amount": 100.0, "market_price": 100.0, "symbol": "ETH"},
                            {"amount": 100.0, "market_price": 25.0, "symbol": "LTC"},
                            {"amount": 70.0, "market_price": 35.0, "symbol": "XRP"},
                        ],
                    },
                ),
                (
                    [7],
                    {
                        "fees": np.float64(0.0),
                        "incomes": [],
                        "investments": [{7: 100.0}],
                        "open_symbols": [
                            {"amount": 100.0, "market_price": 100.0, "symbol": "ETH"},
                            {"amount": 200.0, "market_price": 50.0, "symbol": "LTC"},
                        ],
                    },
                ),
            ],
        )

        # Second test chart data.
        series = TransactionOverviewChart(self.user, df).get_series()
        self.assertEqual(
            series,
            [
                {
                    "name": "Investments",
                    "data": [50.0, 50.0, 50.0, 100.0],
                    "stack": "investments",
                },
                {
                    "data": [0.0, 50.0, 50.0, 0.0],
                    "name": "Investments",
                    "stack": "investments",
                },
                {
                    "data": [
                        {"name": "BTC", "y": 100.0},
                        {"name": "ETH", "y": 0.0},
                        {"name": "BTC", "y": 0.0},
                        {"name": "ETH", "y": 100.0},
                    ],
                    "name": "Market values",
                    "stack": "market_value",
                },
                {
                    "data": [
                        {"name": np.nan, "y": 0.0},
                        {"name": "XRP", "y": 100.0},
                        {"name": "ETH", "y": 100.0},
                        {"name": "LTC", "y": 50.0},
                    ],
                    "name": "Market values",
                    "stack": "market_value",
                },
                {
                    "data": [
                        {"name": np.nan, "y": 0.0},
                        {"name": np.nan, "y": 0.0},
                        {"name": "LTC", "y": 25.0},
                        {"name": np.nan, "y": 0.0},
                    ],
                    "name": "Market values",
                    "stack": "market_value",
                },
                {
                    "data": [
                        {"name": np.nan, "y": 0.0},
                        {"name": np.nan, "y": 0.0},
                        {"name": "XRP", "y": 35.0},
                        {"name": np.nan, "y": 0.0},
                    ],
                    "name": "Market values",
                    "stack": "market_value",
                },
                {"data": [0.0, 0.0, 25.0, 0.0], "name": "Sold", "stack": "sold"},
                {"data": [0.0, 0.0, 15.0, 0.0], "name": "Sold", "stack": "sold"},
            ],
        )


class DashboardMarketValueRatioChartTestCase(BaseTestCase):
    reset_sequences = True

    class Request:
        def __init__(self, user):
            self.user = user

    def test_single_share(self):
        """
        Tests dashboard net worth chart with single share.

        MSFT
        ----

        Prices:
        2018-01-01 -> 100
        2018-01-02 -> 105
        2018-01-03 -> 110
        2018-01-04 -> 110
        2018-01-05 -> 120

        Investment: $1000
        Profit: 20%
        Market value: $1200
        """

        create_items()
        create_user_items()
        create_prices(
            [
                ["2018-01-01", "MSFT", 100],
                ["2018-01-02", "MSFT", 105],
                ["2018-01-03", "MSFT", 110],
                ["2018-01-04", "MSFT", 110],
                ["2018-01-05", "MSFT", 120],
            ]
        )
        create_transactions(
            [
                [
                    {
                        "item": Share.objects.get(symbol="MSFT"),
                        "price": 100,
                        "amount": 10,
                        "fee": 0,
                        "date": "2018-01-01",
                        "is_deposit": True,
                        "user": self.user,
                    },
                    {},
                ],
            ]
        )

        mixin = TransactionChartDataMixin()
        mixin.request = self.__class__.Request(self.user)
        self.assertEqual(
            DashboardMarketValueRatioPieChart().get_series(
                mixin.get_transaction_basic_chart_data()
            ),
            {"USD": [{"name": "Net worth", "data": [{"name": "Share", "y": 1200.0}]}]},
        )

    def test_multiple_shares(self):
        """
        Tests dashboard net worth chart with multiple shares.

        MSFT                        AMD
        ----                        ---

        Prices:                     Prices:
        2018-01-01 -> 100           2018-01-01 -> 50
        2018-01-02 -> 105           2018-01-02 -> 50
        2018-01-03 -> 110           2018-01-03 -> 50
        2018-01-04 -> 110           2018-01-04 -> 50
        2018-01-05 -> 120           2018-01-05 -> 40

        Investment: $1000           Investment: $500
        Profit: 20%                 Profit: -20%
        Market value: $1200         Market value: $400
        """

        create_items()
        create_prices(
            [
                ["2018-01-01", "MSFT", 100],
                ["2018-01-02", "MSFT", 105],
                ["2018-01-03", "MSFT", 110],
                ["2018-01-04", "MSFT", 110],
                ["2018-01-05", "MSFT", 120],
                ["2018-01-01", "AMD", 50],
                ["2018-01-02", "AMD", 50],
                ["2018-01-03", "AMD", 50],
                ["2018-01-04", "AMD", 50],
                ["2018-01-05", "AMD", 40],
            ]
        )
        create_transactions(
            [
                [
                    {
                        "item": Share.objects.get(symbol="MSFT"),
                        "price": 100,
                        "amount": 10,
                        "fee": 0,
                        "date": "2018-01-01",
                        "is_deposit": True,
                        "user": self.user,
                    },
                    {},
                ],
                [
                    {
                        "item": Share.objects.get(symbol="AMD"),
                        "price": 50,
                        "amount": 10,
                        "fee": 0,
                        "date": "2018-01-01",
                        "is_deposit": True,
                        "user": self.user,
                    },
                    {},
                ],
            ]
        )

        mixin = TransactionChartDataMixin()
        mixin.request = self.__class__.Request(self.user)
        self.assertEqual(
            DashboardMarketValueRatioPieChart().get_series(
                mixin.get_transaction_basic_chart_data()
            ),
            {"USD": [{"name": "Net worth", "data": [{"name": "Share", "y": 1600.0}]}]},
        )

    def test_multiple_shares_and_multiple_coins(self):
        """
        Tests dashboard net worth chart with multiple shares.

        MSFT                        AMD
        ----                        ---

        Prices:                     Prices:
        2018-01-01 -> 100           2018-01-01 -> 50
        2018-01-02 -> 105           2018-01-02 -> 50
        2018-01-03 -> 110           2018-01-03 -> 50
        2018-01-04 -> 110           2018-01-04 -> 50
        2018-01-05 -> 120           2018-01-05 -> 40

        Investment: $1000           Investment: $500
        Profit: 20%                 Profit: -20%
        Market value: $1200         Market value: $400

        BTC                         ETH
        ---                         ---

        Prices:                     Prices:
        2018-01-01 -> 1000          2018-01-01 -> 200
        2018-01-02 -> 1000          2018-01-02 -> 200
        2018-01-03 -> 1100          2018-01-03 -> 200
        2018-01-04 -> 1100          2018-01-04 -> 200
        2018-01-05 -> 1200          2018-01-05 -> 170

        Investment: $1000           Investment: $500
        Profit: 20%                 Profit: -15%
        Market value: $1200         Market value: $425
        """

        create_items()
        create_prices(
            [
                ["2018-01-01", "MSFT", 100],
                ["2018-01-02", "MSFT", 105],
                ["2018-01-03", "MSFT", 110],
                ["2018-01-04", "MSFT", 110],
                ["2018-01-05", "MSFT", 120],
                ["2018-01-01", "AMD", 50],
                ["2018-01-02", "AMD", 50],
                ["2018-01-03", "AMD", 50],
                ["2018-01-04", "AMD", 50],
                ["2018-01-05", "AMD", 40],
                ["2018-01-01", "BTC", 1000],
                ["2018-01-02", "BTC", 1000],
                ["2018-01-03", "BTC", 1100],
                ["2018-01-04", "BTC", 1100],
                ["2018-01-05", "BTC", 1200],
                ["2018-01-01", "ETH", 200],
                ["2018-01-02", "ETH", 200],
                ["2018-01-03", "ETH", 200],
                ["2018-01-04", "ETH", 200],
                ["2018-01-05", "ETH", 170],
            ]
        )
        create_transactions(
            [
                [
                    {
                        "item": Share.objects.get(symbol="MSFT"),
                        "price": 100,
                        "amount": 10,
                        "fee": 0,
                        "date": "2018-01-01",
                        "is_deposit": True,
                        "user": self.user,
                    },
                    {},
                ],
                [
                    {
                        "item": Share.objects.get(symbol="AMD"),
                        "price": 50,
                        "amount": 10,
                        "fee": 0,
                        "date": "2018-01-01",
                        "is_deposit": True,
                        "user": self.user,
                    },
                    {},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="BTC"),
                        "price": 1000,
                        "amount": 1,
                        "fee": 0,
                        "date": "2018-01-01",
                        "is_deposit": True,
                        "user": self.user,
                    },
                    {},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="ETH"),
                        "price": 200,
                        "amount": 2.5,
                        "fee": 0,
                        "date": "2018-01-01",
                        "is_deposit": True,
                        "user": self.user,
                    },
                    {},
                ],
            ]
        )

        mixin = TransactionChartDataMixin()
        mixin.request = self.__class__.Request(self.user)
        data = DashboardMarketValueRatioPieChart().get_series(
            mixin.get_transaction_basic_chart_data()
        )

        LOGGER.debug(data)

        coin = list(filter(lambda i: "Coin" == i["name"], data["USD"][0]["data"]))
        share = list(filter(lambda i: "Share" == i["name"], data["USD"][0]["data"]))

        self.assertEqual(coin, [{"name": "Coin", "y": 1625.0}])
        self.assertEqual(share, [{"name": "Share", "y": 1600.0}])


class TemplateTagsTestCase(SimpleTestCase):
    def test_autofloatformat(self):
        # Rounding.
        self.assertEqual(autofloatformat(1234.0, no_str=True), 1234.0)
        self.assertEqual(autofloatformat(0.0, no_str=True), 0.0)
        self.assertEqual(autofloatformat(1234.1234, no_str=True), 1234.12)
        self.assertEqual(autofloatformat(1234.0, no_str=True), 1234)
        self.assertEqual(autofloatformat(0.001234, no_str=True), 0.00123)
        self.assertEqual(autofloatformat(0.00001234, no_str=True), 0.0000123)

        # Rounding + formatting.
        self.assertEqual(autofloatformat(1234.0), "1,234")
        self.assertEqual(autofloatformat(0.0), "0")
        self.assertEqual(autofloatformat(1234.1234), "1,234.12")
        self.assertEqual(autofloatformat(1234.0), "1,234")
        self.assertEqual(autofloatformat(0.001234), "0.00123")
        self.assertEqual(autofloatformat(0.00001234), "1.23e-05")

    def test_coinautofloatformat(self):
        # Rounding.
        self.assertEqual(coinautofloatformat(1234.0, no_str=True), 1234.0)
        self.assertEqual(coinautofloatformat(0.0, no_str=True), 0.0)
        self.assertEqual(coinautofloatformat(1234.1234, no_str=True), 1234.123)
        self.assertEqual(coinautofloatformat(1234.0, no_str=True), 1234)
        self.assertEqual(coinautofloatformat(0.001234, no_str=True), 0.001234)
        self.assertEqual(coinautofloatformat(0.00001234, no_str=True), 0.00001234)

        # Rounding + formatting.
        self.assertEqual(coinautofloatformat(1234.0), "1,234")
        self.assertEqual(coinautofloatformat(0.0), "0")
        self.assertEqual(coinautofloatformat(1234.1234), "1,234.123")
        self.assertEqual(coinautofloatformat(1234.0), "1,234")
        self.assertEqual(coinautofloatformat(0.001234), "0.001234")
        self.assertEqual(coinautofloatformat(0.00001234), "1.234e-05")

    def test_to_quarter_period(self):
        self.assertEqual(
            to_quarter_period(np.datetime64(datetime(2022, 8, 13, 0, 0, 0))), "2022 Q2"
        )
        self.assertEqual(
            to_quarter_period(pd.Timestamp(datetime(2022, 8, 13, 0, 0, 0))), "2022 Q2"
        )
        self.assertEqual(to_quarter_period("2022-08-13"), "2022 Q2")
        self.assertEqual(to_quarter_period(np.str_("2022-08-13")), "2022 Q2")
        self.assertEqual(to_quarter_period(123), 123)


@override_settings(
    CACHES={
        "default": {
            "BACKEND": "django.core.cache.backends.dummy.DummyCache",
        }
    }
)
class TheEyeShareIndexEtfTestCase(BaseTestCase):
    def setUp(self):
        super().setUp()

        # Must be (timezone) aware.
        self.now = None
        self.df = None
        self.open_df = None
        create_items()
        create_user_items()

    @patch("yfinance.Ticker.history")
    def test_fetch_view_pre_market(self, mocked_history):
        mocked_history.side_effect = self.generate_market_data
        self.now = datetime(
            2022, 1, 1, 8, 0, 0, tzinfo=pytz.timezone("America/New_York")
        )
        item = self.get_item()

        response = self.c.get(reverse("core:the_eye_fetch", args=[item.pk]))
        response = response.json()

        self.assertTrue(response["status"])
        self.assertEqual(response["data"]["symbol"], item.item.symbol.upper())
        self.assertIsNone(response["data"]["open"])
        self.assertIsNone(response["data"]["close"])
        self.assertEqual(
            response["data"]["min"]["formatted"], autofloatformat(self.df["Low"].min())
        )
        self.assertEqual(
            response["data"]["max"]["formatted"], autofloatformat(self.df["High"].max())
        )
        self.assertEqual(len(response["data"]["prices"]), len(self.df))

    @patch("yfinance.Ticker.history")
    def test_fetch_view_open_market(self, mocked_history):
        mocked_history.side_effect = self.generate_market_data
        self.now = datetime(
            2022, 1, 1, 10, 0, 0, tzinfo=pytz.timezone("America/New_York")
        )
        item = self.get_item()

        response = self.c.get(reverse("core:the_eye_fetch", args=[item.pk]))
        response = response.json()

        self.assertTrue(response["status"])
        self.assertEqual(response["data"]["symbol"], item.item.symbol.upper())
        self.assertEqual(
            response["data"]["open"],
            date_to_highcharts_timestamp(
                self.open_df.index[0].tz_convert(settings.TIME_ZONE)
            ),
        )
        self.assertIsNone(response["data"]["close"])
        self.assertEqual(
            response["data"]["min"]["formatted"], autofloatformat(self.df["Low"].min())
        )
        self.assertEqual(
            response["data"]["max"]["formatted"], autofloatformat(self.df["High"].max())
        )
        self.assertEqual(len(response["data"]["prices"]), len(self.df))

    @patch("yfinance.Ticker.history")
    def test_fetch_view_post_market(self, mocked_history):
        mocked_history.side_effect = self.generate_market_data
        self.now = datetime(
            2022, 1, 1, 17, 0, 0, tzinfo=pytz.timezone("America/New_York")
        )
        item = self.get_item()

        response = self.c.get(reverse("core:the_eye_fetch", args=[item.pk]))
        response = response.json()

        self.assertTrue(response["status"])
        self.assertEqual(response["data"]["symbol"], item.item.symbol.upper())
        self.assertEqual(
            response["data"]["open"],
            date_to_highcharts_timestamp(
                self.open_df.index[0].tz_convert(settings.TIME_ZONE)
            ),
        )
        self.assertEqual(
            response["data"]["close"],
            date_to_highcharts_timestamp(
                self.open_df.index[-1].tz_convert(settings.TIME_ZONE)
            ),
        )
        self.assertEqual(
            response["data"]["min"]["formatted"], autofloatformat(self.df["Low"].min())
        )
        self.assertEqual(
            response["data"]["max"]["formatted"], autofloatformat(self.df["High"].max())
        )
        self.assertEqual(len(response["data"]["prices"]), len(self.df))

    def generate_market_data(self, *args, **kwargs):
        """
        Creates dataframe with current (fake) market data.
        Is directed by ``self.now`` and ``prepost`` param
        in ``kwargs``.

        Emulates yfinance.Ticker.history() method.

        Generated dataframe has following columns:

        - Date (index)
        - Open
        - High
        - Low
        - Close
        - Volume
        - Dividends
        - Stock Splits
        """

        # NYSE opening hours
        # - pre-market starts at 4:00am EST
        # - stock exchange opens at 9:30am EST
        # - stock exchange closes at 4:00pm EST
        # - post-market ends at 7pm EST

        # Are made aware later when index is created.
        pre_period_start = datetime(2022, 1, 1, 4, 0, 0)
        post_period_end = datetime(2022, 1, 1, 19, 0, 0)

        # Must be aware because are used against aware index.
        period_start = datetime(
            2022, 1, 1, 9, 30, 0, tzinfo=pytz.timezone("America/New_York")
        )
        period_end = datetime(
            2022, 1, 1, 16, 0, 0, tzinfo=pytz.timezone("America/New_York")
        )

        # Create pre-market - post-market df.
        index = pd.DatetimeIndex(
            np.arange(
                pre_period_start,
                post_period_end,
                timedelta(minutes=5),
            ).astype(datetime),
            tz="America/New_York",
        )
        rng = np.random.default_rng()

        df = pd.DataFrame(
            {
                "Open": rng.random(len(index)) * 100,
                "High": rng.random(len(index)) * 100,
                "Low": rng.random(len(index)) * 100,
                "Close": rng.random(len(index)) * 100,
                "Volume": rng.integers(10, 20, size=len(index)),
                "Dividends": np.zeros(len(index), np.int8),
                "Stock Split": np.zeros(len(index), np.int8),
            },
            index=index,
        )

        # In case no pre/post market data are requested we slice
        # the df.
        if not kwargs.get("prepost", False):
            df = df[period_start:period_end]
            # Slice market data until "now".
            df = df[: self.now]
            self.open_df = df
        else:
            # Slice market data until "now".
            df = df[: self.now]
            self.df = df

        return df

    def get_item(self):
        """
        Returns random non-coin item.
        """

        return UserItem.objects.exclude(item__coin__isnull=False).order_by("?").first()


class TheEyeCoinTestCase(BaseTestCase):
    def setUp(self):
        super().setUp()

        create_items()
        create_user_items()
        self.df = None

    @patch("karpet.Karpet.fetch_crypto_live_data")
    def test_fetch_view(self, mocked_live_data):
        mocked_live_data.side_effect = self.generate_market_data
        item = self.get_item()

        response = self.c.get(reverse("core:the_eye_fetch", args=[item.pk]))
        response = response.json()

        self.assertTrue(response["status"])
        self.assertEqual(response["data"]["symbol"], item.item.symbol.upper())
        self.assertEqual(
            response["data"]["min"]["formatted"], autofloatformat(self.df["low"].min())
        )
        self.assertEqual(
            response["data"]["max"]["formatted"], autofloatformat(self.df["high"].max())
        )
        self.assertEqual(len(response["data"]["prices"]), len(self.df))

    def generate_market_data(self, *args, **kwargs):
        """
        Creates dataframe with current (fake) market data.

        Emulates karpet.fetch_crypto_live_data() method.

        Generated dataframe has following columns:

        - date time (index)
        - open
        - high
        - low
        - close
        """

        period_start = datetime(2022, 1, 1, 12, 0, 0)
        period_end = datetime(2022, 1, 2, 12, 0, 0)

        # Create df.
        index = pd.DatetimeIndex(
            np.arange(
                period_start,
                period_end,
                timedelta(minutes=30),
            ).astype(datetime),
        )
        rng = np.random.default_rng()

        self.df = pd.DataFrame(
            {
                "open": rng.random(len(index)) * 100,
                "high": rng.random(len(index)) * 100,
                "low": rng.random(len(index)) * 100,
                "close": rng.random(len(index)) * 100,
            },
            index=index,
        )

        return self.df

    def get_item(self):
        """
        Returns random coin item.
        """

        return UserItem.objects.exclude(item__coin__isnull=True).order_by("?").first()


class BaseDeleteUserItemTestCase(BaseTestCase):
    def spawn(self):
        create_items()
        item = self.model.objects.get(symbol=self.symbol)
        self.task(item.pk)

        ui_0 = UserItem.objects.create(item=item, user=self.user)
        ui_1 = UserItem.objects.create(item=item, user=self.user2)

        self.assertEqual(UserItem.objects.count(), 2)

        self.assertRedirects(
            self.c.get(reverse(self.url, args=[ui_0.pk])), reverse(self.target_url)
        )

        self.assertEqual(UserItem.objects.count(), 1)
        self.assertEqual(UserItem.objects.first(), ui_1)


class BaseItemOverviewAllocationTestCase(BaseTestCase):
    def prepare(self, prices):
        create_items()
        create_user_items()
        create_prices(prices)

    def spawn(self, transactions, url_pattern):
        create_transactions(transactions)

        response = self.c.get(reverse(url_pattern))
        html = response.content.decode("utf-8")

        doc = BeautifulSoup(html, "html.parser")
        tables = doc.find_all("table")

        # test number of tables
        self.assertEqual(len(tables), 2)

        # test number of meters
        meters = tables[0].find_all("meter")
        self.assertEqual(len(meters), len(transactions))

        # test meters values + order
        allocations = []

        for trans in transactions:
            allocations.append(trans[0]["price"] * trans[0]["amount"])

        total = sum(allocations)
        allocations.sort(reverse=True)

        for i, meter in enumerate(meters):
            self.assertEqual(
                round(float(meter["value"]), 1),
                round(100 / (total / allocations[i]), 1),
            )

        # items from 1st table are not duplicated in 2nd table
        symbols_1 = []
        symbols_2 = []

        for tr in tables[0].find_all("tr"):
            if tr.th:
                continue

            symbols_1.append(tr.td.get_text().strip())

        for tr in tables[1].find_all("tr"):
            if tr.th:
                continue

            symbols_2.append(tr.td.get_text().strip())

        self.assertEqual(set(symbols_1) & set(symbols_2), set())

        # print(tables[0].find_all("tr"))


class BaseDrawdownAndAthsTestCase(BaseTestCase):
    def setUp(self):
        super().setUp()

        create_items()
        create_user_items()
        self.user_item = UserItem.objects.get(item=self.model.objects.first())
        create_prices(
            [
                ["2018-01-01", self.user_item.item.symbol, 100],
                ["2018-01-02", self.user_item.item.symbol, 110],
                ["2018-01-03", self.user_item.item.symbol, 120],
                ["2018-01-04", self.user_item.item.symbol, 110],
                ["2018-01-05", self.user_item.item.symbol, 100],
                ["2018-01-06", self.user_item.item.symbol, 90],
            ]
        )


class BaseDrawdownTestCase(BaseDrawdownAndAthsTestCase):
    def spawn_chart_data(self):
        data = self.c.get(reverse("core:drawdown_fetch", args=[self.user_item.pk]))
        self.assertEqual(
            data.json(),
            {
                "status": True,
                "data": [
                    {
                        "data": [
                            [1514764800000.0, -0.0],
                            [1514851200000.0, -0.0],
                            [1514937600000.0, -0.0],
                            [1515024000000.0, 8.333333333333332],
                            [1515110400000.0, 16.666666666666664],
                            [1515196800000.0, 25.0],
                        ]
                    }
                ],
            },
        )

    def spawn_basic_info_data(self):
        doc = self.get_url_dom(reverse(self.url_pattern, args=[self.user_item.pk]))
        drawdown = (
            doc.find(id="basic-info")
            .find_all("li")[self.basic_info_position]
            .get_text()
            .strip()
            .split("\n")
        )

        self.assertEqual(drawdown[0].strip(), "-25%")
        self.assertEqual(drawdown[1].strip(), "(bear market)")


class BaseDrawdownAndAthsPageTestCase(BaseDrawdownAndAthsTestCase):
    def spawn_drawdown_page_table(self):
        context = self.c.get(
            reverse(self.url_pattern, args=[self.user_item.pk])
        ).context
        self.assertEqual(
            list(context["drawdowns"]),
            [
                {
                    "datetime": pd.Timestamp("2018-01-03 00:00:00+0000", tz="UTC"),
                    "ath": 120.0,
                    "datetime_prev": pd.Timestamp("2018-01-02 00:00:00+0000", tz="UTC"),
                    "days": 1.0,
                },
                {
                    "datetime": pd.Timestamp("2018-01-02 00:00:00+0000", tz="UTC"),
                    "ath": 110.0,
                    "datetime_prev": pd.Timestamp("2018-01-01 00:00:00+0000", tz="UTC"),
                    "days": 1.0,
                },
                {
                    "datetime": pd.Timestamp("2018-01-01 00:00:00+0000", tz="UTC"),
                    "ath": 100.0,
                    "datetime_prev": pd.NaT,
                    "days": 0.0,
                },
            ],
        )

    def spawn_drawdown_page_chart(self):
        context = self.c.get(
            reverse(self.url_pattern, args=[self.user_item.pk])
        ).context
        self.assertEqual(
            context["aths_chart_series"],
            [
                {
                    "name": "ATHs",
                    "data": [
                        [1514764800000.0, 100.0],
                        [1514851200000.0, 110.0],
                        [1514937600000.0, 120.0],
                    ],
                    "step": True,
                }
            ],
        )
