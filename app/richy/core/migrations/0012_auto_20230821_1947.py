# Generated by Django 4.0 on 2023-08-21 17:47

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("core", "0011_useritem_indexes"),
    ]

    operations = [migrations.RunSQL("TRUNCATE core_itemdata", migrations.RunSQL.noop)]
