# Generated by Django 4.0 on 2023-04-23 18:18

from django.conf import settings
from django.db import migrations, models


def set_user(apps, schema_editor):
    Item = apps.get_model("core", "Item")
    User = apps.get_model("core", "User")
    user = User.objects.first()

    for i in Item.objects.all():
        i.users.add(user)


class Migration(migrations.Migration):
    dependencies = [
        ("core", "0006_alter_asset_type"),
    ]

    operations = [
        migrations.AddField(
            model_name="item",
            name="users",
            field=models.ManyToManyField(to=settings.AUTH_USER_MODEL),
        ),
        migrations.RunPython(set_user, migrations.RunPython.noop),
    ]
