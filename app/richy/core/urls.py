from django.urls import path

from . import views

app_name = "core"
urlpatterns = [
    path("sign-in/", views.SignInFormView.as_view(), name="sign_in"),
    path("sign-out/", views.SignOutRedirectView.as_view(), name="sign_out"),
    path("search/", views.SearchRedirectView.as_view(), name="search"),
    path("item/<str:slug>/current-price/", views.ItemCurrentPriceAjaxView.as_view()),
    path(
        "the-eye/<int:id>/fetch/",
        views.TheEyeAjaxView.as_view(),
        name="the_eye_fetch",
    ),
    path(
        "item/<int:pk>/drawdown/fetch/",
        views.FetchDrawdownAjaxView.as_view(),
        name="drawdown_fetch",
    ),
]
