import logging
import time
from datetime import datetime

import numpy as np
import pytz
from django.core.cache import cache
from django.utils import timezone

from ..celery import app
from ..core.models import Meta, Price
from ..core.scraper import Manager
from ..core.tasks import generate_performance_charts
from ..transactions.tasks import calculate_dividends
from ..transactions.transactions import Performance, Transactions
from .models import Dividend, Share

LOGGER = logging.getLogger("richy.celery")


@app.task
def fetch_historical_data(share=None):
    """
    Fetches new historical data (prices) for the given share.
    If no share was given fetches data for all shares.

    If new data has been found we update caches and
    regenerate performance chart.

    :param int share: Share PK.
    """

    def fetch_share(share):
        """
        Fetches share historical data.

        :param Share share: Share model instance.
        """

        LOGGER.debug(f"Downloading prices for {share}.")
        df = Manager.fetch_share_prices(share)

        # Save prices into database.
        for index, r in df.iterrows():
            # # Check if price format is valid.
            # price = PManager.sanitize(r["Close"])

            # if price is False:
            #     LOGGER.debug(f"Price for {share.symbol} at {index} is not valid - it's \"{r['Close']}\". Skipping ...")
            #     continue

            try:
                price = float(r["Close"])
                assert not np.isnan(price)
            except Exception:
                LOGGER.debug(
                    "Price for {} at {} is not valid - it's {}. Skipping ...".format(
                        share.symbol, index, r["Close"]
                    )
                )
                continue

            # Create/Update database price record.
            try:
                p, created = Price.objects.get_or_create(
                    item=share,
                    datetime=datetime(
                        index.year, index.month, index.day, tzinfo=pytz.UTC
                    ),
                    defaults={"price": price},
                )

                if created:
                    LOGGER.debug(
                        "Prices successfully saved to database for {} - date {} - ID {}.".format(
                            share.symbol, p.datetime, p.pk
                        )
                    )
            except Exception:
                LOGGER.exception(
                    f"Prices weren't saved to database for {share.symbol} - date {index}!"
                )

    def update_caches(share):
        """
        Delete and/or updates related caches.

        :param Share share: Share model instance.
        """

        share.update_cache()
        cache.delete("shares-transaction-performance-and-assets")
        cache.delete("shares-transaction-profit-performance")

        Performance("share", profit=False, use_cache=False).get_data()
        Performance("share", profit=True, use_cache=False).get_data()

        # Transactions stats.
        Transactions.update_cache()

    LOGGER.debug("Starting datasets downloader...")

    # Fetch specific share.
    if share:
        share = Share.objects.get(pk=share)
        fetch_share(share)
        update_caches(share)
        generate_performance_charts(share.pk)

    # Or fetch all shares in the database.
    else:
        for s in Share.objects.order_by("symbol"):
            try:
                fetch_share(s)
            except Exception:
                LOGGER.exception(
                    "Share historical data fetching has failed.", extra={"share": s}
                )
                LOGGER.debug(f"Share: {s.symbol} ^")
            else:
                update_caches(s)
                generate_performance_charts(s.pk)

    # Save "last price update" meta stamp.
    Meta.objects.update_or_create(
        type=Meta.LAST_PRICE_UPDATE, defaults={"value": timezone.now()}
    )


@app.task
def fetch_financial_data(share=None):
    """
    Fetches financial data for all or the given share. Financial
    data are:
    - earnings
    - revenues
    - ratings

    :param int share: Share PK.
    """

    if share:
        try:
            Manager.fetch_financials(Share.objects.get(pk=share))
        except Exception:
            LOGGER.exception("Couldn't fetch financial data", extra={"share": share})
            LOGGER.debug(f"Share: {share.symbol} ^")
    else:
        for share in Share.objects.order_by("symbol"):
            try:
                Manager.fetch_financials(share)
            except Exception:
                LOGGER.exception(
                    "Couldn't fetch financial data", extra={"share": share}
                )
                LOGGER.debug(f"Share: {share.symbol} ^")

            time.sleep(5)


@app.task(queue="fast")
def fetch_basic_info(share=None):
    """
    Downloads basic info and saves it to ItemData models.

    :param int share: Share PK.
    """

    def fetch_share(share):
        # Download data.
        basic_info = Manager.get_share_basic_info(share)
        share.set_basic_info(basic_info)

        LOGGER.debug(f"Basic data were created/updated for share {share.symbol}")

    if share:
        fetch_share(Share.objects.get(pk=share))
    else:
        for share in Share.objects.all():
            try:
                fetch_share(share)
            except Exception:
                LOGGER.exception(
                    "Couldn't fetch share basic info", extra={"share": share}
                )
                LOGGER.debug(f"Share: {share.symbol} ^")

            time.sleep(5)


@app.task(queue="fast")
def fetch_current_price(share=None):
    """
    Fetches current market price and caches
    it under "item-{}-current-price" key for one hour.
    If no share PK instance is given fetches all shares in database.
    Sleeps 10 seconds between each fetch.

    :param int share: Share PK.
    """

    def fetch_share(share):
        price = Manager.get_current_price_and_change(share)

        if price:
            share.set_current_price_and_change(price)

    if share:
        fetch_share(Share.objects.get(pk=share))
    else:
        for s in Share.objects.all():
            try:
                fetch_share(s)
            except Exception:
                LOGGER.exception(
                    "Couldn't fetch share current price", extra={"share": s}
                )
                LOGGER.debug(f"Share: {s.symbol} ^")
            time.sleep(5)


@app.task(queue="fast")
def fetch_dividends(share=None):
    """
    Downloads dividends and saves it to Dividend models.

    :param int share: Share PK.
    """

    def fetch_share(share):
        LOGGER.debug(f"Fetching dividend for {share.symbol}...")

        # Download data.
        dividends = Manager.get_dividends(share)

        for div in dividends:
            _, created = Dividend.objects.update_or_create(
                share=share,
                ex_date=div["ex_date"],
                defaults={
                    "payment_date": div["payment_date"],
                    "record_date": div["record_date"],
                    "yield_value": div["yield"],
                    "amount_value": div["amount"],
                },
            )

            if created:
                LOGGER.debug(
                    f"Dividend for {share.symbol} for {div['record_date']} has been created."
                )
            else:
                LOGGER.debug(
                    f"Dividend for {share.symbol} for {div['record_date']} has been updated."
                )

    if share:
        fetch_share(Share.objects.get(pk=share))
        calculate_dividends(share)
    else:
        for share in Share.objects.all():
            try:
                fetch_share(share)
            except Exception:
                LOGGER.exception(
                    "Couldn't fetch share dividends.", extra={"share": share}
                )
                LOGGER.debug(f"Share: {share.symbol} ^")
            else:
                calculate_dividends(share)

            time.sleep(5)


@app.task(queue="fast")
def fetch_ratings(share=None):
    """
    Fetches analyst ratings and saves it to
    Asset model.

    :param int share: Share PK.
    """

    if share:
        try:
            Manager.fetch_ratings(Share.objects.get(pk=share))
        except Exception:
            LOGGER.exception("Couldn't fetch ratings data", extra={"share": share})
            LOGGER.debug(f"Share: {share.symbol} ^")
    else:
        for share in Share.objects.order_by("symbol"):
            try:
                Manager.fetch_ratings(share)
            except Exception:
                LOGGER.exception("Couldn't fetch ratings data", extra={"share": share})
                LOGGER.debug(f"Share: {share.symbol} ^")

            time.sleep(5)


@app.task(queue="fast")
def fetch_price_ratings(share=None):
    """
    Fetches analyst price ratings and saves it to
    Asset model.

    :param int share: Share PK.
    """

    if share:
        try:
            Manager.fetch_price_ratings(Share.objects.get(pk=share))
        except Exception:
            LOGGER.exception(
                "Couldn't fetch price ratings data", extra={"share": share}
            )
            LOGGER.debug(f"Share: {share.symbol} ^")
    else:
        for share in Share.objects.order_by("symbol"):
            try:
                Manager.fetch_price_ratings(share)
            except Exception:
                LOGGER.exception(
                    "Couldn't fetch price ratings data", extra={"share": share}
                )
                LOGGER.debug(f"Share: {share.symbol} ^")

            time.sleep(5)
