# Generated by Django 2.2 on 2021-11-16 18:22

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shares', '0002_auto_20211116_1915'),
    ]

    operations = [
        migrations.RenameField(
            model_name='dividend',
            old_name='receive_date',
            new_name='record_date',
        ),
    ]
