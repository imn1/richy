from django.urls import path

from . import views

app_name = "shares"
urlpatterns = [
    path("", views.OverviewCreateView.as_view(), name="overview"),
    path(
        "overview/fetch/", views.FetchOverviewAjaxView.as_view(), name="overview_fetch"
    ),
    path("share/<int:pk>/", views.ShareDetailView.as_view(), name="share_detail"),
    path(
        "share/<int:pk>/update/", views.ShareUpdateView.as_view(), name="update_share"
    ),
    path(
        "share/<int:pk>/delete/",
        views.DeleteShareRedirectView.as_view(),
        name="delete_share",
    ),
    path(
        "share/<int:pk>/reset/",
        views.ResetShareRedirectView.as_view(),
        name="reset_share",
    ),
    path(
        "share/<int:pk>/chart/linear-regression/",
        views.FetchLinearRegressionAjaxView.as_view(),
        name="chart_linear_regression",
    ),
    path(
        "share/<int:pk>/fetch/", views.FetchShareAjaxView.as_view(), name="share_fetch"
    ),
    path(
        "share/<int:pk>/performance/",
        views.PerformanceDetailView.as_view(),
        name="performance",
    ),
    path(
        "share/<int:pk>/dividends/",
        views.DividendListView.as_view(),
        name="dividends",
    ),
    path("splitter/", views.SplitterFormView.as_view(), name="splitter"),
    path(
        "splitter/preview/",
        views.SplitterPreviewFormView.as_view(),
        name="splitter_preview",
    ),
    path(
        "share/<int:pk>/financials",
        views.FinancialsDetailView.as_view(),
        name="financials",
    ),
    path(
        "share/<int:pk>/drawdown-periods-and-aths/",
        views.DrawdownPeriodsAndAthsTemplateView.as_view(),
        name="drawdown_periods_and_aths",
    ),
    path("share/<int:pk>/the-eye/", views.ShareTheEyeView.as_view(), name="the_eye"),
    path(
        "share/<int:pk>/historical-data/",
        views.HistoricalDataListView.as_view(),
        name="historical_data",
    ),
]
