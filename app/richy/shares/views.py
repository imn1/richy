import base64
import logging
from datetime import date, datetime, timedelta

import numpy as np
import pandas as pd
from braces.views import FormMessagesMixin, LoginRequiredMixin
from django.conf import settings
from django.contrib import messages
from django.http.response import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.template.defaultfilters import date as date_filter
from django.urls import reverse, reverse_lazy
from django.utils import timezone
from django.utils.translation import gettext as _
from django.utils.translation import ngettext_lazy
from django.views.generic import (
    DetailView,
    FormView,
    ListView,
    RedirectView,
    UpdateView,
)

from ..core.charts import Manager
from ..core.models import Asset, Price, UserItem
from ..core.templatetags.utils import to_quarter_period
from ..core.views import (
    AjaxView,
    BaseDeleteUserItemRedirectView,
    BaseDrawdownPeriodsAndAthsDetailView,
    BaseFetchItemAjaxView,
    BaseHistoricalData,
    BaseItemDetailView,
    BasePerformanceDetailView,
    BaseUserItemCreateView,
    SubmenuViewMixin,
    UserItemManipulationMixin,
    BaseTheEyeView,
)
from ..news.tasks import download_share_news
from .forms import SplitterForm, SplitterPreviewForm, UserShareForm
from .models import Dividend, Share
from .tasks import (
    fetch_basic_info,
    fetch_dividends,
    fetch_financial_data,
    fetch_historical_data,
    fetch_price_ratings,
    fetch_ratings,
)

LOGGER = logging.getLogger(__name__)


class FetchDataMixin:
    """
    Mixin that ensures all share data which needs to be downloaded.
    """

    def fetch_data(self, pk):
        fetch_basic_info.delay(pk)
        fetch_historical_data.delay(pk)
        download_share_news.delay(pk)
        fetch_financial_data.delay(pk)
        fetch_dividends.delay(pk)
        fetch_ratings.delay(pk)
        fetch_price_ratings.delay(pk)


class ShareSubmenuViewMixin(SubmenuViewMixin):
    # FIXME: is_current needs to be detected based on current URL (or so)
    def get_submenu(self):
        items = []

        # Dividends
        if self.object.item.share.dividend_set.exists():
            items.append(
                {
                    "url": reverse("shares:dividends", args=[self.object.pk]),
                    "title": "Dividends",
                    "is_current": False,
                }
            )

        return items


class ShareChildrenSubmenuViewMixin(ShareSubmenuViewMixin):
    # FIXME: is_current needs to be detected based on current URL (or so)
    def get_submenu(self):
        items = super().get_submenu()

        # Performance
        items.append(
            {
                "url": reverse("shares:performance", args=[self.object.pk]),
                "title": _("Performance"),
                "is_current": False,
            }
        )
        # Financials
        items.append(
            {
                "url": reverse("shares:financials", args=[self.object.pk]),
                "title": _("Financials"),
                "is_current": False,
            }
        )
        # Drawdowns and ATHs
        items.append(
            {
                "url": reverse(
                    "shares:drawdown_periods_and_aths", args=[self.object.pk]
                ),
                "title": _("Drawdowns and ATHs"),
                "is_current": False,
            }
        )
        # Historical data
        items.append(
            {
                "url": reverse("shares:historical_data", args=[self.object.pk]),
                "title": _("Historical data"),
                "is_current": False,
            }
        )
        # The eye
        items.append(
            {
                "url": reverse("shares:the_eye", args=[self.object.pk]),
                "title": "The Eye",
                "is_current": False,
            }
        )

        return items


class OverviewCreateView(FetchDataMixin, BaseUserItemCreateView):
    form_class = UserShareForm
    template_name = "shares/overview.pug"
    success_url = reverse_lazy("shares:overview")
    form_valid_message = _("Share has been saved.")
    form_invalid_message = _("Share hasn't been saved. Please check the form.")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        # Fetch shares.
        share_list = (
            UserItem.objects.select_related("item")
            .by_user(self.request.user)
            .filter(item__share__isnull=False)
            .order_by("is_archived", "item__symbol")
        )
        context["possession_stats"] = self.get_open_items_possession_stats("share")
        open_shares_list, context["share_list"] = self.split_open_items(share_list)

        # Sort open shares based on possession_stats (which are
        # sorted by percentage value).
        context["open_shares_list"] = self.sort_items_by_other_dict_keys(
            open_shares_list, context["possession_stats"]
        )

        return context


class FetchOverviewAjaxView(BaseFetchItemAjaxView):
    def get(self, request, *args, **kwargs):
        series = []

        for s in UserItem.objects.by_user(self.request.user).filter(
            item__share__isnull=False, show_in_overview=True, is_archived=False
        ):
            series.append(
                {
                    "name": s.item.symbol,
                    "data": self.get_item_prices(
                        s.item,
                        datetime__date__gte=date.today()
                        - timedelta(days=settings.OVERVIEW_SHARES_HISTORY),
                    ),
                }
            )

        return self.send(True, {"item": series})


class ShareDetailView(ShareSubmenuViewMixin, BaseItemDetailView):
    model = UserItem
    template_name = "shares/detail.pug"

    def dispatch(self, request, *args, **kwargs):
        # Check if there are already basic data (ItemData model).
        # Probably being downloaded right now.
        if not self.get_object().item.get_basic_info():
            messages.warning(request, _("Basic data are not ready yet."))

        return super().dispatch(request, *args, **kwargs)


class ShareUpdateView(
    LoginRequiredMixin,
    FormMessagesMixin,
    UserItemManipulationMixin,
    FetchDataMixin,
    UpdateView,
):
    model = UserItem
    form_class = UserShareForm
    template_name = "shares/update.pug"
    success_url = reverse_lazy("shares:overview")
    form_valid_message = _("Share has been saved.")
    form_invalid_message = _("Share hasn't been saved. Please check the form.")

    def form_valid(self, form):
        original = UserItem.objects.get(pk=form.instance.pk, user=self.request.user)

        # Unarchiving -> fetch all the data.
        if original.is_archived and not form.instance.is_archived:
            self.fetch_data(form.instance.item.pk)

        return super().form_valid(form)


class ResetShareRedirectView(LoginRequiredMixin, FetchDataMixin, RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        item = get_object_or_404(
            UserItem,
            pk=self.kwargs["pk"],
            user=self.request.user,
            item__share__isnull=False,
        )
        item.item.price_set.all().delete()
        self.fetch_data(item.item.pk)

        messages.success(self.request, _("Share prices are being reset."))

        return reverse("shares:overview")


class DeleteShareRedirectView(BaseDeleteUserItemRedirectView):
    def get_redirect_url(self, *args, **kwargs):
        if self.can_be_deleted():
            self.delete()
            messages.success(self.request, _("Share has been deleted."))

        else:
            messages.error(
                self.request,
                _("Share has open/closed transaction(s) thus cannot be deleted."),
            )

        return reverse("shares:overview")


class FetchLinearRegressionAjaxView(AjaxView):
    def get(self, request, *args, **kwargs):
        q = Price.objects.filter(item__pk=kwargs["pk"]).order_by("-datetime")
        date_set = False

        # Check date from.
        if request.GET.get("date_from"):
            q = q.filter(
                datetime__date__gte=datetime.strptime(
                    request.GET["date_from"], "%Y-%m-%d"
                ).date()
            )
            date_set = True

        # Check date to.
        if request.GET.get("date_to"):
            q = q.filter(
                datetime__date__lte=datetime.strptime(
                    request.GET["date_to"], "%Y-%m-%d"
                ).date()
            )
            date_set = True

        # If date is omitted set limit to last 5 years.
        if not date_set:
            q = q[: 22 * 3]

        m = Manager()
        buffer = m.linear_regression(q)

        if buffer:
            return self.send(True, base64.b64encode(buffer.read()).decode("ascii"))

        return self.send(True)


class FetchShareAjaxView(BaseFetchItemAjaxView):
    def get(self, request, *args, **kwargs):
        share = get_object_or_404(UserItem, pk=kwargs["pk"])
        data = {"item": []}

        # Fetch main series - price series - "share".
        data["item"] = self.get_item_prices(share.item)

        # Lookup for transactions.
        if request.GET.get("transactions"):
            data["transactions"] = self.get_transactions(share.item)

        # SMAs.
        if request.GET.get("smas"):
            if 20 <= len(data["item"]):
                data["ma20"] = share.item.get_sma(20)

            if 50 <= len(data["item"]):
                data["ma50"] = share.item.get_sma(50)

            if 200 <= len(data["item"]):
                data["ma200"] = share.item.get_sma(200)

        # Lookup for index.
        self.add_index_data(request, data)

        return self.send(True, data)


class PerformanceDetailView(ShareChildrenSubmenuViewMixin, BasePerformanceDetailView):
    pass


class DividendListView(LoginRequiredMixin, ShareChildrenSubmenuViewMixin, ListView):
    model = Dividend
    template_name = "shares/dividends.pug"
    ordering = "-payment_date"

    def get_queryset(self):
        self.object = get_object_or_404(UserItem, pk=self.kwargs["pk"])

        # We need to annotate PK of a (possible) transaction.
        query = (
            super()
            .get_queryset()
            .extra(
                select={
                    "dividend_transaction": "SELECT id FROM transactions_dividendtransaction WHERE dividend_id = shares_dividend.id AND user_id = %s LIMIT 1"
                },
                select_params=(self.request.user.pk,),
            )
        )
        query = query.filter(share=self.object.item)

        return query

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["object"] = self.object

        return context


class SplitterFormView(LoginRequiredMixin, FormView):
    # preview_template = "shares/splitter/preview.pug"
    form_class = SplitterForm
    template_name = "shares/splitter/form.pug"
    success_url = reverse_lazy("shares:splitter_preview")

    def form_valid(self, form):
        self.request.session["splitter"] = {
            "share": form.cleaned_data["share"].pk,
            "date": form.cleaned_data["date"].isoformat(),
            "ratio": form.cleaned_data["ratio"],
        }

        return super().form_valid(form)


class SplitterPreviewFormView(LoginRequiredMixin, FormMessagesMixin, FormView):
    """
    Accessible only to admins.
    """

    template_name = "shares/splitter/preview.pug"
    form_class = SplitterPreviewForm
    success_url = reverse_lazy("shares:splitter")

    def dispatch(self, request, *args, **kwargs):
        if "splitter" not in request.session:
            messages.error(request, _("No splitter data. Please fill the form."))

            return HttpResponseRedirect(reverse("shares:splitter"))

        self.splitter = request.session["splitter"]
        self.share = Share.objects.get(pk=self.splitter["share"])
        self.date = datetime.fromisoformat(self.splitter["date"])
        self.ratio = self.splitter["ratio"]
        self.transactions = self.share.transaction_set.filter(date__lt=self.date)

        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        for trans in self.transactions:
            # Split.
            trans.price /= self.ratio
            trans.amount *= self.ratio

            if trans.note:
                trans.node += "/n"
            else:
                trans.node = ""

            trans.node = f"(split {date_filter(timezone.now(), 'DATETIME_FORMAT')}"
            trans.save()

        # Refetch prices.
        self.share.price_set.all().delete()
        fetch_historical_data.delay(self.share.pk)

        return super().form_valid(form)

    def get_form_valid_message(self):
        message = _("Item %(item)s has been split with ratio %(ratio)s:1") % {
            "item": self.share.symbol,
            "ratio": self.ratio,
        }

        # Clean up session.
        del self.request.session["splitter"]

        return message

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["share"] = self.share
        context["date"] = self.date
        context["ratio"] = self.ratio
        context["transactions"] = self.transactions

        return context


class FinancialsDetailView(ShareChildrenSubmenuViewMixin, DetailView):
    model = UserItem
    template_name = "shares/financials.pug"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        df_re, eps_rows = self.get_revenues_and_earnings_and_eps_data()

        context["revenue_earnings_eps_rows"] = (
            df_re.fillna("-").sort_index(ascending=False).to_records()
        )
        context["revenues_earnings_eps_chart_options"] = (
            self.get_revenues_and_earnings_and_eps_chart_options(df_re)
        )
        context["eps_rows"] = eps_rows
        context["eps_chart_options"] = self.get_eps_chart_options(eps_rows)
        context["ratings_chart_options"] = self.get_ratings_chart_options()
        price_ratings_rows = self.get_price_ratings_data()
        context["price_ratings_rows"] = price_ratings_rows
        context["price_ratings_chart_options"] = self.get_price_ratings_chart_options(
            price_ratings_rows
        )

        return context

    def get_revenues_and_earnings_and_eps_data(self):
        """
        Fetches and converts revenues + earnings + eps data from the database
        to pandas dataframe sorted by date in descending order.

        Also return EPS rows except "announcement date" column sorted by date - in descending order.

        :return: Dataframe with "revenues" and "earnings" series and data as index and EPS rows.
        :rtype: tuple
        """

        # TODO: whole method could be cached and invalidated by fetch_financial_data celery task.

        # Revenues.
        try:
            revenues_data = self.object.item.asset_set.get(type=Asset.REVENUES_DATA)
        except Exception:
            revenues_s = pd.Series(name="revenues")
        else:
            try:
                revenues = np.array(revenues_data.data)
                revenues_s = pd.Series(
                    revenues[:, 1].astype(float), index=revenues[:, 0], name="revenues"
                ).round()
            except Exception:
                LOGGER.exception(
                    "Couldn't convert rvenues data to pandas series",
                    extra={"revenues data": revenues_data},
                )
                revenues_s = pd.Series(name="revenues")

        # Earnings.
        try:
            earnings_data = self.object.item.asset_set.get(type=Asset.EARNINGS_DATA)
        except Exception:
            earnings_s = pd.Series(name="earnings")
        else:
            try:
                earnings = np.array(earnings_data.data[1:])
                earnings_s = pd.Series(
                    earnings[:, 1].astype(float), index=earnings[:, 0], name="earnings"
                ).round()
            except Exception:
                LOGGER.exception(
                    "Couldn't convert earnings data to pandas series",
                    extra={"eargnings data": earnings_data},
                )
                earnings_s = pd.Series(name="earnings")

        # EPS
        try:
            eps_data = self.object.item.asset_set.get(type=Asset.EPS_DATA)
        except Exception:
            eps_s = pd.Series(name="eps")
            eps_rows = []
        else:
            try:
                eps = np.array(eps_data.data[1:])
                eps_rows = eps[:, [1, 2, 3]]
                eps_rows = eps_rows[
                    eps_rows[:, 0].argsort()[::-1]
                ]  # Sorts by first column (date) - reversely.
                eps_s = pd.Series(eps[:, 3].astype(float), index=eps[:, 1], name="eps")
            except Exception:
                LOGGER.exception(
                    "Couldn't convert EPS data to pandas series",
                    extra={"eps data": eps_data},
                )
                eps_s = pd.Series(name="eps")
                eps_rows = []

        # Merge together.
        df = pd.concat([revenues_s, earnings_s, eps_s], axis=1)
        df.index = pd.to_datetime(df.index)

        return (
            df,
            eps_rows,
        )

    def get_revenues_and_earnings_and_eps_chart_options(self, df):
        """
        Compiles Highcharts chart options which is later passed to
        highcarts vue component.

        :param pd.DataFrame df: Dataframe with earings and revenues.
        :return: Highcharts options as a dict.
        :rtype: dict
        """

        data = {
            "title": False,
            "chart": {"zooming": {"type": "x"}},
            "colors": settings.CHART_COLORS,
            "xAxis": [
                {
                    "categories": [to_quarter_period(d) for d in df.index],
                },
            ],
            "yAxis": [
                {},
                {
                    "opposite": True,
                },
            ],
        }

        data["series"] = [
            {"name": _("Revenue"), "type": "column", "data": df["revenues"].tolist()},
            {"name": _("Earnings"), "type": "column", "data": df["earnings"].tolist()},
            {
                "name": _("EPS"),
                "type": "column",
                "yAxis": 1,
                "data": df["eps"].tolist(),
                "visible": False,
            },
        ]

        return data

    def get_eps_chart_options(self, rows):
        """
        Compiles Highcharts chart options (which is later passed to
        highcarts vue component) with data sorted by date (first column).

        :param list rows: Table EPS rows without headers and Announcement date column.
        :return: Highcharts option dict.
        :rtype: dict
        """

        # Sort by date.
        rows = sorted(rows, key=lambda r: r[0])

        data = {
            "title": False,
            "chart": {"zooming": {"type": "x"}},
            "colors": settings.CHART_COLORS,
            "xAxis": [
                {
                    "categories": [to_quarter_period(r[0]) for r in rows],
                },
            ],
            "series": [
                {
                    "name": _("Estimated EPS"),
                    "type": "column",
                    "data": [float(r[1]) for r in rows],
                },
                {
                    "name": _("Actual EPS"),
                    "type": "column",
                    "data": [float(r[2]) for r in rows],
                },
            ],
        }

        return data

    def get_ratings_chart_options(self):
        """
        Compiles Highcharts chart options (which is later passed to
        highcarts vue component) for each ratings record in the
        database.

        :return: List of Highcharts option dicts.
        :rtype: list
        """

        try:
            ratings = self.object.item.asset_set.get(type=Asset.RATINGS_DATA)
        except Exception:
            return []

        if not ratings:
            LOGGER.error(
                "Ratings data do exist in database but are falsy",
                extra={"ratings": ratings},
            )
            return []

        data_set = []

        for i, rating in enumerate(ratings.data):
            # Compile chart title.
            if 3 > i:
                title = ngettext_lazy(
                    "%(months)d month ago", "%(months)d months ago", 3 - i
                ) % {"months": 3 - i}
            else:
                title = _("Now")

            # Chart options data.
            data_set.append(
                (
                    title,
                    {
                        "title": False,
                        "chart": {"type": "pie"},
                        "colors": settings.CHART_COLORS,
                        "plotOptions": {
                            "pie": {
                                # "size": "50%",
                                "dataLabels": {
                                    "enabled": False,
                                },
                                "showInLegend": True,
                            }
                        },
                        "tooltip": {"pointFormat": "<b>{point.percentage:.1f} %</b>"},
                        "series": [
                            {
                                "name": _("Ratings"),
                                "data": [
                                    {
                                        "name": rating_state,
                                        "y": int(rating[rating_state]["value"]),
                                    }
                                    for rating_state in rating.keys()
                                ],
                            }
                        ],
                    },
                )
            )

        return data_set

    def get_price_ratings_data(self):
        """
        Fetches price ratings and deserializes date and calculates
        new 2 columns: before price and after price. "After price" is
        always set, "before" only if the difference exists.

        :return: Table rows.
        :rtype: list
        """

        try:
            ratings = self.object.item.asset_set.get(type=Asset.PRICE_RATINGS).data
        except Exception:
            return []

        if not ratings:
            LOGGER.error(
                "Price ratings data do exist in database but are falsy",
                extra={"ratings": ratings},
            )
            return []

        # Deserialize various data for each row.
        for i, row in enumerate(ratings):
            # Date.
            ratings[i][0] = datetime.strptime(row[0], "%Y-%m-%d").date()

            # Price (before, after).
            if type(row[4]) is str and "→" in row[4]:
                before, after = row[4].split("→")
                before = float(before.strip("$ "))
                after = float(after.strip("$ "))
            else:
                before = 0
                after = row[4]

            ratings[i].append(before)
            ratings[i].append(after)

        return ratings

    def get_price_ratings_chart_options(self, rows):
        """
        Compiles Highcharts options (which is later passed to
        highcharts vue component) for price ratings.

        :param list rows: Table price ratings rows.
        :return: Highcharts option object.
        :rtype: dict
        """

        # Sort by date.
        rows = sorted(rows, key=lambda row: row[0])

        # Filter out rows with empty price columns.
        rows = list(filter(lambda row: row[4], rows))
        last_price = self.object.item.get_last_price(current=True)

        data = {
            "title": False,
            "chart": {"zooming": {"type": "x"}},
            "colors": settings.CHART_COLORS,
            "tooltip": {
                "pointFormat": "<span>{series.name}:</span> {point.price_formatted}"
            },
            "xAxis": {
                "categories": [date_filter(row[0]) for row in rows],
            },
            "yAxis": {
                "plotLines": [
                    {
                        "value": last_price.price if last_price else 0,
                        "color": settings.CHART_COLORS[1],
                        "width": 2,
                        "zIndex": 4,
                    }
                ],
            },
            "series": [
                {
                    "name": _("Price"),
                    "type": "column",
                    "data": [
                        {
                            "name": row[2],
                            "y": row[6],
                            "price_formatted": (
                                f"<b>{row[5]}</b> -> " if row[5] else ""
                            )
                            + f"<b>{row[6]}</b>",
                        }
                        for row in rows
                    ],
                },
                {
                    "name": _("Current price"),
                    "type": "line",
                    "color": settings.CHART_COLORS[1],
                },
            ],
        }

        return data


class DrawdownPeriodsAndAthsTemplateView(
    ShareChildrenSubmenuViewMixin, BaseDrawdownPeriodsAndAthsDetailView
):
    pass


class ShareTheEyeView(ShareChildrenSubmenuViewMixin, BaseTheEyeView):
    pass


class HistoricalDataListView(ShareChildrenSubmenuViewMixin, BaseHistoricalData): ...
