from datetime import date

from django.db import models

from ..core.models import Item


def calc_order():
    try:
        i = Share.objects.order_by("-order")[0]

        return i.order + 1

    except Exception:
        return 0


class Share(Item):
    def is_share(self):
        return True

    def is_index(self):
        return False

    def is_etf(self):
        return False

    def is_coin(self):
        return False


class Dividend(models.Model):
    share = models.ForeignKey(Share, on_delete=models.CASCADE)
    ex_date = models.DateField()
    payment_date = models.DateField(null=True)
    record_date = models.DateField(null=True)
    yield_value = models.FloatField()
    amount_value = models.FloatField()

    def is_in_future(self):
        return self.payment_date > date.today()
