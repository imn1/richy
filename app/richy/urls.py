"""richy URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.conf import settings
from django.contrib import admin
from django.urls import include, path

from .core import views as c_views

urlpatterns = [  # pylint: disable=invalid-name
    path("admin/", admin.site.urls),
    path("", c_views.DashboardTemplateView.as_view(), name="dashboard"),
    path("core/", include("richy.core.urls", namespace="core")),
    path("shares/", include("richy.shares.urls", namespace="shares")),
    path("coins/", include("richy.coins.urls", namespace="coins")),
    path("indexes/", include("richy.indexes.urls", namespace="indexes")),
    path("news/", include("richy.news.urls", namespace="news")),
    path("transactions/", include("richy.transactions.urls", namespace="transactions")),
    path("etfs/", include("richy.etfs.urls", namespace="etfs")),
    path("staking/", include("richy.staking.urls", namespace="staking")),
]


if "debug_toolbar" in settings.INSTALLED_APPS:
    import debug_toolbar

    urlpatterns = [  # pylint: disable=invalid-name
        path("__debug__/", include(debug_toolbar.urls))
    ] + urlpatterns
