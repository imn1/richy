# pylint: disable-all
import os

module = "richy"
name = module
workers = 4
bind = ["[::]:8000"]  # IPv6 alias to IPv4 0.0.0.0:8000
user = "root"
loglevel = "debug" if "true" == os.getenv("GUNICORN_DEBUG") else "error"
errorlog = "-"
timeout = 900
reload = True if "true" == os.getenv("GUNICORN_RELOAD") else False
