#!/bin/bash

docker compose -f compose.dev.yaml up webpack
docker compose -f compose.dev.yaml exec richy ./manage.py collectstatic -i node_modules --no-input
